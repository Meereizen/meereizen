(function ($) {

    $.mobiscroll.themes.meereizen = {
        defaults: {
            dateOrder: 'MMdyy',
            rows: 5,
            height: 40,
            width: 60,
            headerText: false,
            showLabel: false,
            btnWidth: false,
            useShortLabels: true
        }
    };

})(jQuery);
