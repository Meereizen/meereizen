$(document).ready(function() {
	FastClick.attach(document.body);

	// Toggle functie omdat die sinds 1.9 niet meer standaard in jQuery zit
	$.fn.clicktoggle = function(a, b) {
	    return this.each(function() {
	        var clicked = false;
	        $(this).bind("click", function() {
	            if (clicked) {
	                clicked = false;
	                return b.apply(this, arguments);
	            }
	            clicked = true;
	            return a.apply(this, arguments);
	        });
	    });
	};

	// JS voor de mobiele site/app weergave
	if( window.innerWidth < 700 ) {

		/*	Loader laten zien wanneer een nieuwe pagina geladen wordt
		
			Dit is echt een smeeerige fix omdat veel mobiele browsers onbeforeunload niet snappen
			Op deze manier hebben we toch een betere 'responsiveness' maar een beetje jammer is het wel.
		*/

		$(window).bind('popstate', function(event) {
			$('.mobile-bottom-loader').animate({bottom: '-200px'}, 0);
		});

		$(document).on("click", 'a', function(event) {
			if($(this).attr('href') !== undefined) {
				var url = $(this).attr("href");
				event.preventDefault();
				$('.mobile-bottom-loader').animate({bottom: '0px'}, 250, function(){
					setTimeout(function(){window.location.href = url}, 1);
				});
			}
			if($(this).data("onclick") !== undefined) {
				var onclick = $(this).data("onclick");
				$('.mobile-bottom-loader').animate({bottom: '0px'}, 250, function(){
					eval(onclick);
				});
//				alert('ONCLICK');
			}
//			alert('OK');
		});

		if($('.header nav .title.mobile-only .verkort-stations').length > 0){
			var title = $('.header nav .title.mobile-only .verkort-stations').html();

			var list = {
				'Amsterdam': 'A\'dam',
				'Almere': 'Alm.',
				'Rotterdam': 'R\'dam',
				'Centraal': 'C.',
				'wijk': 'w.'
			};

			$.each(list, function( index, value ) {
				title = title.replace(index, value);
			});
			$('.header nav .title.mobile-only .verkort-stations').html(title);
		}
		
		// Vervang de placeholders met een JS versie voor oude browsers
//		$("input[placeholder]").placeholder();
	
		// On enter; keyboard down maar niet submitten
		$('input').keydown(function(e) {
			var input = $(this);
			var form = input.closest('form');
			if (e.keyCode == 13) {
				e.preventDefault();
				input.blur();
			}
		});
	}

	// Confirm link
	$(".link-to-confirm").click(function(e){
		e.preventDefault();
		var l = $(this);
		
		if(l.data('message')) {
			var m = confirm(l.data('message'));
		}else{
			var m = confirm("Weet je het zeker");
		}
		alert(l.data('form'));
		if(l.data('form')) {
			var action = function() { document.getElementById(l.data('form')).submit(); return false; };
		}else{
			var action = function() { document.location.href = l.attr('href'); };
		}

		if(m == true) {  
			action();
		}else{
			return false;
		}
	});
	
	// De toggle functie gebruiken om de extra info te laten zien bij het plannen van een reis
	$(".reismogelijkheid .meerinfo").clicktoggle(
		function(){
			$(this).html('<i class="icon-arrow-up"></i> Inklappen');
			$(this).parent().siblings('.info').css('display', 'block');
		},
		function(){
			$(this).html('<i class="icon-arrow-down"></i> Uitklappen');
			$(this).parent().siblings('.info').hide(0);		
		});

	$('.form-submitter').click(function(){
		var form = $(this).data('form');
		$(form).submit();
	});

	$('.inloggen-button').click(function(){
		$('#inloggen-form').toggle();
	});
		
	// Uitgelichte trajecten laden
	$('.ajax-container').each(function() {
		var element = $( this );
		var type = element.data('type');
		switch(type) {

			case 'uitgelicht-vandaag':
				$( this ).load( BASE_URL+"/ajax/uitgelicht/vandaag" );
				break;

			case 'mobile-login':
				$.ajax({
					url: BASE_URL+"/abonnementhouder/index/TRUE/",
					timeout: 5000
				}).done(function(data) {
					element.html(data);
				})
				.fail(function() {
					$('.mobile-bottom-error').animate({bottom: '0px'}, 250, function(){
						element.html(' ');
					});
				});
				break;
				
			case 'prijs':
			
				var fromStation = $( this ).data('from');
				var toStation = $( this ).data('to');
				var dateTime = $( this ).data('date');
			
				$.ajax({
					type: "GET",
					url: BASE_URL+"/ajax/prijsJSON/"+fromStation+"/"+toStation+"/"+dateTime,
				}).done(function( msg ) {
					element.html('<p id="old-price">normaal &euro; '+msg["vol_tarief"]+', nu:</p><span id="new-price">&euro; '+msg["nieuw_tarief"]+'</span>');
				});
				break;

			case 'openstaande-reizen-count':
			
				$.ajax({
					type: "GET",
					url: BASE_URL+"/ajax/openstaandeReizenCount/",
				}).done(function( msg ) {
					element.html(msg);
				});
				break;

			default:
				$(this).html('<div class="alert yellow">Er ging iets fout tijdens het ophalen van de gegevens.</div>');
		}
	});


	// Tabs voor promotionele shizzle	
	$( ".tabs-inactive" ).tabs({
		collapsible: true,
		active: false
	});
	
	// Alle andere tabs
	$( ".tabs" ).tabs();

	if(window.innerWidth > 699 && $('.header-small').length == 0) {

		$('.not-required').focus(function(){
			$(this).css('opacity', '1');
		});

        if (document.cookie.indexOf('visited') == -1) {
			$('body').addClass('overlay');

			$('.promotional-popup .close').click(function(){
				$(this).parent().fadeOut(250, function(){
					$('body').removeClass('overlay');
				});
			});

			$(document).keydown(function(e) {
				if (e.keyCode == 27) {
					$('.promotional-popup').fadeOut(250, function(){
						$('body').removeClass('overlay');
					});
				}
			});


			document.cookie = "visited=yes";
		}

		// Achtergronden shuffle voor de homepage
		var images = ['Gare-du-Nord-Paris.jpg', '_DSC0365.jpg', 'Centraal-Station-Amsterdam.jpg'];
		
		$('.header').css({'background-image': 'url('+BASE_URL+'/static/images/backgrounds/' + images[Math.floor(Math.random() * images.length)] + ')'});

		$('.planner').append('<div class="tooltip-planner desktop-only">Alleen dit veld is verplicht</div>');
	}	
});

$(window).load(function() {
	$('.mobile-bottom-loader').animate({bottom: '-100px'}, 100, function(){
		if($('.swipe-main, .swipe-wrap, .swipe-wrap > div').length > 0){
			$('.swipe-main, .swipe-wrap, .swipe-wrap > div').fadeIn(50);
		}
	});
});