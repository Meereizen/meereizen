# Inleiding

Meereizen is een groot project en dus is het handig om volledige documentatie te hebben van de code, of dit op z'n minst na te streven. In dit document behandel ik (Olivier) de werkwijze, opzet en abstracte ideeën achter het systeem.

# Inhoud van dit document

[TOC]

# Werkwijze

## Git

Het Meereizen project wordt gebouwd met behulp van git. Dit is een versie beheer systeem. Wanneer je bestanden gewijzigd hebt, kun je deze _committen_. Je kan daarna verder gaan, of, als de wijzigingen terug gedraaid moeten worden, terug gaan naar een vorige commit.

## Remote repo

Git werkt lokaal maar er draait een remote repository op Bitbucket. Dit is een kopie van het project, bereikbaar via een URL. Onze lokale commits kunnen we naar deze remote repo _pushen_. Daarna kunnen deelnemers aan het project deze repo _clonen_. Dit houdt in dat ze een kopie downloaden en lokaal kunnen werken aan de code.

## Git workflow

Tijdens het bouwproces hebben we een bepaalde workflow. Wanneer er actief ontwikkeld wordt, houdt dit het volgende in:

De persoon controleert of hij met de laatste versie van de code werkt. Als dit niet het geval is, _fetcht_ hij de laatste versie van de remote repo. Hierna wijzigt de persoon de broncode, verwijdert of voegt dingen toe. Als deze fase voorbij is, commit hij de veranderingen en pusht hij de laatste versie naar de remote repo.

Wanneer er echter meer experimentele features gebouwd worden, kan het verstandig zijn om een nieuwe _branch_ aan te maken. Dit is een zijtak van het project waarin vrij ontwikkeld kan worden door de persoon. Hierna kan deze tak weer samengevoegd worden met de hoofdtak, de _master branch_, door een _pull request_ in te dienen. De beheerder van het project checkt alles dan nog eens voordat hij het pull request goedkeurt.

## Deployment naar development server

Op de development server draait de laatste versie van het project zoals die ook in de remote repo te vinden is. De dev server zou ook kunnen dienen als de remote repo host maar dan missen we de web UI die Bitbucket wel biedt.

## Notities bij installeren op lege server

Aangezien dit nog wel eens voor frustratie kan zorgen:
 
    1. Zorg er eerst voor dat SSH werkt.
    2. Installeer dan php5, apache2, php5-mysql en php5-curl.
    3. Chmod /var/www.
    3. Start de server en check of mod_rewrite aanstaat.
    4. Pas /etc/apache2/sites-available/default aan zodat er naar .htaccess bestanden geluisterd wordt.
    5. Check of Git geinstalleerd is.
    6. Clone de Git repo in /var/www.
    7. Cronjobs opzetten. cronjobs/main.php moet ongeveer elk kwartier/half uur uitgevoerd worden. (`$ crontab -e`  en dan  `0,15,30,45 * * * * /usr/bin/php /var/www/application/cronjobs/main.php`)

# PIP Framework

Meereizen gebruikt (een door mij aangepaste versie van) het [PIP framework][1], een simpel en minimalistisch MVC framework. Er worden weinig extra functies aangeboden die wel te vinden zijn in bijvoorbeeld CakePHP maar daar tegenover staat dat PIP bestaat uit extreem weinig regels code en heel snel is. Nadeel is wel dat er lange applicatie bestanden (zoals de controllers) kunnen ontstaan. De volledige documentatie van PIP is te vinden op [hun website][1] en het is handig om door te lezen.

## Bestandenstructuur

PIP heeft een aantal bestanden nodig om te kunnen functioneren. In de root hebben we de `system`, `static` en `application` folders. De `system` folder bevat de bestanden van PIP en dient ongewijzigd te blijven, dit is waar de MVC magie gebeurt. `static` bevat afbeeldingen, lettertypes, CSS bestanden en JS scripts. Dit zijn de bestanden die mogelijk naar de browser verstuurd worden. In `application` zijn de bestanden van ons systeem te vinden die nooit naar de browser verstuurd zullen worden, maar worden uitgevoerd op onze server. Ze zijn onderverdeeld in de volgende mappen: `models`, `views` en `controllers`. Dit scheidt dus de layout en het ontwerp van de applicatielogica en van de databasecommunicatie.

Er zijn ook nog de mappen `plugins`, `helpers`, `config` en `cache` te vinden. Plugins zijn hier de bestanden of libraries die van buitenaf gedownload zijn en in het systeem gebruikt worden. Helpers zijn stukken code die vaker gebruikt kunnen worden en te lang zijn om in controllers te gebruiken. Beiden kunnen dan geinclude worden. In de config wordt het een en ander ingesteld en de cache folder bevat informatie die we vanuit de NS API verkregen hebben.

### Vind code op basis van URL

Door het systeem vorm te geven zoals in de bovenstaande paragraaf beschreven is, kunnen we code vinden door naar de URL te kijken. De URL is opgebouwd in de volgende vorm:
`http://www.domain.ext/controllerName/functionName/variableOne/variableTwo/`
Variabelen zijn optioneel. Default waarden voor controller en function zijn `main`. De code die op deze pagina uitgevoerd wordt staat in het bestand `Meereizen/application/controllers/[controllerName]` onder de functie `[functionName]`.

In de controller code kan verwezen worden naar een view, model, plugin of helper. Zodra hier naar verwezen wordt, wordt deze code ook uitgevoerd, eerder niet. Standaard wordt er dus niks anders uitgevoerd dan de controller.

## Denkwijze

De MVC denkwijze moet zo veel mogelijk aangehouden worden. In de controllers mag absoluut geen SQL teruggevonden worden en in de models mag alleen data opgehaald, verwerkt en doorgestuurd worden. Eigenlijk mag er in de views alleen maar layout te vinden zijn, maar omdat PIP geen markup language gebruikt is dat moeilijk. Er zal dus ook PHP te vinden zijn in de views, vooral om te includen maar ook om data om te zetten bijvoorbeeld.

## Controllers en URLs

PIP bevat geen router en de URLs worden dus automatisch aangemaakt op de volgende manier:
`http://www.domain.ext/controllerName/functionName/variableOne/variableTwo/`


# Gebruikerssysteem

We onderscheiden twee dingen: authenticatie en autorisatie. Authenticatie is het vaststellen dat een gebruiker echt is wie hij zegt dat hij is. Autorisatie is het kijken of een gebruiker wel de rechten heeft om een pagina te bekijken.

Authenticatie gebeurt bij ons door middel van OAuth en hier hoeven wij ons dus bijna geen zorgen over te maken. Hiervoor gebruiken we de plugin [Opauth][2]. Deze includen we als we hem nodig hebben, dit is alleen maar in de abonnementhouder class. In de constructor van deze class zijn ook de config gegevens te vinden van Opauth. `http://www.meereizen.nl/abonnementhouder/facebook` en soortgelijke URLs zijn automatisch geregistreerd door Opauth. Deze linken door naar Facebook, Twitter of Google en nadat de gebruiker toestemming gegeven heeft, wordt de userdata terug gestuurd naar een callback URL. Dit is bij ons de functie oauth2callback. De functie ontvangt de data van OAuth en zorgt ervoor dat de gebruiker inlogt of geregistreerd wordt.

Na registratie moeten er nog wat details ingevuld worden en wordt er een bevestigings e-mail verstuurd. Hierna kan de gebruiker definitief aan de slag.

De abonnementhouder class maakt gebruik van de `login_helper`. Dit is een helper class met functies die betrekking hebben tot sessions. Op deze manier hebben we alle verwijzingen naar sessions in 1 plek bij elkaar en is het makkelijk te overzien en aan te passen.

# Verificatie van abonnementhouders

Het verifieren van de abonnementhouders is belangrijk. Als we dit niet zouden doen, kunnen mensen frauderen door te zeggen dat ze abonnementhouders zijn terwijl ze nooit op komen dagen. Om dit tegen te gaan hebben we twee verificatiemechanismen bedacht.

## Verificatie code

De eerste is de verificatie code. Dit is een code die de meereiziger ontvangt nadat hij betaald heeft. Het is de bedoeling dat de meereiziger de code overdraagt aan de abonnementhouder wanneer ze elkaar ontmoeten. Vanaf het moment dat de trein vertrekt gaat de _verificatie code gateway_ open. Hier dient de abonnementhouder de code in te voeren. Pas wanneer de (juiste) code ingevoerd is, ontvangt de abonnementhouder zijn geld. Tot die tijd houden we dit geld vast op onze rekening.

## Gebruiker ratings

Het tweede mechanisme zijn de gebruiker ratings. Na afloop van een Meereis vragen we een meereiziger om de abonnementhouder te beoordelen. Vanaf 3 of meer ratings nemen we de gemiddelde beoordeling mee in zoeken naar matches. Abonnementhouders die goede ratings krijgen zullen verkozen worden boven abonnementhouders die slechte ratings hebben. Op die manier brengen we automatisch feedback van meereizigers terug in het systeem. Ook zorgt dit ervoor dat de beste abonnementhouders voordeel opbouwen en dat de slechtste abonnementhouders beter kunnen worden bekeken door ons.

# Betalingen

Voor de betalingen gebruiken we Paypal. Exact gesproken de Delayed Chained Payments die aangemaakt kunnen worden met de Adaptive Payments API. We gebruiken hiervoor de Adaptive Payments SDK voor PHP die Paypal op Github heeft staan.

## Betaling opzetten

Om een betaling op te zetten, vragen we deze eerst aan bij Paypal. Deze checkt of de bedragen kloppen, of de ontvanger niet ook de verstuurder is etc. Na goedkeuring ontvangen wij informatie over de betaling. Hieronder valt de Paykey en een betalings URL. De Paykey slaan we bij de Meereis op in de database. De gebruiker sturen we naar de betalings URL. Hier kan de gebruiker betalen in de omgeving van Paypal.

## Betaling verwerken

Wanneer er betaald is, wordt de gebruiker terug gestuurd naar onze website. Hierbij ontvangen we echter geen data. De 'Bedankt voor het betalen' pagina is dus slechts schijn. De echte verwerking van de betaling laat nog ongeveer 1 seconde op zich wachten. Als Paypal de betaling dan toch verwerkt heeft, sturen ze ons daar een bericht van. Dit heet een IPN (instant payment notification).

De SDK bevat ook functies om de IPN te valideren. Als blijkt dat het een geldige notification is, kunnen we verder. De notification bevat informatie over de betaling, waaronder de Paykey. Deze gebruiken wij om de Meereis weer op te zoeken. We updaten de status dan naar 'betaald' en sturen wat mailtjes en/of notifications naar beide reizigers. Voor het mailtje naar de meereiziger genereren we ook een verificatiecode. Deze wordt ook door de IPN listener opgeslagen in de database.

## IPN listener en development

De IPN's kunnen alleen verstuurd worden naar adressen op port 80. Daarom heb ik een proxy gemaakt die op www.vleckanie.nl/ipn.php staat. Deze stuurt de IPN door naar Meereizen. Dit zorgt er echter wel voor dat het IPN verificatie systeem niet meer werkt. Dit heb ik daarom tijdelijk uitgeschakeld maar het is van groot belang dat deze wel weer aan gezet wordt zodra we openbaar gaan.

Ook lijkt het me een goed idee om de servers van Paypal te whitelisten zodat niemand anders onze IPN listener uberhaupt kan bereiken.

# Mobiele app

## Phonegap

De app die we gaan maken, zal gemaakt worden met Phonegap. Dit is een systeem dat HTML en Javascript gebaseerde apps kan compilen naar native applicaties voor iOS, Android en Windows Phone. Dit compilen laten we doen door de servers van Adobe, via de zogenaamde Phonegap Build service.

De uiteindelijke app is dus eigenlijk niks meer dan een fullscreen browser die een lokale pagina laat zien. Wel heeft de app toegang tot alles waar een app normaal gesproken ook toegang tot heeft, zoals het notification center en de geografische locatie van de gebruiker.

Onze app zal naar http://www.meereizen.nu/ leiden. Hij laat dus gewoon de mobiele versie van de website zien. Hierdoor hoeven we geen native taal te leren en ook geen API te maken om een mobiele client mee te laten werken.

## Mobiele website

De mobiele website moet zo goed en native mogelijk overkomen op de gebruiker. Het liefst zorgen we er ook voor dat de website responsive is, zodat elke gebruiker meteen een versie van de website voorgeschoteld krijgt die perfect geschikt is voor zijn of haar schermgrootte.

In de versie van de mobiele website die we op het moment van schrijven gebruiken, wordt de [SwipeJS][3] plugin gebruikt om voor de drie swipe-able homepage panels te zorgen.

# Analyseren

## Admin panel

Op het moment van schrijven is dit nog een toekomstdroom maar het is leuk om over na te denken: het admin panel. Dit wordt een pagina waarop wij, de beheerders van Meereizen, alle gebeurtenissen bij kunnen houden. Het zal een real-time systeem worden, wat inhoudt dat alle data niet meer dan een seconde oud is en de pagina niet herladen hoeft te worden om nieuwe gegevens op te halen. Zodra er iets gebeurt zal het admin panel dat laten zien.

Naast gebeurtenissen zoals betalingen, nieuwe gebruikers, reis aanvragen en zoekopdrachten zal er ook andere data te zien zijn. Hierbij moet je denken aan het populairste traject, de populairste abonnementhouder, de groeifactor van de reisaanvragen ten opzichte van vorige maand.

## Google Analytics

Voor gegevens over de bezoekers van de website, zoals de demogafie of het aantal bezoekers per dag zullen we Google Analytics gebruiken. Het mooiste zou zijn als we die data ook nog eens zouden kunnen koppelen aan het admin panel, zodat we werkelijk alles op 1 pagina kunnen zien.

[1]: http://gilbitron.github.io/PIP/
[2]: http://opauth.org/
[3]: http://swipejs.com/