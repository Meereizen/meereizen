<?php

class Model {

	public $connection;

	public function __construct()
	{
		global $config;
		
		$this->connection = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
	}
	
}
?>