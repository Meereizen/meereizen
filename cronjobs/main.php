<?php
include ('../application/config/config.php');
chdir(dirname(__FILE__));

ini_set('display_startup_errors', 0);
ini_set('display_errors', 0);
ini_set('error_log', '/var/log/apache2/error.log');

try {
	// Verbinding maken met de database
    $dbh = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);

	require_once('../application/helpers/log_helper.php');
	$logHelper = new Log_helper();

    // Verlopen Meereizen deleten en geld terug storten
    include('verlopen.php');

    $dbh = null;

} catch (PDOException $e) {
	error_log('Cronjob eror: '.$e->getMessage());
    echo "Error!: " . $e->getMessage() . "<br/>";
    die();
}

?>