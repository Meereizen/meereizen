<?php

$sqlQuery = "	SELECT
					id,
					betaald,
					paykey
				FROM meereizen
				WHERE TIMEDIFF(vertrektijd, NOW()) < -14;";
$select = $dbh->prepare($sqlQuery);

if(!$select->execute())
{
	echo 'Kon niet checken welke reizen verlopen zijn.';
}
else
{
	require_once('../application/plugins/paypal/paypal.php');
	require_once('../application/helpers/payment_helper.php');
	$paymentHelper = new Payment_helper(TRUE);

	$verlopenMeereizen = $select->fetchAll(PDO::FETCH_ASSOC);

	foreach ($verlopenMeereizen as $verlopenMeereis) {
		if($verlopenMeereis['betaald'] == 1)
		{
			// Hier op de een of andere manier een refund doen..
			if($paymentHelper->refundPayment($verlopenMeereis['paykey']))
			{
				$logHelper->log('refund', 'Geld voor Meereis #'.$verlopenMeereis['id'].' terug gestort.');
			}
			else
			{
				echo 'Refund voor Meereis #'.$verlopenMeereis['id'].' is mislukt.';
			}
		}

		$sqlQuery = "	DELETE
						FROM meereizengebruikerlinks
						WHERE meereis = :id;";
		$delete = $dbh->prepare($sqlQuery);
		$delete->bindParam(':id', $verlopenMeereis['id'], PDO::PARAM_STR);

		if(!$delete->execute())
		{
			echo 'Deleten van meereislinks bij Meereis #'.$verlopenMeereis['id'].' mislukt.';
		}
		else
		{
			$sqlQuery = "	DELETE
							FROM meereizen
							WHERE id = :id;";
			$delete = $dbh->prepare($sqlQuery);
			$delete->bindParam(':id', $verlopenMeereis['id'], PDO::PARAM_STR);

			if(!$delete->execute())
			{
				echo 'Deleten van Meereis #'.$verlopenMeereis['id'].' mislukt.';
			}
			else
			{
				$logHelper->log('delete', 'Meereis #'.$verlopenMeereis['id'].' is verwijderd.');
			}
		}
	}
}
?>