<?php

/**
* IPN Controller
*
* Locatie waar Paypal heen gaat om ons te vertellen dat de betaling gelukt is.
*
* @author	Olivier Jansen
* @package	Meereizen.nu
*/

class IPN extends Controller {

	// getPlanner() zet de $_POST gegevens van de planner om naar een bruikbare URL. Dit is de plek waar de planner als eerst heengestuurd wordt
	function index()
	{
		$logHelper = $this->loadHelper('log_helper');

		$paymentHelper = $this->loadHelper('payment_helper');

		$IPNResponse = $paymentHelper->IPN();

		// IPN is geldig en de transactie in incompleet (dat betekent dat het geld nog niet naar de secondary receiver overgemaakt is)
		if($IPNResponse['status'] == TRUE && $IPNResponse['data']['status'] == 'INCOMPLETE')
		{
			$meereisModel = $this->loadModel('Meereis_model');
			$abonnementhouderModel = $this->loadModel('abonnementhouder_model');
			
			$meereis = $meereisModel->getMeereisPaykey($IPNResponse['data']['pay_key']);
			if(!empty($meereis) && is_numeric($meereis['gebruiker']))
			{
				if($meereisModel->setBetaald($IPNResponse['data']['pay_key']))
				{
					// De mail shizzle laden
					$mailHelper = $this->loadHelper('mail_helper');
					$this->loadPlugin('PHPMailer/class.phpmailer');
					$mail = new PHPMailer();

					// Gegevens van abonnementhouder opvragen
					$abonnementhouderData = $abonnementhouderModel->getPublicData($meereis['gebruiker']);
		
					// Emailadressen array
					$emails = Array();
					$emails['abonnementhouder'] = $abonnementhouderData['email'];
					$emails['reiziger'] = $meereis['email'];

					// De gegevens die verstuurd moeten worden.
					$gegevens = Array();
	  				$gegevens['abonnementhouder'] = Array();
	  				$gegevens['abonnementhouder']['naam'] = $abonnementhouderData['naam'];
		   			$gegevens['abonnementhouder']['geslacht'] = $abonnementhouderData['geslacht'];
		   			$gegevens['abonnementhouder']['email'] = $abonnementhouderData['email'];
		   			$gegevens['abonnementhouder']['telefoon'] = $abonnementhouderData['telefoon'];
		   			$gegevens['abonnementhouder']['foto'] = $abonnementhouderData['foto'];

		   			// Gegevens van de reiziger komen uit de data array die alle data van de Meereis bevat
		   			$gegevens['reiziger'] = Array();
		   			$gegevens['reiziger']['email'] = $meereis['email'];
		   			$gegevens['reiziger']['telefoon'] = $meereis['telefoon'];

		   			// Code genereren die de reiziger aan de abonnementhouder over zal moeten dragen
		   			$gegevens['code'] = mt_rand(10000000,99999999);

		   			// Code in database zetten
		   			if($meereisModel->insertVerificatieCode($meereis['id'], $gegevens['code']))
		   			{
					    // Mail versturen
						if(!$mailHelper->verstuurBetaaldMail($gegevens, $mail, $emails))
						{
							error_log('Error: failed to send mail to '.$emails['abonnementhouder'].' en '.$emails['reiziger'].'.');
						}
									
						// Checken of abonnementhouder Android devices heeft
						$regIds = $abonnementhouderModel->getAllRegids($meereis['gebruiker']);
						if(count($regIds) > 0)
						{
							// Er zijn Android devices, laad de AndroidPusher plugin
							$this->loadPlugin('AndroidPusher/androidpusher.class');
				
							// Pusher instellen met onze Google API key
							$pusher = new Pusher(GOOGLE_API_KEY);
					
							// Alle RegIDs een notification sturen
							foreach($regIds as $regId)
							{
								// Push een bericht naar het device.
								$pusher->notify($regId['regid'], 'De reiziger heeft betaald!');
										
								// Checken of het succesvol geweest is.
								$pusherResult = $pusher->getOutputAsArray();
								if($pusherResult['failure']>0 && $pusherResult['success']==0)
								{
									error_log('Error: notification niet verstuurd. RegId kan verlopen zijn.');
								}
							}
						}

						$logHelper->log('betaling', 'Meereis #'.$meereis['id'].' is betaald.');
					}
					else
					{
						error_log('IPN ontvangen, Meereis als betaald gemarkeerd maar kon verificatiecode niet invoeren.');
					}
				}
				else
				{
					error_log('IPN ontvangen maar kon Meereis niet als betaald markeren.');
				}
			}
			else
			{
				error_log('IPN ontvangen maar op basis van paykey ('.$IPNResponse['data']['pay_key'].') geen bijbehorende Meereizen gevonden.');
			}
		}
	}
}