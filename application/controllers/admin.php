<?php

class Admin extends Controller {
	
	function index($regid=null)
	{	
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$id = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($id) && $abonnementhouderModel->isAdmin($id))
			{
				$statistiekModel = $this->loadModel('Statistiek_model');

				$data = Array();
				$data['count']['meereizen_totaal'] = $statistiekModel->countMeereizenTotaal();
				$data['count']['meereizen_afgerond'] = $statistiekModel->countMeereizenAfgerond();
				$data['count']['meereizen_verwijderd'] = $statistiekModel->countMeereizenVerwijderd();
				$data['count']['meereizen_refund'] = $statistiekModel->countMeereizenRefund();

				$data['count']['betalingen'] = $statistiekModel->countBetalingen();

				$data['count']['gebruikers_twitter'] = $statistiekModel->countGebruikers('Twitter');
				$data['count']['gebruikers_facebook'] = $statistiekModel->countGebruikers('Facebook');
				$data['count']['gebruikers_google'] = $statistiekModel->countGebruikers('Google');
				$data['count']['gebruikers'] = $data['count']['gebruikers_facebook'] + $data['count']['gebruikers_twitter'] + $data['count']['gebruikers_google'];

				$data['event'] = $statistiekModel->getLatestEvents(25);

				$template = $this->loadView('admin');
				$template->set('data', $data);
				$template->render();
			}
			else
			{
				$template = $this->loadView('404');
				$template->render();
			}
		}
		else
		{
			$template = $this->loadView('404');
			$template->render();
		}

	}
	
}

?>
