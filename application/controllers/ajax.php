<?php

class ajax extends Controller
{
	
	public function stations()
	{
		header('Content-Type: text/xml');
		$this->loadHelper('ns_helper');
		$NSHelper = new NS_helper();
		
		$stations = $NSHelper->getStationList();

		echo $stations;
	}

	public function openstaandeReizenCount()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			$meereisModel = $this->loadModel('Meereis_model');
			$result = $meereisModel->countMeereizenUserOpen($loginHelper->getCurrentId());
			echo '<div>'.$result.'</div>';
		}
	}
	
	public function uitgelicht($type)
	{
		switch($type)
		{
			case 'vandaag':
				$num = 3;
				$trajectModel = $this->loadModel('Traject_model');	

				$trajecten = $trajectModel->getDayMatches(date('N'), $num, true);// Weekdag, aantal trajecten en wel/geen prijzen berekenen
				
				$dayofweek = Array(
					1 => 'maandag',
					2 => 'dinsdag',
					3 => 'woensdag',
					4 => 'donderdag',
					5 => 'vrijdag',
					6 => 'zaterdag',
					7 => 'zondag'
				);
				$weekday = date('N');
				$weekday = $dayofweek[$weekday];

				for($i = 0; $i <= ($num - 1); $i++)
				{
					if(array_key_exists($i, $trajecten))
					{
						$traject = $trajecten[$i];
						?><a class="traject-overview" href="<?php echo BASE_URL;?>reis/plannen/<?php echo urlencode($traject['van']); ?>/<?php echo urlencode($traject['naar']); ?>/<?php echo date('Y-m-d', $traject['timestamp']); ?>/<?php echo date('H:i', strtotime($traject['vertrektijd'])); ?>">
							<span>
								<span class="station">
									<?php echo $traject['van']; ?>
								</span><span class="divider"><i class="icon-angle-right"></i></span><span class="station">
									<?php echo $traject['naar']; ?>
								</span>
							</span>
							<span class="mobile-only right">&euro; <?php echo $traject['vol_tarief'] - $traject['nieuw_tarief'];?> korting</span>
							<span class="half date"><i class="desktop-only"><?php echo $weekday; ?></i><span><?php echo date('H:i', strtotime($traject['vertrektijd'])); ?></span></span><span class="half price desktop-only"><strike>&euro; <?php echo $traject['vol_tarief'];?></strike><span class="desktop-only">&euro; <?php echo $traject['nieuw_tarief'];?></span></span>
						</a><?
					}
				}

				break;
		}
	}
	
	public function prijs($van, $naar, $time)
	{
		header('Content-Type: text/xml');
		$this->loadHelper('ns_helper');
		$NSHelper = new NS_helper();
		
		$prijs = $NSHelper->getPrijs($van, $naar, $time);

		echo $prijs;
	}
	
	public function prijsJSON($from, $to, $dateTime)
	{
		header('Content-type: application/json');

		// Data laden (zie de functie hiervoor)
		$xmlObject = simplexml_load_file(BASE_URL.'/ajax/prijs/'.urlencode(urldecode($from)).'/'.urlencode(urldecode($to)).'/'.$dateTime);
		
		// Lege array opzetten
		$array = Array();

		// Volledige prijs van traject:
		$array['vol_tarief'] = number_format(str_replace(',', '.', $xmlObject->Product[0]->Prijs[0]),2);

		$prijs40Korting = str_replace(',', '.', $xmlObject->Product[0]->Prijs[2]);
		
		// Het bedrag dat betaald gaat worden (vol tarief + meereizen bedrag):
		$array['nieuw_tarief'] = number_format($prijs40Korting + $array['vol_tarief'] * (MEEREIZEN_VOOR_MEEREIZEN + MEEREIZEN_VOOR_ABONNEMENTHOUDER),2);

		echo json_encode($array);
	}

	public function tussenstations($van, $naar)
	{
		header('Content-Type: application/json');
		$this->loadHelper('ns_helper');
		$NSHelper = new NS_helper();
		
		$tussenStations = $NSHelper->getTussenstations($van, $naar);

		echo $tussenStations;
	}

}

?>