<?php

/**
* Reis Controller
*
* Bevat alle logica voor alles wat betreft de reizen en het plannen van een reis.
* In de code wordt onderscheid gemaakt tussen 'plannen' en 'zoeken'. Plannen is gebruikt makend van de reisplanner (vertrek- en aankomststation + tijden) en maakt gebruik van de NS API om alle reismogelijkheden te weergeven, ook als er geen matchend traject in de database staat. Zoeken is slechts op basis van traject of vertrekstation en weergeeft alleen de matches.
*
* @author	Olivier Jansen
* @package	Meereizen.nu
*/

class Reis extends Controller {	
	
	// getPlanner() zet de $_POST gegevens van de planner om naar een bruikbare URL. Dit is de plek waar de planner als eerst heengestuurd wordt
	function getPlanner()
	{
		// Checken of er wel data verstuurd is
		if(isset($_POST['van']) && isset($_POST['naar']) && isset($_POST['tijd']) && !empty($_POST['van']) && !empty($_POST['naar']) && !empty($_POST['tijd']))
		{
			$datum = date('Y-m-d', strtotime($_POST['tijd']));
			$tijd = date('H:i', strtotime($_POST['tijd']));
			
			// Doorsturen naar de juiste URL, functie plannen() in deze controller
			$this->redirect('reis/plannen/'.$_POST['van'].'/'.$_POST['naar'].'/'.$datum.'/'.$tijd);
		}
		elseif(isset($_POST['van']) && isset($_POST['naar']) && (!isset($_POST['tijd']) || empty($_POST['tijd'])))
		{
			$this->redirect('reis/zoeken/'.$_POST['van'].'/'.$_POST['naar']);
		}
		elseif(isset($_POST['van']))
		{
			$this->redirect('reis/zoeken/'.$_POST['van']);
		}
		else
		{
			$template = $this->loadView('customerror');
			$template->set('errorMessage', 'Er is iets mis met de informatie die je opgegeven hebt. Heb je alle velden ingevuld?');
			$template->render();
		}
	}
	
	// getZoeker() zet de $_POST gegevens van het zoek form om naar een bruikbare URL. Dit is de plek waar het form als eerst heengestuurd wordt.
	function getZoeker()
	{
		// Checken of er wel data verstuurd is
		if(isset($_POST['van']) && isset($_POST['naar']))
		{
			$this->redirect('reis/zoeken/'.$_POST['van'].'/'.$_POST['naar']);
		}
		elseif(isset($_POST['van']))
		{
			$this->redirect('reis/zoeken/'.$_POST['van']);			
		}
		else
		{
			$template = $this->loadView('customerror');
			$template->set('errorMessage', 'Er is iets mis met de informatie die je opgegeven hebt. Heb je alle velden ingevuld?');
			$template->render();
		}
	}
	
	// plannen() doet het daadwerkelijke planproces
	function plannen($from='Utrecht+Centraal', $to='Almere+Muziekwijk',$date=null,$time=null)
	{		
		// Als er geen tijd meegegeven is, wordt de huidige tijd genomen.
		if(empty($date) || empty($time))
		{
			$date = date('d-m-Y');
			$time = date('H:i');
		}
		else
		{
			$time = $time.':00';
		}
		
		if(date('H', strtotime($time)) < 9 && date('H', strtotime($time)) > 3)
		{
			$template = $this->loadView('customerror');
			$template->set('errorMessage', 'De reistijd die u opgegeven heeft is ongeldig. Met abonnementen van de NS is meereizen met korting voor 9 uur \'s ochtends niet toegestaan.');
			$template->render();
		}
		else
		{
			// Laad de NS helper, die alle API functies bevat
			$this->loadHelper('ns_helper');
			$NSHelper = new NS_helper();
			
			// Laad het Traject model, dat de matches zal zoeken in de database
			$trajectModel = $this->loadModel('Traject_model');
					
			// Geeft aan dat we als tijd de vertrektijd aangeven
			$departure = "true";
			
			// Datetime naar tijd converteren en daarna omzetten naar het gewenste formaat
			$timeTemp = strtotime($date.' '.$time);
			$dateTime = date("Y-m-d\TH:i:s\&plus;O", $timeTemp);
	
			// Reisplanner aanvragen
			$xmlObject = simplexml_load_string($NSHelper->getPlanner($from, $to, $dateTime));
	
			// Array met reismogelijkheden
			$reisMogelijkheden = $xmlObject->ReisMogelijkheid;
			
			// Array met results, de data die naar de view gepusht zal worden
			$reisMogelijkhedenResult = Array();
			
			$j=0;
			// Voor elke reismogelijkheid
			foreach($reisMogelijkheden as $reisMogelijkheid)
			{
				$j++;
				
				// Kijken of de reis niet op een ongeldige tijd is
				if((date('H', strtotime($reisMogelijkheid->ActueleVertrekTijd)) < 9 && date('H', strtotime($reisMogelijkheid->ActueleVertrekTijd)) > 3) || (strtotime($reisMogelijkheid->ActueleVertrekTijd) < time()))
				{
					continue;
				}
				
				// Results voor deze mogelijkheid die naar de view gepusht zullen worden
				$reisMogelijkheidResult = Array();
				
				// Basis informatie voor de view
				$reisMogelijkheidResult['id'] = $j;
				$reisMogelijkheidResult['date'] = date('d-m-Y H:i', strtotime($reisMogelijkheid->ActueleVertrekTijd));
				$reisMogelijkheidResult['vertrekTijd'] = date('H:i', strtotime($reisMogelijkheid->ActueleVertrekTijd));
				$reisMogelijkheidResult['aankomstTijd'] = date('H:i', strtotime($reisMogelijkheid->ActueleAankomstTijd));
				$reisMogelijkheidResult['reisTijd'] = $reisMogelijkheid->ActueleReisTijd;
				$reisMogelijkheidResult['overstappen'] = $reisMogelijkheid->AantalOverstappen;
				
				$dayofweek = date('N', strtotime($reisMogelijkheid->ActueleVertrekTijd));
	
				// Tijdsinterval voor de matches maken
				$time1 = date('H:i:s', strtotime($reisMogelijkheid->ActueleVertrekTijd) - ( 60 * 8 )); // Huidige tijd min 8 minuten
				$time2 = date('H:i:s', strtotime($reisMogelijkheid->ActueleVertrekTijd) + ( 60 * 8 )); // Huidige tijd min 8 minuten
	
				// Het aantal 'directe' matches opzoeken
				$reisMogelijkheidResult['matches'] = $trajectModel->countDirectMatches($from, $to, $time1, $time2, $dayofweek);
				
				// Alle reisdelen uit het XML bestand halen en wat informatie voor de view aanmaken
				$reisDelen = $reisMogelijkheid->ReisDeel;
				$reisMogelijkheidResult['aantalReisDelen'] = count($reisDelen);
				$reisMogelijkheidResult['reisDelen'] = Array();
				
				$i = 0;
				// Voor elk reisdeel..
				foreach($reisDelen as $reisDeel)
				{
					// ..een array aanmaken met alle stops
					$reisMogelijkheidResult['reisDelen'][$i] = Array();
	
					$h = 0;
					// Voor elke stop..
					foreach($reisDeel->ReisStop as $station)
					{
						// ..deze stop toevoegen aan de array met stops
						$reisMogelijkheidResult['reisDelen'][$i][$h]['tijd'] = date('H:i', strtotime($station->Tijd));
						$reisMogelijkheidResult['reisDelen'][$i][$h]['naam'] = (string) $station->Naam;
	
						if(isset($station->Spoor))
						{
							$reisMogelijkheidResult['reisDelen'][$i][$h]['spoor'] = (string) $station->Spoor;
						}
						$h++;
					}
					$i++;
				}
				
				if($reisMogelijkheidResult['matches']>0)
				{
					// De results van deze mogelijkheid aan de grote array met alle results koppelen
					$reisMogelijkhedenResult[] = $reisMogelijkheidResult;
				}
			}
			
			$matches = 0;
			// Ga elk resultaat af
			foreach($reisMogelijkhedenResult as $result)
			{
				// Als er een match is voor dit resultaat
				if($result['matches']>0)
				{
					// Als dit de enige match is, voeg dan de redirect info toe
					if($matches==0)
					{
						$redirect = $result;
					}
					// Hoog dan de matches op
					$matches++;
				}
			}
			error_log($matches .' matches');
			// Checken of de matches nog steeds 1 zijn, dan kunnen we namelijk meteen doorsturen naar die mogelijkheid
			if($matches==1 && !empty($redirect) && is_array($redirect))
			{
				$this->redirect('reis/details/'.$from.'/'.$to.'/'.date('Y-m-d', strtotime($redirect['date'])).'/'.$redirect['vertrekTijd'].'/'.$redirect['aankomstTijd']);
			}
			
			$dateplustime = strtotime($date.' '.$time);
	
			$dateEarlier = date("Y-m-d", $dateplustime - 60*60);
			$timeEarlier = date("H", $dateplustime - 60*60);
	
			$dateLater = date("Y-m-d", $dateplustime + 60*60);
			$timeLater = date("H", $dateplustime + 60*60);
			
			$url_earlier = BASE_URL.'reis/plannen/'.$from.'/'.$to.'/'.$dateEarlier.'/'.$timeEarlier;
			$url_later = BASE_URL.'reis/plannen/'.$from.'/'.$to.'/'.$dateLater.'/'.$timeLater;
			
			// View laden
			$template = $this->loadView('reis-plannen');
			
			// Variabelen naar de view pushen
			$template->set('from', $from);
			$template->set('to', $to);
			$template->set('date', $date);
			$template->set('time', $time);
			$template->set('url_earlier', $url_earlier);
			$template->set('url_later', $url_later);
			$template->set('results', $reisMogelijkhedenResult);
			$template->set('matches', $matches);
			
			// View renderen
			$template->render();
		}
	}
	
	// en zoeken() doet het daadwerkelijke zoekproces
	function zoeken($from='Utrecht+Centraal', $to=null)
	{
		// Laad het Traject model, dat de matches zal zoeken in de database
		$trajectModel = $this->loadModel('Traject_model');
		
		if(!empty($to))
		{
			// Als het een traject-zoekopdracht is, de matches ophalen met de volgende functie
			$matches = $trajectModel->getTrajectMatches($from, $to);

			// Lege array met matches op dag aanmaken
			$matchesPerDay = Array('maandag' => Array(), 'dinsdag' => Array(), 'woensdag' => Array(), 'donderdag' => Array(), 'vrijdag' => Array(), 'zaterdag' => Array(), 'zondag' => Array());
			$matchesPerDayEnglish = Array('maandag' => 'monday', 'dinsdag' => 'tuesday', 'woensdag' => 'wednesday', 'donderdag' => 'thursday', 'vrijdag' => 'friday', 'zaterdag' => 'saturday', 'zondag' => 'sunday');

			$i = 0;
			foreach($matches as $match)
			{
				foreach($matchesPerDay as $dag => $array)
				{
					if($match[$dag]==1)
					{
						$i++;
						array_push($matchesPerDay[$dag], $match);
						if($i==1)
						{
							$onlyMatch = $match;
							$onlyMatchDag = date('Y-m-d', strtotime('next '.$matchesPerDayEnglish[array_search($matchesPerDay[$dag], $matchesPerDay)]));
						}
					}
				}
			}

			if($i==1)
			{
				$this->redirect('reis/details/'.$from.'/'.$to.'/'.$onlyMatchDag.'/'.$onlyMatch['vertrektijd']);
			}

			// View laden
			$template = $this->loadView('reis-traject-zoeken');
			
			// Variabelen naar de view pushen
			$template->set('from', $from);
			$template->set('to', $to);
			$template->set('results', $matchesPerDay);
			
			// Render de view
			$template->render();
		}
		else
		{
			// Als het een vertrekstation-zoekopdracht is
			$matches = $trajectModel->getVertrekstationMatches($from);

			if(count($matches) == 1)
			{
				$this->redirect('reis/zoeken/'.$from.'/'.$matches[0]['naar']);
			}
			
			// View laden
			$template = $this->loadView('reis-vertrekstation-zoeken');
						
			// Variabelen naar de view pushen
			$template->set('from', $from);
			$template->set('results', $matches);
	
			// Render de view
			$template->render();

		}
	}
	
	function details($from='Utrecht+Centraal', $to='Almere+Muziekwijk',$date=null,$time=null,$timeAankomst=null)
	{
		// Als er geen tijd meegegeven is, wordt de huidige tijd genomen.
		if(empty($date) || empty($time))
		{
			$date = date('d-m-Y');
			$time = date('H:i');
		}
		
		if(date('H', strtotime($time)) < 9 && date('H', strtotime($time)) > 3)
		{
			$template = $this->loadView('customerror');
			$template->set('errorMessage', 'De reistijd die u opgegeven heeft is ongeldig. Met abonnementen van de NS is meereizen met korting voor 9 uur \'s ochtends niet toegestaan.');
			$template->render();
		}
		else
		{
			// Laad de NS helper, die alle API functies bevat
			$this->loadHelper('ns_helper');
			$NSHelper = new NS_helper();
			
			// Laad het Traject model, dat de matches zal zoeken in de database
			$trajectModel = $this->loadModel('Traject_model');
			
			// Datetime naar tijd converteren en daarna omzetten naar het gewenste formaat
			$timeTemp = strtotime($date.' '.$time);
			$dateTime = date("Y-m-d\TH:i:s\&plus;O", $timeTemp);
	
			// Reisplanner aanvragen
			$xmlObject = simplexml_load_string($NSHelper->getPlanner($from, $to, $dateTime, 0, 0));
	
			// Array met reismogelijkheden
			$reisMogelijkheden = $xmlObject->ReisMogelijkheid;
		
			if(empty($reisMogelijkheden))
			{
				error_log('Eerste poging om details op te halen mislukt.');
				error_log('Tijdsinterval aanpassen.');
			
				// De tijd herschrijven tot een breder interval
				$timeTemp = $timeTemp - 3 * 60;
				$dateTime = date("Y-m-d\TH:i:s\&plus;O", $timeTemp);
			
				// Reisplanner aanvragen
				$xmlObject = simplexml_load_string($NSHelper->getPlanner($from, $to, $dateTime, 1, 1));		
				// Array met reismogelijkheden
				$reisMogelijkheden = $xmlObject->ReisMogelijkheid;
				if(empty($reisMogelijkheden))
				{
					error_log('Tweede poging om details op te halen mislukt.');
					error_log('Tijdsinterval aanpassen.');

					// De tijd herschrijven tot een breder interval
					$timeTemp = $timeTemp + 3 * 60;
					$dateTime = date("Y-m-d\TH:i:s\&plus;O", $timeTemp);
				
					// Reisplanner aanvragen
					$xmlObject = simplexml_load_string($NSHelper->getPlanner($from, $to, $dateTime, 1, 1));		
					// Array met reismogelijkheden
					$reisMogelijkheden = $xmlObject->ReisMogelijkheid;
					if(empty($reisMogelijkheden))
					{
						error_log('Derde poging om details op te halen mislukt.');
						error_log('Tijdsinterval aanpassen.');

						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'Deze reis bestaat niet. Het kan zijn dat de abonnementhouder geen exacte vertrektijd heeft ingevoerd. Klik <a href="">hier</a> om de omliggende vertrektijden te bekijken.');
						$template->render();
						die();
					}
				}
				$closestReisTijd = 1000000;
				$closestReis = '';
				
				foreach($reisMogelijkheden as $reisMogelijkheid)
				{
					$vertrektijd = (array) $reisMogelijkheid->ActueleVertrekTijd;
					$vertrektijd = strtotime($vertrektijd[0]);
					$vertrektijd = abs(strtotime($date.' '.$time) - $vertrektijd);
					
					if($vertrektijd < $closestReisTijd)
					{
						$closestReisTijd = $vertrektijd;
						$closestReis = $reisMogelijkheid;
					}
				}
				$reisMogelijkheden = Array();
				$reisMogelijkheden[] = $closestReis;				
			}

			// Array met results, de data die naar de view gepusht zal worden
			$reisMogelijkhedenResult = Array();
				
			$j=0;
			// Voor elke reismogelijkheid
			foreach($reisMogelijkheden as $reisMogelijkheid)
			{
					$j++;
					
					// Kijken of de reis niet op een ongeldige tijd is of dat de opgegeven aankomsttijd niet overeenkomt met de NS data
					if((date('H', strtotime($reisMogelijkheid->ActueleVertrekTijd)) < 9 && date('H', strtotime($reisMogelijkheid->ActueleVertrekTijd)) > 3) || (strtotime($reisMogelijkheid->ActueleVertrekTijd) < time()) || date("H:i", strtotime($reisMogelijkheid->ActueleAankomstTijd)) !== date("H:i", strtotime($timeAankomst)))
					{
						if(!empty($timeAankomst))
						{
							continue;
						}
					}
					
					// Results voor deze mogelijkheid die naar de view gepusht zullen worden
					$reisMogelijkheidResult = Array();
					
					// Basis informatie voor de view
					$reisMogelijkheidResult['id'] = $j;
					$reisMogelijkheidResult['date'] = date('d-m-Y H:i', strtotime($reisMogelijkheid->ActueleVertrekTijd));
					$reisMogelijkheidResult['vertrekTijd'] = date('H:i', strtotime($reisMogelijkheid->ActueleVertrekTijd));
					$reisMogelijkheidResult['aankomstTijd'] = date('H:i', strtotime($reisMogelijkheid->ActueleAankomstTijd));
					$reisMogelijkheidResult['reisTijd'] = $reisMogelijkheid->ActueleReisTijd;
					$reisMogelijkheidResult['overstappen'] = $reisMogelijkheid->AantalOverstappen;
					
					$dayofweek = date('N', strtotime($reisMogelijkheid->ActueleVertrekTijd));
		
					// Tijdsinterval voor de matches maken
					$time1 = date('H:i:s', strtotime($reisMogelijkheid->ActueleVertrekTijd) - ( 60 * 8 )); // Huidige tijd min 8 minuten
					$time2 = date('H:i:s', strtotime($reisMogelijkheid->ActueleVertrekTijd) + ( 60 * 8 )); // Huidige tijd min 8 minuten
		
					// Het aantal 'directe' matches opzoeken
					$reisMogelijkheidResult['matches'] = $trajectModel->countDirectMatches($from, $to, $time1, $time2, $dayofweek);
					
					// Alle reisdelen uit het XML bestand halen en wat informatie voor de view aanmaken
					$reisDelen = $reisMogelijkheid->ReisDeel;
					$reisMogelijkheidResult['aantalReisDelen'] = count($reisDelen);
					$reisMogelijkheidResult['reisDelen'] = Array();
					
					$i = 0;
					// Voor elk reisdeel..
					foreach($reisDelen as $reisDeel)
					{
						// ..een array aanmaken met alle stops
						$reisMogelijkheidResult['reisDelen'][$i] = Array();
		
						$h = 0;
						// Voor elke stop..
						foreach($reisDeel->ReisStop as $station)
						{
							// ..deze stop toevoegen aan de array met stops
							$reisMogelijkheidResult['reisDelen'][$i][$h]['tijd'] = date('H:i', strtotime($station->Tijd));
							$reisMogelijkheidResult['reisDelen'][$i][$h]['naam'] = (string) $station->Naam;
		
							if(isset($station->Spoor))
							{
								$reisMogelijkheidResult['reisDelen'][$i][$h]['spoor'] = (string) $station->Spoor;
							}
							$h++;
						}
						$i++;
					}
					
					// De results van deze mogelijkheid aan de grote array met alle results koppelen
					$reisMogelijkhedenResult[] = $reisMogelijkheidResult;
			}
			
			// Dirty fix. Er mag maar 1 resultaat zijn, dus selecteer de eerste.
			$reisMogelijkhedenResult = $reisMogelijkhedenResult[0];
			
			$dateplustime = strtotime($date.' '.$time);
		
			// View laden
			$template = $this->loadView('reis-details');
				
			// Variabelen naar de view pushen
			$template->set('from', $from);
			$template->set('to', $to);
			$template->set('date', $date);
			$template->set('dateParsed', $dateTime);
			$template->set('time', $time);
			$template->set('result', $reisMogelijkhedenResult);
				
			// View renderen
			$template->render();
		}
	}
	
	function aanvragen()
	{
		if(!isset($_POST['datum']) || !isset($_POST['van']) || !isset($_POST['naar']) || !isset($_POST['vertrektijd']) || !isset($_POST['aankomsttijd']) || !isset($_POST['reistijd']) || !isset($_POST['overstappen']) || !isset($_POST['email']) || !isset($_POST['telefoon']))
		{
			$this->redirect('404');
		}
		else
		{
			if(empty($_POST['email']) || empty($_POST['telefoon']))
			{
				$template = $this->loadView('customerror');
				$template->set('errorMessage', 'Voer alstublieft een geldig e-mailadres en telefoonnummer in. Dit is voor uw eigen voordeel aangezien de abonnementhouder deze contactgegevens zal gebruiken om u te bereiken.');
				$template->render();
			}
			else
			{
				// We hebben de trajecten en abonnementenmodelen nodig
				$trajectModel			= $this->loadModel('Traject_model');
				$abonnementhouderModel	= $this->loadModel('Abonnementhouder_model');
				$meereisModel			= $this->loadModel('Meereis_model');
				
				// Alle POST data omzetten naar variabelen.
				$email 			= $_POST['email'];
				$telefoon 		= $_POST['telefoon'];
	
				$datum 			= $_POST['datum'];
				$datumParsed 	= $_POST['datumparsed'];
				$van 			= $_POST['van'];
				$naar			= $_POST['naar'];
				$vertrektijd 	= $_POST['vertrektijd'];
				$aankomsttijd 	= $_POST['aankomsttijd'];
				$reistijd		= $_POST['reistijd'];
				$overstappen	= $_POST['overstappen'];
				
				$vertrektijd	= date("Y-m-d H:i:s", strtotime($datum));
				$tijd1			= date('H:i:s', strtotime($vertrektijd) - ( 60 * 10 ));
				$tijd2			= date('H:i:s', strtotime($vertrektijd) + ( 60 * 10 ));
				$dayofweek		= date('N', strtotime($datum));
				
				$regid = 0;
				if(isset($_SESSION['regid'])) $regid = $_SESSION['regid'];
				
				// Checken of er nog steeds een match is.
				$matchesCount = $trajectModel->countDirectMatches($van, $naar, $tijd1, $tijd2, $dayofweek);
				if($matchesCount < 1)
				{
					// Zo nee, dan is er iets fout gegaan.
					$template = $this->loadView('customerror');
					$template->set('errorMessage', 'Er is iets mis gegaan. Op dit moment konden wij geen match vinden. Doet dit probleem zich vaker voor? Neem dan contact met ons op.');
					$template->render();
				}
				else
				{
					// Meer data over de match(es) ophalen.
					$matches = $trajectModel->getDirectMatches($van, $naar, $tijd1, $tijd2, $dayofweek);
	
					// Het ID van het traject ophalen.
					$trajectId = $trajectModel->getTrajectId($van, $naar);
	
					$prijzen = BASE_URL.'ajax/prijsJSON/'.urlencode($van).'/'.urlencode($naar).'/'.$datumParsed;
					$prijzen = json_decode(file_get_contents($prijzen),true);
	
					// Een nieuwe DB entry maken.
					$meereisId = $meereisModel->insertMeereis($trajectId, $vertrektijd, $email, $telefoon, $prijzen['vol_tarief'], $prijzen['nieuw_tarief'], $regid);
					if($meereisId)
					{
						// De mail shizzle alvast laden
						$mailHelper = $this->loadHelper('mail_helper');
					    $this->loadPlugin('PHPMailer/class.phpmailer');
					    $mail = new PHPMailer();
	
						// Er kunnen meerdere matches zijn, ga ze allemaal af.
						foreach($matches as $match)
						{
							error_log($match['gebruiker'].' toevoegen link. #'.$meereisId);

							// Elke abonnementhouder koppelen aan de Meereis in de db.
							$meereisModel->insertMeereisLink($meereisId, $match['gebruiker']);
						
							// Elke match (is abonnementhouder) moeten we een mail en mogelijk een notification sturen.
	
						    // De URL opgeven waar de abonnementhouder naar verwezen wordt.
	   						$url = BASE_URL.'abonnementhouder/';
	
	   						$abonnementhouderEmail = $abonnementhouderModel->getEmail($match['gebruiker']);
	
						    // Mail versturen
							if($mailHelper->verstuurMeereisMail($url, $mail, $abonnementhouderEmail, $van, $naar))
							{
								error_log('Mail verstuurd naar '.$abonnementhouderEmail.'.');
							}
							else
							{
								error_log('Error: failed to send mail to '.$abonnementhouderEmail);
							}
							
							// Checken of abonnementhouder Android devices heeft
							$regIds = $abonnementhouderModel->getAllRegids($match['gebruiker']);
							if(count($regIds) > 0)
							{
								if(!class_exists('Pusher')) {
									// Er zijn Android devices, laad de AndroidPusher plugin
									$this->loadPlugin('AndroidPusher/androidpusher.class');									
								}
		
								// Pusher instellen met onze Google API key
								$pusher = new Pusher(GOOGLE_API_KEY);
			
								// Alle RegIDs een notification sturen
								foreach($regIds as $regId)
								{
									error_log($regId['regid']);
									// Push een bericht naar het device.
									$pusher->notify($regId['regid'], 'Er wil een reiziger met u Meereizen!');
										
									// Checken of het succesvol geweest is.
									$pusherResult = $pusher->getOutputAsArray();
									if($pusherResult['failure']==0 && $pusherResult['success']>0)
									{
										error_log('Notification verstuurd.');
									}
									else
									{
										error_log('Error: notification niet verstuurd. RegId kan verlopen zijn.');
									}
								}
							}
						}
						$template = $this->loadView('reis-aangevraagd');
						$template->render();

						$logHelper = $this->loadHelper('log_helper');
						$logHelper->log('reisAanvraag', 'Er is een reis aangevraagd.');
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'Er is iets mis gegaan. Het lukte niet om de reis in onze database te zetten. Om deze reden hebben we geen abonnementhouders gecontacteerd. Probeer het opnieuw en neem contact met ons op als dit probleem zich voor blijft doen.');
						$template->render();
					}
	
				}
			}
		}
	}
	
	function betalen($meereisId=null, $code=null)
	{
		// Als er geen URL parameters opgegeven zijn
		if($meereisId!==null && $code!==null)
		{
			$meereisModel = $this->loadModel('Meereis_model');
			
			// Checken of een Meereis met dit ID en deze code bestaat en of de code juist is.
			if($meereisModel->isPayMeereis($meereisId, $code))
			{
				// Alle data van de Meereis laden.
				$data = $meereisModel->getMeereizenId($meereisId);

				// Kijken of er al op 'Betaal' geklikt is.
				if(!isset($_POST['submit']))
				{
					$template = $this->loadView('reis-betalen');
							
					$data['prijs_voor_meereizen'] = (MEEREIZEN_VOOR_MEEREIZEN + MEEREIZEN_VOOR_ABONNEMENTHOUDER) * $data['vol_tarief'];
							
					$template->set('data', $data);
					$template->render();
				}
				else
				{
					// Laad het abonnementhouder model
					$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
					
					// Paypal rekening ophalen van de gebruiker die bij de Meereis ingeschreven staat.
					$abonnementhouderPaypal = $abonnementhouderModel->getPaypal($data['gebruiker']);
					
					// Paypal adressen van de ontvangers van het geld
					$ontvangers = Array(
						'primary' => MEEREIZEN_PAYPAL,
						'secondary' => $abonnementhouderPaypal
					);
					
					// De bedragen die betaald worden
					$bedragen = Array(
						'primary' => (MEEREIZEN_VOOR_MEEREIZEN + MEEREIZEN_VOOR_ABONNEMENTHOUDER) * $data['vol_tarief'],
						'secondary' =>  MEEREIZEN_VOOR_ABONNEMENTHOUDER * $data['vol_tarief']
					);

					// De helper die betalingen afhandelt laden
					$paymentHelper = $this->loadHelper('payment_helper');
					
					$cancelUrl = BASE_URL.'reis/betalen/'.$meereisId.'/'.$code;
					$returnUrl = BASE_URL.'reis/betaald';
					$ipn = 'http://www.oli4jansen.nl/ipn.php';

					// Betaald via een Paypal Chained payments. Ontvangers en bedragen als array aanleveren.
					$betaling = $paymentHelper->payPaypalDelayedChained($ontvangers, $bedragen, $cancelUrl, $returnUrl, $ipn);
					if($betaling['status'] == TRUE)
					{
						// Sla de paykey op in de database
						if($meereisModel->setPaykey($meereisId, $betaling['paykey']))
						{
							header('Location: '.$betaling['url']);

						}
						else
						{
							$template = $this->loadView('customerror');
							$template->set('errorMessage', 'De betaling is mislukt. We kregen een goedkeuring van Paypal maar de database weigerde dit op te slaan. Neem alstublieft contact met ons op.');
							$template->render();
						}
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'De betaling is mislukt. We kregen van Paypal de volgende terug: "'.$betaling['message'].'" Probeer het nog eens en neem contact met ons op als dit probleem zich voor blijft doen.');
						$template->render();							
					}
				}
			}
			else
			{
				$this->redirect('main/index');
			}
		}
		else
		{
			$this->redirect('main/index');
		}
	}

	public function betaald()
	{
		$template = $this->loadView('reis-betalen-bedankt');
		$template->render();
	}
}

?>