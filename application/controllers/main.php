<?php

class Main extends Controller {
	
	function index($regid=null, $loggedIn=null)
	{	
		if(isset($regid) && $regid !== null)
		{
			$_SESSION['regid'] = $regid;
		}

		if(isset($loggedIn) && $loggedIn !== null)
		{
			?>
			<script>
				if( window.innerWidth > 700 ) {
					window.location.assign("<?php echo BASE_URL; ?>abonnementhouder")
				}
			</script>
			<?php
		}

		$template = $this->loadView('main');
		$template->set('planner', 'true');
		$template->render();
	}
	
	function uitgelicht()
	{	
		$template = $this->loadView('uitgelicht');
		$template->render();
	}
}

?>
