<?php

/**
* Docs Controller
*
* @author	Olivier Jansen
* @package	Meereizen.nu
*/

class Docs extends Controller {	
	
	// getPlanner() zet de $_POST gegevens van de planner om naar een bruikbare URL. Dit is de plek waar de planner als eerst heengestuurd wordt
	public function index()
	{
		$this->redirect('docs/page/algemeen/introductie');
	}
	
	public function page($categorie, $onderwerp)
	{
		$template = $this->loadView('docs');

		$data['title'] = ucfirst($onderwerp);
		
		$data['body'] = $this->getDocs($categorie, $onderwerp);

		$template->set('data', $data);

		$template->render();
	}
	
	private function getDocs($categorie, $onderwerp)
	{
		$bodyLocation = 'application/docs/'.$categorie.'/'.$onderwerp.'.html';
		error_log($bodyLocation);
		if(file_exists($bodyLocation))
		{
			return file_get_contents($bodyLocation);
		}
		else
		{
			$notFound = 'Sorry, deze pagina is niet gevonden.';
			return $notFound;
		}
	}
	
}