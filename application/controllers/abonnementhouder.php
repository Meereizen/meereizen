<?php

/**
* Abonnementhouder Controller
*
* Bevat alle logica voor alles wat betreft de abonnementhouders.
*
* @author	Olivier Jansen
* @author	Ruben Steendam
* @package	Meereizen.nl
*/

class Abonnementhouder extends Controller {
	
	private $_config;

	// Construct functie met config voor OAuth
	function __construct()
	{
		$this->_config = array(
			'path' => '/abonnementhouder/',
			'callback_url' => '{path}oauth2callback/',
			'security_salt' => 'LDFmiilY8Fyw510rx4W1KsVrQCnpBzzpTBWA5vJidQKDx8pJbmw8R1C4m',
			'Strategy' => array(
				'Facebook' => array(
					'app_id' => '420089441435792',
					'app_secret' => '01f164255b477275ac11f7fd9de7739a',
					'scope' => 'email'
				),
				'Google' => array(
					'client_id' => '816688811923.apps.googleusercontent.com',
					'client_secret' => 'Nzn_OH0I4gDbkSN2C2CZEHlc'
				),
				'Twitter' => array(
					'key' => 'dlpx6pGqhzFUkhIgdhcrVQ',
					'secret' => 'fzFdi0KXvkbUVaoj4dOgfiIt43Iys3FcUAfozkBgFI'
				)
			),
		);
		$this->loadPlugin('Opauth/Opauth');
	}
	
	private function generateRememberHash($id)
	{
		$random = '';
		for ($i = 0; $i < 4; $i ++) {
			$random .= sha1(microtime(true).mt_rand(10000,90000));
		}
		return $id . ':' . substr($random, 0, 128);
	}

	public function rememberme()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
			{
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
			else
			{
				header('Location: ' . BASE_URL);
			}
		}
		else
		{
			error_log('Je bent niet succesvol ingelogd. Terug naar: '.$_SERVER['HTTP_REFERER']);
			setcookie('remember', 'niksnadariennoppes', time() - 3600, '/');
			if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
			{
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
			else
			{
				header('Location: ' . BASE_URL);
			}
		}
	}

	// Login pagina of gebruikerspagina als er al ingelogd is
	public function index($mobile=FALSE)
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$id = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($id))
			{
				
				// Naam van de gebruiker ophalen
				$naam = $abonnementhouderModel->getNaam($id);

				// traject model laden
				$trajectModel = $this->loadModel('Traject_model');			

				$logHelper = $this->loadHelper('log_helper');
				
				// Lege array voor mogelijke data				
				$data = Array();
				
				// Meereis model inladen
				$meereisModel = $this->loadModel('Meereis_model');

				// Alle Meereizen van de huidige gebruiker laden en als data naar de view versturen
				$data['meereizen'] = $meereisModel->getMeereizenUserOpen($id);

				// View laden, variabalen instellen en renderen				
				$template = $this->loadView('abonnementhouder');
				$template->set('naam', $naam);
				$template->set('user_id', $id);

				$template->set('data', $data);

				$template->set('autocomplete', 'true');
				
				$template->render();
			}
			else
			{
				$this->redirect('abonnementhouder/activeer');
			}
		}
		else
		{
			$template = $this->loadView('login');
			$template->set('mobile', $mobile);
			$template->render();
		}
	}

	// Geschiedenis pagina v/h dashboard
	function geschiedenis()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$id = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($id))
			{
				// traject model laden
				$trajectModel = $this->loadModel('Traject_model');			

				$logHelper = $this->loadHelper('log_helper');
				
				// Lege array voor mogelijke data				
				$data = Array();
				
				// Meereis model inladen
				$meereisModel = $this->loadModel('Meereis_model');

				// Alle Meereizen van de huidige gebruiker laden en als data naar de view versturen
				$data['meereizen'] = $meereisModel->getMeereizenUserPast($id);

				// View laden, variabalen instellen en renderen				
				$template = $this->loadView('abonnementhouder');
				$template->set('naam', $naam);
				$template->set('page', 'geschiedenis');
				$template->set('user_id', $id);

				$template->set('data', $data);

				$template->set('autocomplete', 'true');
				
				$template->render();
			}
			else
			{
				$this->redirect('abonnementhouder/activeer');
			}
		}
		else
		{
			$template = $this->loadView('login');
			$template->render();
		}
	}

	// Instellingen pagina v/h dashboard
	function instellingen()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$id = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($id))
			{
				// traject model laden
				$trajectModel = $this->loadModel('Traject_model');			

				$logHelper = $this->loadHelper('log_helper');
				
				// Lege array voor mogelijke data				
				$data = Array();
				
				if(isset($_POST) && !empty($_POST))
				{
					if(isset($_POST['update']))
					{
						if($abonnementhouderModel->updateContactinfo($id, $_POST))
						{
							$data['success'] = 'Uw gegevens zijn succesvol gewijzigd.';
						}
						else
						{
							$data['error'] = 'Er ging iets mis, uw gegevens konden niet gewijzigd worden.';
						}
					}
				}
				$data['gebruiker'] = $abonnementhouderModel->getAllData($id);

				// View laden, variabalen instellen en renderen				
				$template = $this->loadView('abonnementhouder');
				$template->set('page', 'instellingen');
				$template->set('user_id', $id);

				$template->set('data', $data);

				$template->set('autocomplete', 'true');
				
				$template->render();
			}
			else
			{
				$this->redirect('abonnementhouder/activeer');
			}
		}
		else
		{
			$template = $this->loadView('login');
			$template->render();
		}
	}

	// Trajecten pagina v/h dashboard
	function trajecten()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$id = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($id))
			{
				// traject model laden
				$trajectModel = $this->loadModel('Traject_model');			

				$logHelper = $this->loadHelper('log_helper');
				
				// Lege array voor mogelijke data				
				$data = Array();
				
				if(isset($_POST) && !empty($_POST))
				{
					if(!isset($_POST['delete']))
					{
						if($trajectModel->insertTraject($id, $_POST['van'], $_POST['naar'], $_POST['tijd'], $_POST['dag']))
						{
							$data['success'] = 'Het traject is toegevoegd aan onze database.';

							$logHelper->log('trajectInsert', 'Nieuw traject: '.$_POST['van'].' naar '.$_POST['naar']);
						}
						else
						{
							$data['error'] = 'Er ging iets mis, het traject is niet toegevoegd aan onze database.';
						}
					}
					elseif(isset($_POST['delete']))
					{
						if($trajectModel->deleteTraject($id, $_POST['van'], $_POST['naar'], $_POST['tijd']))
						{
							$data['success'] = 'Het traject is verwijderd van uw lijst';
						}
						else
						{
							$data['error'] = 'Er ging iets mis, het traject is niet uit uw lijst gehaald.';
						}
					}
					else
					{
						$this->redirect('abonnementhouder');
					}
				}
				$data['trajecten'] = $trajectModel->userTrajects($id);

				// View laden, variabalen instellen en renderen				
				$template = $this->loadView('abonnementhouder');
				$template->set('page', 'trajecten');
				$template->set('user_id', $id);

				$template->set('data', $data);

				$template->set('autocomplete', 'true');
				
				$template->render();
			}
			else
			{
				$this->redirect('abonnementhouder/activeer');
			}
		}
		else
		{
			$template = $this->loadView('login');
			$template->render();
		}
	}

	// 'Voor abonnementhouders' pagina
	function info()
	{
		$template = $this->loadView('abonnementhouder-info');
		$template->render();
	}
	
	// Een Meereis verificeren met de code
	function verificatie()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$userId = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($userId))
			{
				if(isset($_POST['code']) && isset($_POST['meereis']))
				{
					$meereis = $_POST['meereis'];
					$code = $_POST['code'];
					if(strlen($code) == 8 && is_numeric($code))
					{
						// Meereis model laden
						$meereisModel = $this->loadModel('Meereis_model');
						// Check if Meereis is from user
						if($meereisModel->isMyGekoppeldeMeereis($meereis, $userId))
						{
							// Echte code ophalen en checken of hij overeenkomt met de ingevoerde code
							$realCode = $meereisModel->getVerificatieCode($meereis);
							if($code == $realCode)
							{
								// Paykey uit database ophalen
								$paykey = $meereisModel->getPaykey($meereis);

								// Payment helper helpt met het maken van betalingen
								$paymentHelper = $this->loadHelper('Payment_helper');

								// De delayed chained payment executen
								if($paymentHelper->executePayment($paykey))
								{
									// Update database, set Meereis = afgerond
									if($meereisModel->verifieer($meereis))
									{
										$logHelper = $this->loadHelper('log_helper');
										$logHelper->log('verificatie', 'Meereis met ID '.$meereis.' is geverifieerd.');

										$this->redirect('abonnementhouder');
									}
									else
									{
										$template = $this->loadView('customerror');
										$template->set('errorMessage', 'Het geld is naar u overgemaakt maar de Meereis kon niet geverifieerd worden met deze code. Volgens onze gegevens is de code echter wel correct. Neem alstublieft contact met ons op.');
										$template->render();
									}
								}
								else
								{
									$template = $this->loadView('customerror');
									$template->set('errorMessage', 'Paypal weigerde de aangevraagd betaling uit te voeren. Het lijkt erop dat de betaling al uitgevoerd is.');
									$template->render();
								}
							}
							else
							{
								$template = $this->loadView('customerror');
								$template->set('errorMessage', 'De door u ingevoerde code komt niet overeen met de verificatie code zoals die in onze database staat.');
								$template->render();
							}
						}
						else
						{
							$template = $this->loadView('customerror');
							$template->set('errorMessage', 'Volgens onze gegevens is deze Meereis niet van jou. Alleen de gekoppelde abonnementhouder kan het geld opeisen.');
							$template->render();
						}
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'De verificatie code had niet het juiste formaat. Geldige verificatiecodes zijn 8 tekens lang en bestaan enkel uit cijfers.');
						$template->render();
					}
				}
				else
				{
					$this->redirect('abonnementhouder');
				}
			}
			else
			{
				$this->redirect('abonnementhouder/activeer');
			}
		}
		else
		{
			$this->redirect('abonnementhouder');
		}
	}

	// Een Meereis accepteren of weigeren
	function meereis()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			// ID van de gebruiker ophalen
			$id = $loginHelper->getCurrentId();
			
			if($abonnementhouderModel->isActivated($id))
			{
				$data = $_POST;
				$meereisModel = $this->loadModel('Meereis_model');
				
				// Kijken of er geaccepteerd of geweigerd is
				if($data['state']==1)
				{
					/*
					*
					*
					* 		Geaccepteerd
					*
					*
					*/
					
					if($meereisModel->isMyMeereis($data['id'], $id))
					{
						$code = md5($data['id'].$id);

						// Update db, set user_id to Meereizen table
						if($meereisModel->accept($data['id'], $id, $code))
						{
							// Alle overige informatie over de reis ophalen
							$meereisData = $meereisModel->getData($data['id']);

							// Generate unique code and URL
							$urlExt = 'reis/betalen/'.$data['id'].'/'.$code;
							$url = BASE_URL.$urlExt;
							
							// Send mail to Reiziger with URL to payment
							$mailHelper = $this->loadHelper('mail_helper');
							$this->loadPlugin('PHPMailer/class.phpmailer');
							$mail = new PHPMailer();
							if($mailHelper->verstuurMeereisAcceptedMail($url, $mail, $meereisData['email']))
							{
								error_log('Email verstuurd naar '.$meereisData['email']);
								// RegID ophalen
								$regId = $meereisData['regid'];

								// Als er een RegID bestaat
								if($regId)
								{
									// Android Pusher laden
									$this->loadPlugin('AndroidPusher/androidpusher.class');
	
									// Pusher instellen met onze Google API key
									$pusher = new Pusher(GOOGLE_API_KEY);
	
									// Push een bericht naar de aanvrager.
									$pusher->notify($regId, Array("message" => 'Je mag meereizen naar '.$meereisData['naar'], "page" => $urlExt));
									
								}

								$logHelper = $this->loadHelper('log_helper');
								$logHelper->log('reisAccept', 'Reis #'.$data['id'].' is geaccepteerd.');

								// Als alles gedaan is, wordt de gebruiker weer terug gestuurd naar de abonnementhouder page
								$this->redirect('abonnementhouder');
							}
							else
							{
								$template = $this->loadView('customerror');
								$template->set('errorMessage', 'Er ging iets fout tijdens het versturen van de bevestigingse-mail. Dit is van essentieel belang voor het Meereizen. Neem alstublieft contact met ons op.');
								$template->render();
							}

						}
						else
						{
							$template = $this->loadView('customerror');
							$template->set('errorMessage', 'Wij konden uw account niet goed koppelen met deze aanvraag. Als dit probleem zich blijft herhalen, neem dan alstublieft contact met ons op.');
							$template->render();
						}
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'De aanvraag die u probeert te accepteren kunnen wij niet (meer) vinden in onze database. Probeer het opnieuw. Als deze fout zich herhaalt, neem dan contact met ons op.');
						$template->render();
					}
				}
				else
				{
					/*
					*
					*
					* 		Geweigerd
					*
					*
					*/
					
					$count = $meereisModel->countLinks($data['id']);
					if($count == 1)
					{
						if($meereisModel->deleteMeereisLink($data['id'], $id))
						{
							// Alle overige informatie over de reis ophalen
							$meereisData = $meereisModel->getData($data['id']);

							// Send mail to Reiziger
							$mailHelper = $this->loadHelper('mail_helper');
							$this->loadPlugin('PHPMailer/class.phpmailer');
							$mail = new PHPMailer();
							if($mailHelper->verstuurMeereisRejectedMail($mail, $meereisData['email']))
							{
								$this->redirect('abonnementhouder');
							}
							else
							{
								$template = $this->loadView('customerror');
								$template->set('errorMessage', 'Er ging iets mis met het versturen van een mail naar de aanvrager van dit traject. Neem alstublieft contact met ons op.');
								$template->render();
							}
						}
						else
						{
							$template = $this->loadView('customerror');
							$template->set('errorMessage', 'Er ging iets mis met het verwijderen van de koppeling tussen u en deze aanvraag. Probeer het opnieuw. Als dit probleem zich voor blijft doen, neem dan contact met ons op.');
							$template->render();
						}
					}
					else
					{
						if($meereisModel->deleteMeereisLink($data['id'], $id))
						{
							$this->redirect('abonnementhouder');
						}
						else
						{
							$template = $this->loadView('customerror');
							$template->set('errorMessage', 'Er ging iets mis met het verwijderen van de koppeling tussen u en deze aanvraag. Probeer het opnieuw. Als dit probleem zich voor blijft doen, neem dan contact met ons op.');
							$template->render();
						}
					}
				}
			}
		}
		else
		{
			$this->redirect('abonnementhouder');
		}
	}
	
	// Alle OAuth requests linken terug naar deze pagina
	function oauth2callback()
	{
		// Opauth instantie aanmaken.
		$Opauth = new Opauth( $this->_config, false );
		
		$response = null;
		// Door de opties heengaan opzoek naar de transport manier om de user data te ontvangen.
		switch($Opauth->env['callback_transport'])
		{	
			case 'session':
				//session_start();
				$response = $_SESSION['opauth'];
				unset($_SESSION['opauth']);
				break;
			case 'post':
				$response = unserialize(base64_decode( $_POST['opauth'] ));
				break;
			case 'get':
				$response = unserialize(base64_decode( $_GET['opauth'] ));
				break;
			default:
				$template = $this->loadView('customerror');
				$template->set('errorMessage', 'Deze transport methode wordt niet ondersteund.');
				$template->render();
				break;
		}
		
		// Als er een error bericht in het antwoord zit.
		if (array_key_exists('error', $response))
		{
			$template = $this->loadView('customerror');
			$template->set('errorMessage', 'Toegang geweigerd.');
			$template->set('response', $response);
			$template->render();
		}
		else
		{
			// Als er een waarde ontbreekt uit het antwoord.
			if (empty($response['auth']) || empty($response['timestamp']) || empty($response['signature']) || empty($response['auth']['provider']) || empty($response['auth']['uid']))
			{
				$this->redirect('abonnementhouder');
			}
			// Als het pakket ongeldig is.
			elseif (!$Opauth->validate(sha1(print_r($response['auth'], true)), $response['timestamp'], $response['signature'], $reason))
			{
				$template = $this->loadView('customerror');
				$template->set('errorMessage', 'Ongeldig Oauth antwoord.');
				$template->render();
			}
			// Alles aan het Oauth antwoord klopt.
			else
			{
				if($response['auth']['provider']=='Facebook' || $response['auth']['provider']=='Google')
				{
					$userData = Array(
						'social_id' => $response['auth']['uid'],
						'social_provider' => $response['auth']['provider'],
						'naam' => $response['auth']['info']['name'],
						'geslacht' => $response['auth']['raw']['gender'],
						'email' => $response['auth']['raw']['email']
					);
					if(isset($response['auth']['info']['image']))
					{
						$userData['foto'] = $response['auth']['info']['image'];
					}
				}
				elseif($response['auth']['provider']=='Twitter')
				{
					$userData = Array(
						'social_id' => $response['auth']['uid'],
						'social_provider' => $response['auth']['provider'],
						'naam' => $response['auth']['info']['name'],
						'geslacht' => 'unknown',
						'email' => '',
						'foto' => $response['auth']['info']['image']
					);
				}
				else
				{
					echo 'De UserProvider is onbekend.';
					echo "<pre>";
					print_r($response);
					echo "</pre>";
					die();
				}

				$abonnementhouderModel = $this->loadModel('abonnementhouder_model');
				// Check user functie gelijk gebruiken om ID op te vragen.
				$id = $abonnementhouderModel->getId($userData['social_id'], $userData['social_provider']);

				if($id)
				{
					// Succesvol ingelogd!
					$name = $abonnementhouderModel->getNaam($id);

					$loginHelper = $this->loadHelper('login_helper');
					$loginHelper->logInSessions($id, $userData['social_id'], $name);
					
					if(isset($_SESSION['regid']) && $_SESSION['regid'] !== 'null')
					{
						// Check voor regId (=Android app gebruiker) en voeg deze toe in de database
						$abonnementhouderModel->insertRegId($id, $_SESSION['regid']);
					}
					
					if($abonnementhouderModel->isActivated($id)){

						$rememberHash = $this->generateRememberHash($id);
						setcookie('remember', $rememberHash, time() + 60 * 60 * 24 * 365, '/');


						if(!$abonnementhouderModel->updateRememberHash($id, $rememberHash)){
							$logHelper = $this->loadHelper('log_helper');
							$logHelper->log('database-error', 'Er ging iets fout met het updaten van de remember-me-hash in de database.');
						}

						$this->redirect('main/index/null/loggedIn#2');
					}else{
						$this->redirect('abonnementhouder/activeer');
					}
				}else{
					if($abonnementhouderModel->insertUser($userData)==TRUE){
						// Succesvol geregistreerd.
						$id = $abonnementhouderModel->getId($userData['social_id'], $userData['social_provider']);
						
						if(isset($_SESSION['regid']) && $_SESSION['regid'] !== 'null')
						{
							// Check voor regId (=Android app gebruiker) en voeg deze toe in de database
							$abonnementhouderModel->insertRegId($id, $_SESSION['regid']);
						}
						
						$loginHelper = $this->loadHelper('login_helper');
						$loginHelper->logInSessions($id, $userData['social_id']);

						$logHelper = $this->loadHelper('log_helper');
						$logHelper->log('registratie', 'Een gebruiker (met id '.$id.') heeft geregistreerd met behulp van '.$userData['social_provider']);
						
						$this->redirect('abonnementhouder/activeer');
					}else{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'Registratie mislukt.');
						$template->render();
					}
				}
			}
		}		
	}
	
	// Account activeren = gegevens invullen + mail sturen
	function activeer($verstuurd=0)
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			$id = $loginHelper->getCurrentId();
			if(isset($_POST['email']) && !empty($_POST['email']) 
			&& isset($_POST['telefoon']) && !empty($_POST['telefoon']) 
			&& isset($_POST['paypal']) && !empty($_POST['paypal']))
			{
				$data = Array(
					'email' => $_POST['email'],
					'telefoon' => $_POST['telefoon'],
					'paypal' => $_POST['paypal']
				);
				$code = substr(sha1(time().$id),0,45);
				if($abonnementhouderModel->activateUser($id, $data, $code))
				{
					$url = BASE_URL.'abonnementhouder/bevestig/'.$id.'/'.$code;
				
				    $mailHelper = $this->loadHelper('mail_helper');
				    $this->loadPlugin('PHPMailer/class.phpmailer');
				    $mail = new PHPMailer();
					if($mailHelper->verstuurBevestigingsMail($url, $mail, $data['email']))
					{
						$this->redirect('abonnementhouder/activeer/verstuurd');
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'Er ging iets fout tijdens het versturen van de bevestigings e-mail. Je account is wel geactiveerd maar nog niet te gebruiken omdat het e-mailadres niet bevestigd is. Neem contact met ons op om dit probleem op te lossen.');
						$template->render();
					}
				}
				else
				{
					$template = $this->loadView('customerror');
					$template->set('errorMessage', 'Het activeren van uw account is niet gelukt. Probeer het nog eens en neem contact met ons op als deze fout zich voor blijft doen.');
					$template->render();
				}
			}
			else
			{
				if($abonnementhouderModel->isActivated($id))
				{
					$this->redirect('abonnementhouder');
				}
				elseif($abonnementhouderModel->isUnverified($id))
				{
					$email = $abonnementhouderModel->getEmail($id);
					$template = $this->loadView('onbevestigdedata');
					if($verstuurd!==0)
					{
						$template->set('verstuurd', TRUE);
					}
					$template->set('email', $email);
					$template->render();
				}
				else
				{
					$data = $abonnementhouderModel->getAllData($id);
					$template = $this->loadView('activeer');
					$template->set('id', $id);
					$template->set('data', $data);
					$template->render();
				}
			}
		}
		else
		{
			$this->redirect('abonnementhouder');
		}
	}
	
	// De bevestigingsmail nog een keer sturen
	function herstuur()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			$id = $loginHelper->getCurrentId();
			if($abonnementhouderModel->isUnverified($id))
			{
				$code = substr(sha1(time().$id),0,45);
				if($abonnementhouderModel->updateEmailCode($id, $code))
				{
					$url = BASE_URL.'abonnementhouder/bevestig/'.$id.'/'.$code;
				
				    $mailHelper = $this->loadHelper('mail_helper');
				    $this->loadPlugin('PHPMailer/class.phpmailer');
				    $mail = new PHPMailer();
				    $adres = $abonnementhouderModel->getEmail($id);
					if($mailHelper->verstuurBevestigingsMail($url, $mail, $adres))
					{
						$this->redirect('abonnementhouder/activeer/verstuurd');
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'Er ging iets fout tijdens het versturen van de bevestigings e-mail. Je account is wel geactiveerd maar nog niet te gebruiken omdat het e-mailadres niet bevestigd is. Neem contact met ons op om dit probleem op te lossen.');
						$template->render();
					}
				}
				else
				{
					$template = $this->loadView('customerror');
					$template->set('errorMessage', 'Het herberekenen van de bevestigingscode is niet gelukt. Als deze fout zich voor blijft doen, neemt u dan contact met ons op.');
					$template->render();
				}
			}
			else
			{
				$this->redirect('abonnementhouder');
			}
		}
		else
		{
			$this->redirect('abonnementhouder');
		}
	}
	
	// Wijzig e-mailadres en herstuur bevestigingsemail
	function wijzigherstuur()
	{
		// abonnementhouder model laden
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		// Login helper; class met handige login functies
		$loginHelper = $this->loadHelper('login_helper');
		// Checken of er ingelogd is
		if($loginHelper->loggedIn($abonnementhouderModel))
		{
			$id = $loginHelper->getCurrentId();
			if($abonnementhouderModel->isUnverified($id))
			{
				if(isset($_POST['email']))
				{
					if($abonnementhouderModel->updateEmail($id, $_POST['email']))
					{
						$this->redirect('abonnementhouder/herstuur');
					}
					else
					{
						$template = $this->loadView('customerror');
						$template->set('errorMessage', 'Er ging iets fout tijdens het veranderen van uw e-mailadres. Probeer het nog eens en neem contact met ons op als deze fout zich voor blijft doen.');
						$template->render();
					}
				}
				else
				{
					$template = $this->loadView('customerror');
					$template->set('errorMessage', 'U dient een e-mailadres op te geven, anders kunnen wij u geen bevestigings e-mail sturen.');
					$template->render();
				}
			}
			else
			{
				$this->redirect('abonnementhouder');
			}
		}
		else
		{
			$this->redirect('abonnementhouder');
		}
	}
	
	// Hier wordt naartoe gelinkt in het mailtje
	function bevestig($id, $code)
	{
		if(isset($code) && isset($id) && !empty($code) && !empty($code) && is_numeric($id))
		{
			$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
			if($abonnementhouderModel->isUser($id))
			{
				$loginHelper = $this->loadHelper('login_helper');
				$loginHelper->logOut();
				if($abonnementhouderModel->bevestigEmail($id, $code))
				{
					$this->redirect('abonnementhouder');
				}
				else
				{
					$template = $this->loadView('customerror');
					$template->set('errorMessage', 'De bevestigingscode die u gebruikt heeft is niet geldig.');
					$template->render();
				}
			}else{
				$this->redirect('');
			}
		}
		else
		{
			$this->redirect('');
		}
	}
	
	// Een gebruiker uitloggen
	function loguit()
	{
		$abonnementhouderModel = $this->loadModel('Abonnementhouder_model');
		$loginHelper = $this->loadHelper('login_helper');
		$loginHelper->logOut($abonnementhouderModel);
		$this->redirect('');
	}
        
    // Onderstaande functies zijn voor specifieke OAuth requests
        
    function facebook()
    {
    	$config = $this->_config;
    	$Opauth = new Opauth( $config );
    }
    
    function twitter()
    {
    	$config = $this->_config;
    	$Opauth = new Opauth( $config );
    }
    
    function google($var=0)
    {
    	$config = $this->_config;
    	$Opauth = new Opauth( $config );
    	
    	if(isset($var) && $var == 'oauth2callback')
    	{
    		$this->redirect('abonnementhouder/oauth2callback');
    	}
    }
    
}

?>
