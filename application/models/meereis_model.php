<?php

/**
* Traject Model
*
* Bevat alle database communicatie voor de trajecten van abonnementhouders.
*
* @author	Olivier Jansen
* @author	Ruben Steendam
* @package	Meereizen.nl
*/

class Meereis_model extends Model {

	// Alle niet-afgeronde Meereizen van een gebruiker weergeven
	public function getMeereizenUserOpen($user_id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				trajecten.van AS van, 
				trajecten.naar AS naar, 
				meereizen.vertrektijd AS vertrektijd, 
				meereizen.id AS id, 
				meereizen.gebruiker AS gebruiker, 
				meereizen.vol_tarief AS vol_tarief, 
				meereizen.nieuw_tarief AS nieuw_tarief, 
				meereizen.email AS email, 
				meereizen.betaald AS betaald
			FROM meereizengebruikerlinks 
			INNER JOIN meereizen 
			ON meereizengebruikerlinks.meereis = meereizen.id 
			INNER JOIN trajecten 
			ON meereizen.traject = trajecten.id
			WHERE meereizengebruikerlinks.gebruiker = :user_id 
			AND afgerond = 0
			ORDER BY id DESC;");
		

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);

		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results;
	}

	public function countMeereizenUserOpen($user_id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(meereis) AS count
			FROM meereizengebruikerlinks 
			INNER JOIN meereizen 
			ON meereizengebruikerlinks.meereis = meereizen.id 
			WHERE meereizengebruikerlinks.gebruiker = :user_id
			AND afgerond = 0;");
			
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);

		$stmt->execute();
		$result = $stmt->fetch();

		return $result['count'];
	}
	
	// Alle Meereizen van een gebruiker weergeven
	public function getMeereizenUserPast($user_id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				trajecten.van AS van, 
				trajecten.naar AS naar, 
				meereizen.vertrektijd AS vertrektijd, 
				meereizen.id AS id, 
				meereizen.gebruiker AS gebruiker, 
				meereizen.vol_tarief AS vol_tarief, 
				meereizen.nieuw_tarief AS nieuw_tarief, 
				meereizen.email AS email, 
				meereizen.betaald AS betaald 
			FROM meereizengebruikerlinks 
			INNER JOIN meereizen 
			ON meereizengebruikerlinks.meereis = meereizen.id 
			INNER JOIN trajecten 
			ON meereizen.traject = trajecten.id
			WHERE meereizengebruikerlinks.gebruiker = :user_id 
			AND afgerond = 1 
			ORDER BY id DESC;");
			
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);

		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results;
	}

	// Alle gegevens van een Meereis ophalen op basis van het ID
	public function getMeereizenId($id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				trajecten.van AS van, 
				trajecten.naar AS naar, 
				meereizen.vertrektijd AS vertrektijd, 
				meereizen.id AS id, 
				meereizen.gebruiker AS gebruiker, 
				meereizen.betaald AS betaald, 
				meereizen.vol_tarief AS vol_tarief, 
				meereizen.nieuw_tarief AS nieuw_tarief, 
				meereizen.email AS email, 
				meereizen.telefoon AS telefoon 
			FROM meereizengebruikerlinks 
			INNER JOIN meereizen 
			ON meereizengebruikerlinks.meereis = meereizen.id 
			INNER JOIN trajecten 
			ON meereizen.traject = trajecten.id
			WHERE meereizen.id = :id
			LIMIT 1;");
			
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);

		$stmt->execute();
		$results = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $results;
	}

	// Haal paykey op, op basis van id
	public function getPaykey($id)
	{
		try
		{
			$stmt = $this->connection->prepare("
				SELECT paykey
				FROM meereizen
				WHERE id = :id");

			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			return $result['paykey'];
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

	// Haal gegevens van een Meereis op, op basis van paykey
	public function getMeereisPaykey($paykey)
	{
		try
		{
			$stmt = $this->connection->prepare("
				SELECT
					id,
					vertrektijd,
					gebruiker,
					email,
					telefoon
				FROM meereizen
				WHERE paykey = :paykey");

			$stmt->bindParam(':paykey', $paykey, PDO::PARAM_STR);
			$stmt->execute();

			return $stmt->fetch(PDO::FETCH_ASSOC);
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}
	
	// Deze moet er uit gesloopt worden want doet volgens mij hetzelfde als bovenstaande
	public function getData($id)
	{
		$stmt = $this->connection->prepare("
			SELECT 
				meereizen.*,
				trajecten.van AS van, 
				trajecten.naar AS naar 
			FROM meereizen 
			INNER JOIN trajecten 
			ON trajecten.id = meereizen.traject
			WHERE meereizen.id = :id LIMIT 1;");
			
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);

		$stmt->execute();
		$results = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $results;
	}

	public function getVerificatieCode($id)
	{
		$stmt = $this->connection->prepare("SELECT verificatie_code FROM meereizen WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['verificatie_code'])){
			return $result['verificatie_code'];
		}else{
			return FALSE;
		}
	}
	
	// Checkt of een Meereis wel van een bepaalde gebruiker is
	public function isMyMeereis($id, $user_id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM meereizen 
			WHERE id = :id 
			AND id IN (SELECT meereis FROM meereizengebruikerlinks WHERE gebruiker = :user_id)
			AND gebruiker = 0;");
			
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);

		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		if($result['count'] > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	// Checkt of een Meereis wel van een bepaalde gebruiker is
	public function isMyGekoppeldeMeereis($id, $user_id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM meereizen 
			WHERE id = :id 
			AND gebruiker = :user_id;");
			
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);

		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		if($result['count'] > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function isPayMeereis($id, $code)
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM meereizen 
			WHERE id = :id 
			AND betaalcode = :code
			AND betaald = 0;");
			
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->bindParam(':code', $code, PDO::PARAM_STR);

		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		if($result['count'] > 0)
		{
			$stmt = $this->connection->prepare("
				SELECT
					betaalcode,
					gebruiker 
				FROM meereizen 
				WHERE id = :id 
				AND betaalcode = :code;");
				
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			$code = md5($id.$result['gebruiker']);
			
			if($code == $result['betaalcode'])
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function verifieer($meereis)
	{
		try
		{
			$stmt = $this->connection->prepare("
				UPDATE meereizen 
				SET afgerond = 1 
				WHERE id = :id");

			$stmt->bindParam(':id', $meereis, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

	public function setPaykey($id, $paykey)
	{
		try
		{
			$stmt = $this->connection->prepare("
				UPDATE meereizen 
				SET paykey = :paykey 
				WHERE id = :id");

			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			$stmt->bindParam(':paykey', $paykey, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

	public function setBetaald($paykey)
	{
		try
		{
			$stmt = $this->connection->prepare("
				UPDATE meereizen 
				SET betaald = 1 
				WHERE paykey = :paykey");

			$stmt->bindParam(':paykey', $paykey, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

	public function accept($id, $gebruiker, $code)
	{

		try
		{
			$stmt = $this->connection->prepare("
				UPDATE meereizen 
				SET gebruiker = :gebruiker, 
				betaalcode = :code 
				WHERE id = :id");

			$stmt->bindParam(':gebruiker', $gebruiker, PDO::PARAM_STR);
			$stmt->bindParam(':code', $code, PDO::PARAM_STR);
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}

	}
	
	public function insertMeereisLink($meereis, $gebruiker)
	{
		try
		{
			$stmt = $this->connection->prepare("
				INSERT INTO meereizengebruikerlinks 
					(meereis, gebruiker) 
				VALUES 
					(:meereis, :gebruiker)");

			$stmt->bindParam(':meereis', $meereis, PDO::PARAM_STR);
			$stmt->bindParam(':gebruiker', $gebruiker, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

	public function insertVerificatieCode($id, $code)
	{
		try
		{
			$stmt = $this->connection->prepare("
				UPDATE meereizen
				SET verificatie_code=:code
				WHERE id = :id");

			$stmt->bindParam(':code', $code, PDO::PARAM_STR);
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}
	
	public function countLinks($meereis)
	{
		try
		{
			$stmt = $this->connection->prepare("SELECT COUNT(*) AS count 
				FROM meereizengebruikerlinks
				WHERE meereis = :meereis;");

			$stmt->bindParam(':meereis', $meereis, PDO::PARAM_STR);
			$stmt->execute();
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $result['count'];
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return '';
		}
	}
	
	public function deleteMeereisLink($meereis, $gebruiker)
	{
		try
		{
			$stmt = $this->connection->prepare("
				DELETE FROM meereizengebruikerlinks
				WHERE meereis = :meereis 
				AND gebruiker = :gebruiker");

			$stmt->bindParam(':meereis', $meereis, PDO::PARAM_STR);
			$stmt->bindParam(':gebruiker', $gebruiker, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

	public function insertMeereis($traject, $vertrektijd, $email, $telefoon, $vol_tarief, $nieuw_tarief, $regid)
	{
		try
		{
			$stmt = $this->connection->prepare("
				INSERT INTO meereizen 
					(traject,
					 vertrektijd,
					 email,
					 telefoon,
					 vol_tarief,
					 nieuw_tarief,
					 regid) 
				VALUES 
					(:traject,
					 :vertrektijd,
					 :email,
					 :telefoon,
					 :vol_tarief,
					 :nieuw_tarief,
					 :regid)");

			$stmt->bindParam(':traject', $traject, PDO::PARAM_STR);
			$stmt->bindParam(':vertrektijd', $vertrektijd, PDO::PARAM_STR);
			$stmt->bindParam(':email', $email, PDO::PARAM_STR);
			$stmt->bindParam(':telefoon', $telefoon, PDO::PARAM_STR);
			$stmt->bindParam(':vol_tarief', $vol_tarief, PDO::PARAM_STR);
			$stmt->bindParam(':nieuw_tarief', $nieuw_tarief, PDO::PARAM_STR);
			$stmt->bindParam(':regid', $regid, PDO::PARAM_STR);
			
			if($stmt->execute())
			{
				return $this->connection->lastInsertId();
			}
			else
			{
				return FALSE;
			}
		}
		catch(PDOException $Exception)
		{
			error_log('Error: '.$Exception->getMessage());
			return FALSE;
		}
	}

}

?>
