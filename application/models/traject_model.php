<?php

/**
* Traject Model
*
* Bevat alle database communicatie voor de trajecten van abonnementhouders.
*
* @author	Olivier Jansen
* @author	Ruben Steendam
* @package	Meereizen.nl
*/

class Traject_model extends Model {

	public function countDirectMatches($from, $to, $time1, $time2, $dayofweek)
	{
		$from = urldecode($from);
		$to = urldecode($to);

		$dayofweektodb = Array(
			1 => 'maandag',
			2 => 'dinsdag',
			3 => 'woensdag',
			4 => 'donderdag',
			5 => 'vrijdag',
			6 => 'zaterdag',
			7 => 'zondag'
		);
		
		$stmt = $this->connection->prepare("
			SELECT count(gebruiker)AS count 
			FROM `trajectlinks` 
			WHERE traject 
				IN(	SELECT id 
					FROM `trajecten` 
					WHERE van = :from 
					AND naar = :to) 
			AND vertrektijd > :time1 
			AND vertrektijd < :time2
			AND ".$dayofweektodb[$dayofweek]." = '1'");
		$stmt->bindParam(':from', $from, PDO::PARAM_STR);
		$stmt->bindParam(':to', $to, PDO::PARAM_STR);
		$stmt->bindParam(':time1', $time1, PDO::PARAM_STR);
		$stmt->bindParam(':time2', $time2, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['count'];
	}
	
	public function getDirectMatches($from, $to, $time1, $time2, $dayofweek)
	{
		$from = urldecode($from);
		$to = urldecode($to);
		
		$dayofweektodb = Array(
			1 => 'maandag',
			2 => 'dinsdag',
			3 => 'woensdag',
			4 => 'donderdag',
			5 => 'vrijdag',
			6 => 'zaterdag',
			7 => 'zondag'
		);
		
		$stmt = $this->connection->prepare("
			SELECT gebruiker, traject 
			FROM `trajectlinks` 
			WHERE traject 
				IN(	SELECT id 
					FROM `trajecten` 
					WHERE van = :from 
					AND naar = :to) 
			AND vertrektijd > :time1 
			AND vertrektijd < :time2
			AND ".$dayofweektodb[$dayofweek]." = '1'");
		$stmt->bindParam(':from', $from, PDO::PARAM_STR);
		$stmt->bindParam(':to', $to, PDO::PARAM_STR);
		$stmt->bindParam(':time1', $time1, PDO::PARAM_STR);
		$stmt->bindParam(':time2', $time2, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	public function getDayMatches($dayofweek, $limit, $prijzen=false)
	{
		$dayofweektodb = Array(
			1 => 'maandag',
			2 => 'dinsdag',
			3 => 'woensdag',
			4 => 'donderdag',
			5 => 'vrijdag',
			6 => 'zaterdag',
			7 => 'zondag'
		);
		$dayofweekenglish = Array(
			1 => 'monday',
			2 => 'tuesday',
			3 => 'wednesday',
			4 => 'thursday',
			5 => 'friday',
			6 => 'saturday',
			7 => 'sunday'
		);
		
		$stmt = $this->connection->prepare("
			SELECT gebruiker, traject, van, naar, vertrektijd
			FROM `trajectlinks` 
			INNER JOIN trajecten 
			ON trajectlinks.traject = trajecten.id 
			WHERE ".$dayofweektodb[$dayofweek]." = '1' 
			ORDER BY RAND() 
			LIMIT ".$limit);
		$stmt->execute();

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if($prijzen==true)
		{
			foreach($results as $i => $result)
			{
				$timestamp = strtotime('next '.$dayofweekenglish[$dayofweek]);
				$datumParsed = date("Y-m-d\TH:i:s\&plus;O", $timestamp);
				$prijzen = BASE_URL.'ajax/prijsJSON/'.urlencode($result['van']).'/'.urlencode($result['naar']).'/'.$datumParsed;
				$prijzen = json_decode(file_get_contents($prijzen),true);
				
				$results[$i]['timestamp'] = $timestamp;
				$results[$i]['vol_tarief'] = $prijzen['vol_tarief'];
				$results[$i]['nieuw_tarief'] = $prijzen['nieuw_tarief'];
			}
		}

		return $results;
	}


	public function getTrajectMatches($from, $to)
	{
		$from = urldecode($from);
		$to = urldecode($to);
	
		$stmt = $this->connection->prepare("
			SELECT gebruiker, traject, van, naar, vertrektijd, 
			maandag, dinsdag, woensdag, donderdag, vrijdag, zaterdag, zondag 
			FROM `trajectlinks` 
			INNER JOIN trajecten 
			ON trajectlinks.traject = trajecten.id 
			WHERE trajecten.van = :from 
			AND trajecten.naar = :to 
			ORDER BY vertrektijd ASC");
		$stmt->bindParam(':from', $from, PDO::PARAM_STR);
		$stmt->bindParam(':to', $to, PDO::PARAM_STR);
		$stmt->execute();

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $results;
	}
	
	public function getVertrekstationMatches($from)
	{
		$from = urldecode($from);
	
		$stmt = $this->connection->prepare("
			SELECT naar
			FROM `trajectlinks` 
			INNER JOIN trajecten 
			ON trajectlinks.traject = trajecten.id 
			WHERE trajecten.van = :from 
			GROUP BY naar 
			ORDER BY vertrektijd ASC");
		$stmt->bindParam(':from', $from, PDO::PARAM_STR);
		$stmt->execute();

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $results;
	}

	public function insertTraject($user_id, $from, $to, $time, $dagen)
	{
		$time = date("H:i:s", strtotime($time));

		$i = 1;
		while($i < 8)
		{
			if(!isset($dagen[$i]))
			{
				$dagen[$i] = '0';
			}
			else
			{
				$dage[$i] = '1';
			}
			$i++;
		}
				
		$stmt = $this->connection->prepare("
			SELECT 
				count(gebruiker)AS count 
			FROM `trajectlinks` 
			WHERE traject IN(
				SELECT 
					id 
				FROM `trajecten` 
				WHERE van = :from 
				AND naar = :to
			) 
			AND vertrektijd = :time 
			AND gebruiker = :user_id 
			AND maandag = :maandag
			AND dinsdag = :dinsdag
			AND woensdag = :woensdag
			AND donderdag = :donderdag
			AND vrijdag = :vrijdag
			AND zaterdag = :zaterdag
			AND zondag = :zondag");
		$stmt->bindParam(':from', $from, PDO::PARAM_STR);
		$stmt->bindParam(':to', $to, PDO::PARAM_STR);
		$stmt->bindParam(':time', $time, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
		
		$stmt->bindParam(':maandag', $dagen[1], PDO::PARAM_STR);
		$stmt->bindParam(':dinsdag', $dagen[2], PDO::PARAM_STR);
		$stmt->bindParam(':woensdag', $dagen[3], PDO::PARAM_STR);
		$stmt->bindParam(':donderdag', $dagen[4], PDO::PARAM_STR);
		$stmt->bindParam(':vrijdag', $dagen[5], PDO::PARAM_STR);
		$stmt->bindParam(':zaterdag', $dagen[6], PDO::PARAM_STR);
		$stmt->bindParam(':zondag', $dagen[7], PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if($result['count'] > 0)
		{
			// Traject en koppeling bestaat al.
			return TRUE;
		}
		else
		{
			// De trajectlink is nog niet gelegd.			
			$stmt = $this->connection->prepare("
				SELECT id 
				FROM `trajecten` 
				WHERE van = :from 
				AND naar = :to 
				LIMIT 1");
			$stmt->bindParam(':from', $from, PDO::PARAM_STR);
			$stmt->bindParam(':to', $to, PDO::PARAM_STR);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			
			if(is_numeric($result['id']))
			{
				// Het traject zelf is al wel opgenomen in de database.
				try
				{
				
					$stmt = $this->connection->prepare("
						INSERT INTO trajectlinks 
							(gebruiker,
							 traject,
							 vertrektijd, 
							 maandag, 
							 dinsdag, 
							 woensdag, 
							 donderdag, 
							 vrijdag, 
							 zaterdag, 
							zondag) 
						VALUES 
							(:social_id,
							 :traject_id,
							 :vertrektijd, 
							 :maandag,
							 :dinsdag,
							 :woensdag,
							 :donderdag,
							 :vrijdag,
							 :zaterdag,
							 :zondag)");
					$stmt->bindParam(':social_id', $user_id, PDO::PARAM_STR);
					$stmt->bindParam(':traject_id', $result['id'], PDO::PARAM_STR);
					$stmt->bindParam(':vertrektijd', $time, PDO::PARAM_STR);
		
					$stmt->bindParam(':maandag', $dagen[1], PDO::PARAM_STR);
					$stmt->bindParam(':dinsdag', $dagen[2], PDO::PARAM_STR);
					$stmt->bindParam(':woensdag', $dagen[3], PDO::PARAM_STR);
					$stmt->bindParam(':donderdag', $dagen[4], PDO::PARAM_STR);
					$stmt->bindParam(':vrijdag', $dagen[5], PDO::PARAM_STR);
					$stmt->bindParam(':zaterdag', $dagen[6], PDO::PARAM_STR);
					$stmt->bindParam(':zondag', $dagen[7], PDO::PARAM_STR);
			
					if($stmt->execute())
					{
						return TRUE;
					}
					else
					{
						return FALSE;
					}
				}
				catch(PDOException $Exception)
				{
					error_log('Error: '.$Exception->getMessage());
					return FALSE;
				}
			}
			else
			{
				// Ook het traject bestaat niet in de database.
				try
				{
				
					$stmt = $this->connection->prepare("
						INSERT INTO trajecten 
							(van, naar) 
						VALUES 
							(:van, :naar)");
					$stmt->bindParam(':van', $from, PDO::PARAM_STR);
					$stmt->bindParam(':naar', $to, PDO::PARAM_STR);
			
					if($stmt->execute())
					{
						$stmt = $this->connection->prepare("
							SELECT id 
							FROM `trajecten` 
							WHERE van = :from 
							AND naar = :to 
							LIMIT 1");
						$stmt->bindParam(':from', $from, PDO::PARAM_STR);
						$stmt->bindParam(':to', $to, PDO::PARAM_STR);
						$stmt->execute();
						$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
						if(is_numeric($result['id']))
						{
							$stmt = $this->connection->prepare("
								INSERT INTO trajectlinks 
									(gebruiker,
									 traject, 
									 vertrektijd, 
									 maandag, 
									 dinsdag, 
									 woensdag, 
									 donderdag, 
									 vrijdag, 
									 zaterdag, 
									zondag) 
								VALUES 
									(:social_id,
									 :traject_id,
									 :vertrektijd, 
									 :maandag,
									 :dinsdag,
									 :woensdag,
									 :donderdag,
									 :vrijdag,
									 :zaterdag,
									 :zondag)");
							$stmt->bindParam(':social_id', $user_id, PDO::PARAM_STR);
							$stmt->bindParam(':traject_id', $result['id'], PDO::PARAM_STR);
							$stmt->bindParam(':vertrektijd', $time, PDO::PARAM_STR);
		
							$stmt->bindParam(':maandag', $dagen[1], PDO::PARAM_STR);
							$stmt->bindParam(':dinsdag', $dagen[2], PDO::PARAM_STR);
							$stmt->bindParam(':woensdag', $dagen[3], PDO::PARAM_STR);
							$stmt->bindParam(':donderdag', $dagen[4], PDO::PARAM_STR);
							$stmt->bindParam(':vrijdag', $dagen[5], PDO::PARAM_STR);
							$stmt->bindParam(':zaterdag', $dagen[6], PDO::PARAM_STR);
							$stmt->bindParam(':zondag', $dagen[7], PDO::PARAM_STR);

							if($stmt->execute())
							{
								return TRUE;
							}
							else
							{
								return FALSE;
							}
						}
						else
						{
							return FALSE;
						}
					}
					else
					{
						return FALSE;
					}
				}
				catch(PDOException $Exception)
				{
					error_log('Error: '.$Exception->getMessage());
					return FALSE;
				}
				return FALSE;
			}
		}
	}
	
	public function deleteTraject($user_id, $from, $to, $time)
	{
		$time = date("H:i:s", strtotime($time));
		
		error_log($from.' en '.$to.' en '.$time.' en '.$user_id);
	
		$stmt = $this->connection->prepare("
			DELETE FROM trajectlinks 
			WHERE traject 
				IN(	SELECT id 
					FROM trajecten 
					WHERE van = :from 
					AND naar = :to) 
			AND vertrektijd = :time 
			AND gebruiker = :user_id");
		$stmt->bindParam(':from', $from, PDO::PARAM_STR);
		$stmt->bindParam(':to', $to, PDO::PARAM_STR);
		$stmt->bindParam(':time', $time, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);

		if($stmt->execute())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function userTrajects($user_id)
	{
		$stmt = $this->connection->prepare("
			SELECT
				trajecten.van AS van, 
				trajecten.naar AS naar, 
				trajecten.id AS id, 
				trajectlinks.vertrektijd AS vertrektijd, 
				trajectlinks.maandag AS maandag, 
				trajectlinks.dinsdag AS dinsdag, 
				trajectlinks.woensdag AS woensdag, 
				trajectlinks.donderdag AS donderdag, 
				trajectlinks.vrijdag AS vrijdag, 
				trajectlinks.zaterdag AS zaterdag, 
				trajectlinks.zondag AS zondag 
			FROM trajectlinks 
			INNER JOIN trajecten 
			ON trajectlinks.traject = trajecten.id 
			WHERE trajectlinks.gebruiker = :user_id;");
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
		$stmt->execute();

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results;
	}
	
	public function getTrajectId($van, $naar)
	{
		$stmt = $this->connection->prepare("
			SELECT id
			FROM trajecten 
			WHERE van = :van AND naar = :naar;");

		$stmt->bindParam(':van', $van, PDO::PARAM_STR);
		$stmt->bindParam(':naar', $naar, PDO::PARAM_STR);

		$stmt->execute();

		$results = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $results['id'];
	}

}

?>
