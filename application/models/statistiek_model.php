<?php

/**
* Statistiek Model
*
* Bevat alle database communicatie voor de statistieken, zal het meest gebruikt worden in het admin panel.
*
* @author	Olivier Jansen
* @author	Ruben Steendam
* @package	Meereizen.nl
*/

class Statistiek_model extends Model {

	public function countMeereizenTotaal()
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM meereizen;");
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result['count'];
	}
	
	public function countMeereizenAfgerond()
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM meereizen
			WHERE afgerond = 1;");
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result['count'];
	}

	public function countMeereizenVerwijderd()
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM log
			WHERE type = 'delete';");
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result['count'];
	}

	public function countMeereizenRefund()
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM log
			WHERE type = 'refund';");
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result['count'];
	}

	public function countBetalingen()
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM log
			WHERE type = 'betaling';");
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result['count'];
	}
	
	public function getLatestEvents($num=10)
	{
		$stmt = $this->connection->prepare("
			SELECT
				*
			FROM log
			ORDER BY id DESC
			LIMIT :num;");
		$stmt->bindParam(':num', $num, PDO::PARAM_INT);

		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results;
	}

	public function countGebruikers($provider='Facebook')
	{
		$stmt = $this->connection->prepare("
			SELECT
				COUNT(id) AS count
			FROM gebruikers
			WHERE social_provider = :provider;");
		$stmt->bindParam(':provider', $provider, PDO::PARAM_INT);

		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result['count'];
	}
}

?>