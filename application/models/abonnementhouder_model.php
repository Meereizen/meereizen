<?php

/**
* Abonnementhouder Model
*
* Bevat alle database communicatie voor de abonnementhouders. Wordt voornamelijk aangeroepen door de abonnementhouder controller of helpers.
*
* @author	Olivier Jansen
* @author	Ruben Steendam
* @package	Meereizen.nl
*/

class Abonnementhouder_model extends Model {
	
	public function getAllData($id)
	{
		$stmt = $this->connection->prepare("SELECT * FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}
	
	public function getPublicData($id)
	{
		$stmt = $this->connection->prepare("SELECT naam, geslacht, email, telefoon, foto FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	public function getAllRegids($id)
	{
		$stmt = $this->connection->prepare("SELECT regid FROM gebruikerapplinks WHERE user_id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
	
	public function getSocialId($id)
	{
		$stmt = $this->connection->prepare("SELECT social_id FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['social_id'];
	}
	
	public function getNaam($id)
	{
		$stmt = $this->connection->prepare("SELECT naam FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['naam'])){
			return $result['naam'];
		}else{
			return FALSE;
		}
	}
	
	public function getPaypal($id)
	{
		$stmt = $this->connection->prepare("SELECT paypal FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['paypal'])){
			return $result['paypal'];
		}else{
			return FALSE;
		}
	}
	
	public function getEmail($id)
	{
		$stmt = $this->connection->prepare("SELECT email FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['email'])){
			return $result['email'];
		}else{
			return FALSE;
		}
	}
	
	public function getRememberHash($id)
	{
		$stmt = $this->connection->prepare("SELECT rememberhash FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['rememberhash'])){
			return $result['rememberhash'];
		}else{
			return FALSE;
		}
	}

	public function getId($social_id, $social_provider)
	{
		$stmt = $this->connection->prepare("SELECT id FROM gebruikers WHERE social_id = :social_id AND social_provider = :social_provider");
		$stmt->bindParam(':social_id', $social_id, PDO::PARAM_STR);
		$stmt->bindParam(':social_provider', $social_provider, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(is_numeric($result['id'])){
			return $result['id'];
		}else{
			return FALSE;
		}
	}
	
	public function isUser($id)
	{
		$stmt = $this->connection->prepare("SELECT COUNT(id) AS count FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['count']) && $result['count']==1){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function isAdmin($id)
	{
		$stmt = $this->connection->prepare("SELECT COUNT(userid) AS count FROM gebruikersrechten WHERE userid = :userid AND admin = 1");
		$stmt->bindParam(':userid', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['count']) && $result['count']==1){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function isActivated($id)
	{
		$stmt = $this->connection->prepare("SELECT activated FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['activated']) && $result['activated']==1){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function isUnverified($id)
	{
		$stmt = $this->connection->prepare("SELECT activated FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if(isset($result['activated']) && $result['activated']==2){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function activateUser($id, $array, $code)
	{
		// Vereiste arraywaardes: email, telefoon
		if(isset($array['email']) && isset($array['telefoon']) && isset($array['paypal']))
		{
			$stmt = $this->connection->prepare("UPDATE gebruikers SET activated = 2, email_code = :code, email = :email, telefoon = :telefoon, paypal = :paypal WHERE id = :id");
			$stmt->bindParam(':code', $code, PDO::PARAM_STR);
			$stmt->bindParam(':email', $array['email'], PDO::PARAM_STR);
			$stmt->bindParam(':telefoon', $array['telefoon'], PDO::PARAM_STR);
			$stmt->bindParam(':paypal', $array['paypal'], PDO::PARAM_STR);
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
	
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function updateEmailCode($id, $code)
	{
		$stmt = $this->connection->prepare("SELECT email_code, activated FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if($result['activated']==2 && !empty($result['email_code']))
		{
			$stmt = $this->connection->prepare("UPDATE gebruikers SET activated = 2, email_code = :code WHERE id = :id");
			$stmt->bindParam(':code', $code, PDO::PARAM_STR);
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	public function updateEmail($id, $email)
	{
		$stmt = $this->connection->prepare("UPDATE gebruikers SET email = :email WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		if($stmt->execute())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function updateRememberHash($id, $hash)
	{
		$stmt = $this->connection->prepare("UPDATE gebruikers SET rememberhash = :rememberhash WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->bindParam(':rememberhash', $hash, PDO::PARAM_STR);
		if($stmt->execute())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function updateContactinfo($id, $data)
	{
		if(isset($data['email']) && isset($data['telefoon']))
		{
		
			$data['email'] = (string) $data['email'];
		
			$stmt = $this->connection->prepare("UPDATE gebruikers SET email = :email, telefoon = :telefoon, paypal = :paypal WHERE id = :id");
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			$stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
			$stmt->bindParam(':telefoon', $data['telefoon'], PDO::PARAM_STR);
			$stmt->bindParam(':paypal', $data['paypal'], PDO::PARAM_STR);
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function bevestigEmail($id, $code)
	{
		$stmt = $this->connection->prepare("SELECT email_code, activated FROM gebruikers WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if($result['activated']==2 && $result['email_code'] == $code)
		{
			$stmt = $this->connection->prepare("UPDATE gebruikers SET activated = 1, email_code = '' WHERE id = :id");
			$stmt->bindParam(':id', $id, PDO::PARAM_STR);
			if($stmt->execute())
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	public function insertUser($array)
	{
		if(!isset($array['foto']))
		{
			$array['foto'] = '';
		}
		
		$stmt = $this->connection->prepare("INSERT INTO gebruikers (social_id, social_provider, activated, naam, geslacht, email, foto) VALUES (:social_id, :social_provider, '0', :naam, :geslacht, :email, :foto)");

		$stmt->bindParam(':social_id', $array['social_id'], PDO::PARAM_STR);
		$stmt->bindParam(':social_provider', $array['social_provider'], PDO::PARAM_STR);
		$stmt->bindParam(':naam', $array['naam'], PDO::PARAM_STR);
		$stmt->bindParam(':geslacht', $array['geslacht'], PDO::PARAM_STR);
		$stmt->bindParam(':email', $array['email'], PDO::PARAM_STR);
		$stmt->bindParam(':foto', $array['foto'], PDO::PARAM_STR);

		if($stmt->execute()==1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function insertRegId($id, $regid)
	{
		if($regid == 'null') return TRUE;
		$stmt = $this->connection->prepare("REPLACE INTO gebruikerapplinks (user_id, regid) values(:user_id, :regid)");

		$stmt->bindParam(':user_id', $id, PDO::PARAM_STR);
		$stmt->bindParam(':regid', $regid, PDO::PARAM_STR);

		if($stmt->execute()==1)
		{
			error_log('Nieuwe gebruiker heeft ingelogd via de app!');
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}

?>
