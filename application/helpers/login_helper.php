<?php

/**
* Login Helper
*
* Bevat functies die helpen bij het inloggen. Alle login sessies worden vanaf hier gestart.
*
* @author	Olivier Jansen
* @package	Meereizen.nl
*/

class Login_helper {

	function loggedIn($model)
	{
		if(isset($_SESSION['mr_logged']) && isset($_SESSION['mr_user_agent']))
		{
			if($_SESSION['mr_user_agent'] == md5($_SERVER['HTTP_USER_AGENT'])){					
				$social_id = $model->getSocialId($_SESSION['mr_id']);
				
				$mr_logged = sha1($_SESSION['mr_id'].$social_id);
								
				if($_SESSION['mr_logged']==$mr_logged)
				{
					if(isset($_SESSION['regid']) && !empty($_SESSION['regid']))
					{
						$model->insertRegId($_SESSION['mr_id'], $_SESSION['regid']);
					}
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		elseif(isset($_COOKIE['remember']))
		{
 			$cookie = explode(':', $_COOKIE['remember']);
 			$rememberHash = $model->getRememberHash($cookie[0]);

 			if(!empty($rememberHash))
 			{
 				if($rememberHash == $_COOKIE['remember'] && $_COOKIE['remember'] !== '')
 				{
 					if($this->logInSessions($cookie[0], $model->getSocialId($cookie[0]), $model->getNaam($cookie[0])))
 					{
 						return TRUE;
 					}
 					else
 					{
 						return FALSE;
 					}
 				}
 				else
 				{
 					return FALSE;
 				}
 			}
 			else
 			{
 				return FALSE;
 			}
		}
		else
		{
			return FALSE;
		}
	}

	function getCurrentId() {
		return $_SESSION['mr_id'];
	}
	
	function logInSessions($id, $social_id, $name='Je Moeder')
	{
		$_SESSION['mr_id'] = $id;
		$_SESSION['mr_logged'] = sha1($id.$social_id);
		$_SESSION['mr_user_agent'] = md5($_SERVER['HTTP_USER_AGENT']);
		$_SESSION['mr_name'] = $name;

		return TRUE;
	}
	
	function logOut($model)
	{
		$model->updateRememberHash($_SESSION['mr_id'], '');

		unset($_SESSION['mr_id']);
		unset($_SESSION['mr_logged']);
		unset($_SESSION['mr_user_agent']);
		setcookie('remember', '', time() - 3600, '/');
	}

}

?>