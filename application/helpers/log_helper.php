<?php

/**
* Log Helper
*
* Bevat functies die helpen bij het loggen van gebeurtenissen naar de database.
*
* @author	Olivier Jansen
* @package	Meereizen.nl
*/

class Log_helper {

	private $connection;

	function __construct()
	{
		// Config gegevens voor de log-database.
		$this->connection = new PDO('mysql:host=195.8.208.76;dbname=vleckanie', 'oli4', '151294jans&');
	}

	function log($type, $comment)
	{
		try {
		    $sqlQuery = "INSERT DELAYED INTO log (type, datetime, comments) VALUES ('".$type."', NOW(), '".$comment."');";
		    if(!$this->connection->query($sqlQuery)) {
		    	error_log("----------- ERROR TIJDENS LOGGEN: log niet in database geplaatst");
		    }

		} catch (PDOException $e) {
		    error_log("----------- ERROR TIJDENS LOGGEN: " . $e->getMessage());
		    die();
		}
	}

}

?>