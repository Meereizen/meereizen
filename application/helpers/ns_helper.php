<?php

/*
*
*	NS_helper helpt bij het ophalen van gegevens vanuit de NS API en verzorgt ook de caching.
*
*/

class NS_helper {

	// Config
	private $_email = 'oli4jansen.nl@gmail.com';
	private $_password = 'lztt8Kbri44tlYlcvvbiZJ-oxoo-qcif4j5dIAciwuPGqdehAP0tiA';
	private $_cacheLocation = 'application/cache';

	private $_url = '';
	
	function setUrl($url)
	{
		$this->_url = 'http://'.$this->_email.':'.$this->_password.'@'.$url;
	}
	
	function getFile($cacheName, $ageInSeconds)
	{
		// Cachelocatie instellen
		$cacheName = $this->_cacheLocation.$cacheName;

		// Kijken of de cache al bestaat OF wanneer het bestand aangemaakt is en of het al verlopen is.
		if(!file_exists($cacheName) || filemtime($cacheName) + $ageInSeconds < time())
		{
			// Nieuwe API request + cache schrijven
			error_log('API request naar NS.');
			$contents = file_get_contents($this->_url);
			file_put_contents($cacheName, $contents);
		}
		
		// Bestand uit de cache ophalen
		$result = file_get_contents($cacheName);

		return $result;
	}
	
	/*
	*	Requests die de getFile() functie (kunnen) gebruiken.
	*/
	
	function getStationList()
	{
		$this->setUrl('webservices.ns.nl/ns-api-stations-v2');

		$cacheName = '/stationslijst.xml.cache';
		$ageInSeconds = 3600 * 24 * 7;
		
		$file = $this->getFile($cacheName, $ageInSeconds);

		error_log('getStationList voorbereiden.');
		
		return $file;
	}
	
	
	function getPrijs($from='Almere+Muziekwijk', $to='Delft', $time=null)
	{
		if($time==null)
		{
			$time = date("Y-m-d\TH:i:s\&plus;O", time());
		}
		
		$this->setUrl('webservices.ns.nl/ns-api-prijzen-v2?from='.$from.'&to='.$to.'&dateTime='.$time);

		$cacheName = '/prijzen/prijs'.$from.'-'.$to.'-'.$time.'.xml.cache';
		$ageInSeconds = 60 * 60 * 24 * 7;
		
		$file = $this->getFile($cacheName, $ageInSeconds);

		error_log('getPrijs voorbereiden.');
		
		return $file;
	}
	
	function getPlanner($from='Almere+Muziekwijk', $to='Delft', $dateTime=null, $previous=1, $next=5, $departure=true)
	{
		if($dateTime==null)
		{
			$dateTime = date("Y-m-d\TH:i:s\&plus;O", time());
		}
	
		$this->setUrl("webservices.ns.nl/ns-api-treinplanner?fromStation=".$from."&toStation=".$to."&previousAdvices=".$previous."&nextAdvices=".$next."&dateTime=".$dateTime."&departure=".$departure);
		
		$cacheName = '/planner/'.$from.'-'.$to.'-'.$dateTime.'-'.$previous.'+'.$next.'.xml.cache';
		$ageInSeconds = 60 * 60 * 24 * 7;
		
		$file = $this->getFile($cacheName, $ageInSeconds);

		error_log('getPlanner voorbereiden.');
		
		return $file;
	}
	
	/*
	*	Requests met een apart caching systeem.
	*/

	function getTussenstations($from, $to)
	{
		$this->setUrl('webservices.ns.nl/ns-api-treinplanner?fromStation='.$from.'&toStation='.$to);

		// Cachet naar bijv. /trajecten/trajectAlmere+Muziekwijk-Eindhoven.json.cache
		$cacheName = '/trajecten/traject'.$from.'-'.$to.'.json.cache';

		// Verloopt na een week
		$ageInSeconds = 3600 * 24 * 7;

		if(!file_exists($cacheName) || filemtime($cacheName) + $ageInSeconds < time())
		{
			error_log('API request voor tussenstations.');

			// Een API request plaatsen en response ophalen
			$file = simplexml_load_file($this->_url);
			
			// De response verwerken tot een JSON bestand met alle mogelijke tussenstations
			$reisMogelijkheden = $file->ReisMogelijkheid;
			$tussenStations = Array();			

			foreach($reisMogelijkheden as $reisMogelijkheid)
			{
				foreach($reisMogelijkheid->ReisDeel as $reisDeel)
				{
					foreach($reisDeel->ReisStop as $reisStop)
					{
						if(!in_array($reisStop->Naam, $tussenStations))
						{
							$tussenStations[] = (string) $reisStop->Naam;
						}
					}
				}

			}

			// Schrijf het bestand naar de cache folder			
			$fp = fopen($cacheName, 'w');
			fwrite($fp, json_encode($tussenStations));
			fclose($fp);
		}
		
		$result = file_get_contents($cacheName);
		
		return $result;
	}

}

?>