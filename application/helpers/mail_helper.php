<?php

class Mail_helper {

	// Verstuur vraagt om mail object, adres van ontvanger, onderwerp en een array met het bericht in plain text en html
	public function verstuur($mail, $adres, $subject, $body)
	{
		$mail->IsSMTP();
		$mail->SMTPDebug  = 0;
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = '587';
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;
		
		$mail->Username = 'noreplymeereizen@gmail.com';
		$mail->Password = 'meereizeniscool';
						
		$mail->From = 'noreplymeereizen@gmail.com';
		$mail->FromName = 'Meereizen.nl';
		$mail->ClearAllRecipients();
		$mail->AddAddress($adres, $adres);
					
		$mail->IsHTML(true);
		$mail->Subject = $subject;

		ob_start();
		?>
		<body style="padding:0;margin:0;">
		<div style=";margin: 0 2%;border-radius:0 0 3px 3px;background-color:#FF6242;padding: 15px;font-size: 19px;color:white;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;"><img src="http://oli4jansen.nl:81/static/images/logo-light.png" height="40" width="59"></div>

		<?php
		$i = 0;
		$total = count($body);
		foreach($body as $segment)
		{
			$i++;
			?>
			<p style="<?php 
			if($i == 1 || $i == $total)
			{
				?>font-size:1.3em;<?php
			}
			?>width:90%;margin: 20px 0 0;padding: 0 5%;max-size:800px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height: 1.9em;-webkit-font-smoothing: antialiased;">
			<?php echo $segment; ?>
			</p>
			<?php
		}
		?>
		</body>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		$mail->Body = $body;
		
		if(!$mail->Send())
		{
			return FALSE;
		}
		else
		{
			error_log('Mail verstuurd naar '.$adres);
			return TRUE;
		}
	}

	public function verstuurBevestigingsMail($url, $mail, $adres)
	{	
		$subject = 'Bevestig uw account op Meereizen.nu';
		
		$body[] = 'Beste abonnementhouder,';
		$body[] = 'Om er zeker van te zijn of dit e-mailadres echt van u is, hebben wij u dit bericht gestuurd. Om dit aan ons te laten weten en uw account te bevestigen, klikt u op de volgende link.';
		$body[] = '<a href="'.$url.'">'.$url.'</a>';
		$body[] = ' - Meereizen';

		if(!$this->verstuur($mail, $adres, $subject, $body))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function verstuurBetaaldMail($gegevens, $mail, $emails)
	{
		// Mail aan meereiziger
		$subject = 'Contactgegevens abonnementhouder';
		$info = json_encode($gegevens['abonnementhouder']);

		$body[] = 'Beste gebruiker van Meereizen,';
		$body[] = 'Lorem ipsum hier zijn de contactgegevens van de abonnementhouder:';
		$body[] = '<pre>'.$info.'</pre>';
		$body[] = 'Kom met elkaar in contact om een geschikte locatie te vinden om af te spreken.Stuur een mail of sms naar de abonnementhouder om een afspraak te maken. Zodra jullie een afspraak hebben is het laatste wat je nog moet doen de volgende code doorgeven:';
		$body[] = '<b style="font-size:2em">'.$gegevens['code'].'</b>';
		$body[] = ' - Meereizen';

		$status = TRUE;

		if(!$this->verstuur($mail, $emails['reiziger'], $subject, $body))
		{
			$status = FALSE;
		}

		// Mail aan abonnementhouder
		$subject = 'Contactgegevens meereiziger';
		$info = json_encode($gegevens['reiziger']);

		$body[] = 'Beste abonnementhouder,';
		$body[] = 'Lorem ipsum hier zijn de contactgegevens van de meereiziger:';
		$body[] = '<pre>'.$info.'</pre>';
		$body[] = ' - Meereizen';

		$status = TRUE;

		if(!$this->verstuur($mail, $emails['abonnementhouder'], $subject, $body))
		{
			$status = FALSE;
		}

		return $status;
	}

	public function verstuurMeereisMail($url, $mail, $adres, $van, $naar)
	{
		$subject = 'Een nieuwe Meereis!';
		
		$body[] = 'Beste abonnementhouder,';
		$body[] = 'Iemand wil met je Meereizen van <b>'.$van.'</b> naar <b>'.$naar.'</b>. Op je dashboard kun je deze gebruiker accepteren of weigeren.';
		$body[] = '<a href="'.BASE_URL.'abonnementhouder">Ga naar dashboard</a>';
		$body[] = ' - Meereizen';
		if(!$this->verstuur($mail, $adres, $subject, $body))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function verstuurMeereisAcceptedMail($url, $mail, $adres)
	{
		$subject = 'De Meereis is geaccepteerd';
		
		$body[] = 'Beste gebruiker van Meereizen,';
		$body[] = 'De abonnementhouder heeft je aanvraag om mee te reizen geaccepteerd! Klik op de onderstaande link om te betalen en zijn of haar contactgegevens te ontvangen.';
		$body[] = '<a href="'.$url.'">'.$url.'</a>';
		$body[] = ' - Meereizen';

		if(!$this->verstuur($mail, $adres, $subject, $body))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function verstuurMeereisRejectedMail($mail, $adres)
	{
		$subject = 'De Meereis is geweigerd';
		
		$body[] = 'Beste gebruiker van Meereizen,';
		$body[] = 'De abonnementhouder heeft je aanvraag om mee te reizen niet geaccepteerd.';
		$body[] = ' - Meereizen';

		if(!$this->verstuur($mail, $adres, $subject, $body))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}

?>