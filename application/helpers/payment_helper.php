<?php

/*
*
*	Payment_helper helpt bij het betalen van koppelingen.
*
*/

class Payment_helper {

	private $meereizenPaypal = 'noreplymeereizen@gmail.com';
	static $pluginLoaded = FALSE;

	public function __construct($loadPlugin = TRUE)
	{
		if($loadPlugin)
		{
			self::$pluginLoaded = TRUE;
		}
	}

	// Vangt IPN berichten op, verifieert ze en geeft de data terug
	public function IPN()
	{
		$ipnMessage = new PPIPNMessage(null, Configuration::getConfig());
		$response = Array();

		foreach($ipnMessage->getRawData() as $key => $value)
		{
			$response['data'][$key] = $value;
			error_log("IPN: $key => $value");
		}

		$response['status'] = FALSE;
		

		/* VALIDATIE VAN IPN STAAT UIT OMDAT DIT NIET TE DOEN IS OP DE TEST SERVER.
		WANNEER HET SYSTEEM IN GEBRUIK GAAT IS HET SUPER BELANGRIJK DIT WEER AAN TE ZETTEN.

		if($ipnMessage->validate())
		{*/
			$response['status'] = TRUE;
		//}
		return $response;
	}

	public function executePayment($paykey)
	{
		$executePaymentRequest = new ExecutePaymentRequest(new RequestEnvelope("en_US"), $paykey);
		$executePaymentRequest->actionType = 'PAY';

		$service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());

		try
		{
			$response = $service->ExecutePayment($executePaymentRequest);
		}
		catch(Exception $ex)
		{
			error_log('Error type: '.get_class($ex));
			error_log('Message: '.$ex->getMessage());
			error_log('Detailled message: '.getDetailedExceptionMessage($ex));
			return FALSE;
		}

		$ack = strtoupper($response->responseEnvelope->ack);
		if($ack != "SUCCESS")
		{
			error_log('Paypal error: '.$response->error[0]->message);
			return FALSE;
		}
		else
		{
			return TRUE;
		}
		return FALSE;
	}

	public function refundPayment($paykey)
	{
		$refundRequest = new RefundRequest(new RequestEnvelope("en_US"));

		$refundRequest->currencyCode = 'EUR';
		$refundRequest->payKey = $paykey;

		$service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());

		try
		{
		    $response = $service->Refund($refundRequest);
		}
		catch(Exception $ex)
		{
		    error_log('Error type: '.get_class($ex));
		    error_log('Message: '.$ex->getMessage());
		    error_log('Detailled message: '.getDetailedExceptionMessage($ex));
		    return FALSE;
		}

		$ack = strtoupper($response->responseEnvelope->ack);
		if($ack != "SUCCESS")
		{
			error_log('Paypal error: '.$response->error[0]->message);
			return FALSE;
		}
		else
		{ 
	        return TRUE;
		}
		return FALSE;
	}

	public function payPaypalDelayedChained($ontvangers, $bedragen, $cancelUrl, $returnUrl, $ipn)
	{
		// De array die we gaan returnen
		$return = Array();

		// Totaalbedrag ophalen. Dit wordt het primary bedrag
		$primaryAmount = round($bedragen['primary'], 2, PHP_ROUND_HALF_DOWN);
		$secondaryAmount = round($bedragen['secondary'], 2, PHP_ROUND_HALF_DOWN);

		// Checken of de bedragen een beetje kloppen
		if(is_numeric($primaryAmount) && $primaryAmount < 100 && $secondaryAmount < $primaryAmount)
		{
			// Lege array met ontvangers
			$receiver = array();

			// Primary ontvanger is Meereizen
			$receiver[0] = new Receiver();
			$receiver[0]->email = $this->meereizenPaypal;
			$receiver[0]->amount = $primaryAmount;
			$receiver[0]->primary = 'true';

			// Secondary ontvanger is de abonnementhouder
			$receiver[1] = new Receiver();
			$receiver[1]->email = $ontvangers['secondary'];
			$receiver[1]->amount = $secondaryAmount;
			$receiver[1]->primary = 'false';

			// De shit in een list zetten zoals de SDK aangeeft
			$receiverList = new ReceiverList($receiver);

			// PayRequest instantie aanmaken en constanten instellen
			$payRequest = new PayRequest(new RequestEnvelope("en_US"), 'PAY_PRIMARY', $cancelUrl, 'EUR', $receiverList, $returnUrl);
			$payRequest->ipnNotificationUrl = $ipn;
			$payRequest->memo = 'Betaling aan Meereizen. Na verificatie wordt de abonnementhouder betaald. Indien dit binnen een maand na vertrek niet gebeurd is, wordt het geld automatisch terug gestort.';
				
			$service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());

			// Paypal proberen te bereiken
			try
			{
				$response = $service->Pay($payRequest);
			}
			catch(Exception $ex)
			{
				if (isset($ex) && $ex instanceof Exception)
				{
					// Er was een error tijdens het verbinden met Paypal
					$return['status'] = FALSE;
					$return['message'] = $ex->getMessage();
				}
				exit;
			}

			$ack = strtoupper($response->responseEnvelope->ack);
			if($ack != "SUCCESS")
			{
				// Er was een error van Paypal
				$return['status'] = FALSE;
				$return['message'] = $response->error[0]->message;
			}
			else
			{
				// Betaling aangemaakt, return TRUE, de url en de PayKey
				$return['status'] = TRUE;
				$return['url'] = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $response->payKey;
				$return['paykey'] = $response->payKey;
			}

		}
		else
		{
			// Nope, de bedragen zijn niet kloppend
			$return['status'] = FALSE;
			$return['message'] = 'De door Paypal ontvangen bedragen lijken niet geldig te zijn.';
		}

		return $return;
	}

}

if(!Payment_helper::$pluginLoaded && isset($this) && $this instanceOf Controller)
{
	$this->loadPlugin('paypal/paypal');
}