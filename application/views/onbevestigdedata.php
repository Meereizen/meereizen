<?php include('header.php'); ?>

<div class="col6">
<h1>Bevestigings e-mail verstuurd</h1>
	<br>
	<?php
	if(isset($verstuurd))
	{
		?>
		<div class="alert yellow">
			Wij hebben een e-mail verstuurd naar <b><?php echo $email; ?></b>. In deze e-mail vindt u een bevestigingslink. Klik hierop om uw account definitief te activeren.
		</div>
	<?php
	}
	?>
	<p>Uw account is klaar, we hebben alle vereiste gegevens van u. We weten echter niet zeker of uw e-mailadres wel echt van u is.. Daarom hebben wij u een e-mail verstuurd met een link erin. Zodra u op deze link klikt, weten wij dat u dit e-mailadres bezit en gebruikt.</p>
	
	<p>Is er iets fout gegaan, heeft u bijvoorbeeld geen e-mail ontvangen? Raadpleeg dan even de informatie die hiernaast staat. Houd er wel rekening mee dat het soms even, tot wel een kwartier, kan duren voordat de e-mail binnen komt in uw mailbox.</p>
	
	<div class="button-holder">
		<a href="http://www.gmail.com/" class="btn google"><i class="icon-external-link">&nbsp;</i> Gmail.com</a>
		<a href="http://www.outlook.com/" class="btn outlook"><i class="icon-external-link">&nbsp;</i> Outlook.com</a>
		<a href="http://mail.yahoo.com/" class="btn yahoo"><i class="icon-external-link">&nbsp;</i> Yahoo! Mail</a>
	</div>

</div><div class="col4 inloggen">
	
	<legend>Ik heb geen e-mail ontvangen.</legend>
	<br>
	<p>Het kan voorkomen dat de e-mail nooit is aangekomen bij u, maar dat is geen probleem. We sturen u graag nog een e-mail.</p>
	<input type="submit" value="Stuur mij nog een e-mail" onclick="window.location.href='<?php echo BASE_URL;?>abonnementhouder/herstuur'" class="btn">

	<legend>Mijn e-mailadres klopt niet.</legend>
	<br>
	<p>Als u een onjuist e-mailadres ingevuld heeft, is onze e-mail naar de verkeerde persoon verstuurd. U hoeft zich geen zorgen te maken dat hij of zij toegang heeft tot uw account. Hieronder kunt u uw e-mailadres wijzigen en meteen een e-mail laten versturen.</p>
	<form action="<?php echo BASE_URL;?>abonnementhouder/wijzigherstuur" method="post">
		<input type="text" name="email" placeholder="Uw juiste e-mailadres" value="<?php echo $email; ?>"><br>
		<input type="submit" value="Update e-mailadres en verstuur e-mail" class="btn">
	</form>

</div>

<?php include('footer.php'); ?>