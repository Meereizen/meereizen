<?php
$title = 'Reisplanner';
include('header.php');
?>

<ul class="light-menu desktop-only">
	<li><a href="#introductie" class="current">Introductie</a></li>
	<li><a href="#uitgelicht">Uitgelichte reizen</a></li>
</ul>

<div class="uitleg-item uitleg-first desktop-only" id="introductie">
	<div class="uitleg-icon">
		<i class="ion-ios7-paperplane-outline"></i>
	</div><div class="uitleg-content">
		<h1>Reis samen, geniet van kortingen.</h1>
		<p>Door samen te reizen met een abonnementhouder mag je kaartjes met korting kopen. Meereizen zorgt voor een snelle koppeling, waarna jij 20% korting op je treinreis krijgt.</p>
	</div>
</div>

<div class="uitleg-item desktop-only">
	<div class="uitleg-content">
		<h1>Verdien geld met je abonnement.</h1>
		<p>Zodra je als abonnementhouder een reiziger mee neemt, krijg je van Meereizen 10% van de trajectprijs terug. Op 1 abonnement kun je tot wel 3 personen mee laten reizen.</p>
	</div><div class="uitleg-icon">
		<i class="ion-ios7-pricetag-outline"></i>
	</div>
</div>

<div class="uitleg-item uitleg-last desktop-only">
	<div class="uitleg-icon">
		<i class="ion-iphone"></i>
	</div><div class="uitleg-content">
		<h1>Meereizen is overal.</h1>
		<p>Gebruik Meereizen op je computer, tablet of telefoon. Ga naar Meereizen.nu, installeer de Android app of de iOS webapp (<a href="#">hoe?</a>).</p>
	</div>
</div>

<div class="uitleg-item uitleg-small border desktop-only">
	<div class="uitleg-content"><a href="<?php echo BASE_URL; ?>docs" class="btn right gray">Lees meer&nbsp;&nbsp;<i class="ion-chevron-right"></i></a><p>Lees meer over Meereizen in onze documentatie.</p></div>
</div>

<div class="desktop-only uitleg-item desktop-only" id="uitgelicht">
	<div class="uitleg-content">
	<?php
	$footerAlreadyRequested = true;
	include('uitgelicht.php');
	$footerAlreadyRequested = false;
	?>
	</div>
</div>

<?php
include('footer.php');
?>