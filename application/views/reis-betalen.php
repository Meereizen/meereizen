<?php
$title = 'Betalen';
include 'header.php';
?>

<div class="col6">
	<h1 class="desktop-only">Betaal en ontvang gegevens</h1>

	<div class="mobile-only android-header">Belangrijke informatie</div>

	<div class="mobile-only clickable-list">
		<div class="alert blue">Betreft: <?php echo $data['van']; ?> - <?php echo $data['naar']; ?> op <?php echo date('d-m-Y', strtotime($data['vertrektijd'])); ?> om <?php echo date('H:i', strtotime($data['vertrektijd'])); ?> uur.</div>
	</div>

	<p class="desktop-only">Het betreft hier de reis van <?php echo $data['van']; ?> naar <?php echo $data['naar']; ?> op <?php echo date('d-m-Y', strtotime($data['vertrektijd'])); ?> om <?php echo date('H:i', strtotime($data['vertrektijd'])); ?> uur.</p>

	<p class="clickable-list nohorizontalpadding">Deze reis kost <b>normaal gesproken &euro; <?php echo number_format($data['vol_tarief'], 2, ',', ' ') ?></b>. Betaal hier  &euro; <?php echo number_format($data['vol_tarief']*(MEEREIZEN_VOOR_MEEREIZEN + MEEREIZEN_VOOR_ABONNEMENTHOUDER), 2, ',', ' ') ?> en u ontvangt de gegevens van een abonnementhouder. Door met de abonnementhouder mee te reizen mag u op het station een kaartje kopen met 40% korting (&euro; <?php echo number_format($data['vol_tarief']*0.6, 2, ',', ' ') ?>). <b>U betaalt in totaal dus slechts  &euro; <?php echo number_format($data['nieuw_tarief'], 2, ',', ' ') ?>.</b></p>

	<div class="mobile-only android-header">Kosten</div>

	<form method="post" class="alert">
		<div class="one-fourth">
			<p class="desktop-only">Te betalen:</p>
			<span class="huge">
				&euro; <?php echo number_format($data['vol_tarief']*(MEEREIZEN_VOOR_MEEREIZEN + MEEREIZEN_VOOR_ABONNEMENTHOUDER), 2, ',', ' ') ?>
			</span>
		</div><div class="alert three-fourth">
			<p class="desktop-only">Betaal &euro; <?php echo number_format($data['vol_tarief']*(MEEREIZEN_VOOR_MEEREIZEN + MEEREIZEN_VOOR_ABONNEMENTHOUDER), 2, ',', ' ') ?> met je Paypal account of een creditcard.</p>
			
			<button type="submit" class="paypal large" name="submit">Betaal met Paypal <i class="icon-arrow-right"></i></button>
		</div>
	</form>

	
</div><div class="col4">
	<div class=" alert inloggen desktop-only">
		<h2>Reis goedgekeurd</h2>
				
		<p>Er is een reiziger met een abonnement gevonden die deze reis ook aflegt. Wat zijn de volgende stappen?</p>
				
		<ol>
			<li><strike>Laten weten dat jij mee wil reizen.</strike><br>
				<small class="help"><strike>Dit hebben wij net voor je gedaan.</strike></small>
			</li>
			<li><strike>Wachten op goedkeuring van de reiziger.</strike><br>
				<small class="help"><strike>Op deze manier is de reiziger ervan op de hoogte dat jij mee wil reizen.</strike></small>
			</li>
			<li>De reiziger betalen via ons systeem en zijn contactgegevens ontvangen.<br>
				<small class="help">Het geld is een kleine beloning voor de abonnementhouder en is zo'n 10% van de prijs van het treinkaartje.</small>
			</li>
		</ol>
	</div>
</div>

<?php
include 'footer.php';
?>