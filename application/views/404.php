<?php include('header.php'); ?>


<div class="center">
	<h1>Deze pagina bestaat niet.</h1>
	<div class="alert red">Deze pagina bestaat niet (meer). Het kan zijn dat u hier terecht gekomen bent door een typfout in de URL. Ook kan de link waar u op klikte 'dood' zijn. Is dat het geval, dan zouden we het graag van u horen.</div>
	Blijft u deze foutmelding zien? Neemt u dan alstublieft contact met ons op.
</div>

<?php include('footer.php'); ?>