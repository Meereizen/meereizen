<?php include('header.php'); ?>

<div class="col6">
	<h1>Activeer uw account</h1>
		
	<form class="full" method="post" action="<?php echo BASE_URL;?>abonnementhouder/activeer/">
	
	    <legend>Aanvullende contactinformatie</legend>

	    <p>
		    <label for="email">
			    Uw e-mailadres
			    <small class="help">U zult een e-mail ontvangen om uw adres te bevestigen</small>
			</label>
			<input type="email" id="email" name="email" placeholder="Bijv. olivier@meereizen.nl" value="<?php echo $data['email']; ?>" required>
		</p>

	    <p>
	    	<label for="telefoon">
	        	Uw mobiele telefoonnummer
	            <small class="help">Om u te bereiken in noodgevallen</small>
	        </label>
	        <input type="text" id="telefoon" name="telefoon" placeholder="Bijv. 0657750837" required>
	    </p>
	
	    <legend>Rekening informatie</legend>

	    <p>
		    <label for="paypal">
			    Uw Paypal rekeningadres (e-mail)
			    <small class="help">Nog geen Paypal rekening? <a href="https://www.paypal.com" target="_blank">Maak er dan makkelijk en snel een aan</a>.</small>
			</label>
			<input type="email" id="paypal" name="paypal" placeholder="Bijv. paypal@meereizen.nl" required>
		</p>
	    <input type="submit" value="Activeer account">
	</form>
</div><div class="col4">
	<div class="alert gray">
		<legend>Wat is de bedoeling?</legend>
		<br>
		
		<p>Zojuist heeft u voor het eerst ingelogd op Meereizen.nl. Voordat u verder kunt gaan hebben we informatie nodig over onder andere uw abonnement. Hierna kunt u gebruik maken van onze diensten en geld verdienen met uw abonnement.</p>
	</div>
	
	<div class="alert gray">
		<legend>Waarom Paypal?</legend>
		<br>
		
		<p>Paypal is een makkelijke en snelle manier om via het internet te betalen en betalingen te ontvangen. Bovendien is Paypal gratis en kan een Paypal rekening gekoppeld worden aan uw creditcard en bankrekening (dit is dus <u>niet</u> nodig).</p>
	</div>
</div>

<?php include('footer.php'); ?>