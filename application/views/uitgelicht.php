<?php
$title = 'Uitgelicht';
require_once('header.php');
?>

	<h1>Uitgelichte reizen.</h1>
	<p class="desktop-only">Om het vinden van een leuke reis makkelijker te maken..</p>
	
	<div class="android-header mobile-only">Vandaag vertrekkend</div>
	<div class="ajax-container clickable-list" data-type="uitgelicht-vandaag">
		<img src="<?php echo BASE_URL; ?>static/images/spinner.gif" width="16" height="16" class="ajax-loader-animation">
	</div>

<?php require_once('footer.php'); ?>