<?php
$matches = $result['matches'];

$title = 'Details';

if($matches>0)
{
	$formSubmitter = Array();
	$formSubmitter['form'] = 'form#reis-aanvragen';
	$formSubmitter['title'] = 'Aanvragen';
	$title = '&nbsp;';
}
include 'header.php';

if($matches>0)
{
}
else
{
	?>
	<div class="mobile-only android-header">Geen matches gevonden</div>

	<div class="clickable-list mobile-only">
		<p>We hebben geen reiziger met een abonnement gevonden die dezelfde reis aflegt. Probeer een ander traject of tijdstip of <b>gebruik de diepere zoekfunctie</b>.</p>			
	</div>
	<?php
}
?>

<div class="col3 plannen-title-prijs ajax-container desktop-only" data-type="prijs" data-from="<?php echo $from; ?>" data-to="<?php echo $to; ?>" data-date="<?php echo $dateParsed; ?>">
	<img src="<?php echo BASE_URL; ?>static/images/spinner.gif" width="16">
</div>
<div class="col7 plannen-title-wrap">
	<h1 class="nopadding nomargin center plannen-title">
		<span class="mobile-three-seventh table-cell" id="from" data-station="<?php echo $from; ?>">
			<?php echo urldecode($from);?>
		</span><span class="mobile-one-seventh mobile-icon-middle">
			&nbsp;<i class="icon-angle-right"></i>&nbsp;
		</span><span class="mobile-three-seventh table-cell" id="to" data-station="<?php echo $to; ?>">
			<?php echo urldecode($to);?>
		</span>
	</h1>
</div>

<hr class="desktop-only">

<div class="col6" id="reismogelijkheden-holder">
	<div class="android-header mobile-only">Reisinformatie</div>
	<div class="alert reismogelijkheid reismogelijkheid-selected clickable-list">
		<div class="mobile-only traject-info">
			<span class="vertrektijd"><?php echo $result['vertrekTijd']; ?></span>
			<div class="vertrekstation"><?php echo urldecode($from);?><span><?php echo $result['reisDelen'][0][0]['spoor'] ?></span></div>
			<div class="aankomststation"><?php echo urldecode($to);?><span><?php $laatsteReisdeel = end(end($result['reisDelen'])); echo $laatsteReisdeel['spoor']; ?></span></div>
			<span class="aankomsttijd"><?php echo $result['aankomstTijd']; ?></span>
		</div>

		<div class="desktop-only one-fifth field-vertrektijd mobile-one-fourth" data-value="<?php echo $result['vertrekTijd']; ?>">
			<span class="table-label">Vertrek</span>
			<span class="verylarge">
				<?php echo $result['vertrekTijd']; ?>
			</span>
		</div><div class="desktop-only one-fifth field-aankomsttijd mobile-one-fourth" data-value="<?php echo $result['aankomstTijd']; ?>">
			<span class="table-label">Aankomst</span>
			<span class="verylarge">
				<?php echo $result['aankomstTijd']; ?>
			</span>
		</div><div class="one-fifth field-reistijd mobile-one-fourth" data-value="<?php echo $result['reisTijd']; ?>">
			<span class="table-label">Reistijd</span>
			<span class="verylarge">
				<?php echo $result['reisTijd']; ?>
			</span>
		</div><div class="one-fifth field-overstappen mobile-one-fourth" data-value="<?php echo $result['overstappen']; ?>">
			<span class="table-label">Overstappen</span>
			<span class="verylarge ">
				<?php echo $result['overstappen']; ?>
			</span>
		</div><div class="one-fifth matches-count desktop-only">
			<span class="table-label">Matches</span>
			<span class="verylarge">
				<?php echo $result['matches']; ?>
			</span>
		</div><div class="one-fifth mobile-one-fourth mobile-only old-price" id="ajax-container">
			<span class="table-label">Normaal</span>
			<span class="verylarge">
				&euro; 12,00
			</span>
		</div><div class="one-fifth mobile-one-fourth mobile-only new-price" id="ajax-container">
			<span class="table-label">Meereisprijs</span>
			<span class="verylarge">
				&euro; 12,00
			</span>
		</div>
			
		<div class="mobile-only small center reismogelijkheid-detail"><?php echo $result['overstappen']; ?> overstap(pen) en <?php echo $result['matches']; ?> match(es)</div>

		<span class="info">
			<?php
			$i = 0;
			// Voor elk reisdeel..
			foreach($result['reisDelen'] as $reisDeel)
			{
					// Een lijst maken (= lijn met puntjes na CSS)
					echo '<ul>';
	
					// Voor elk station..
					foreach($reisDeel as $station)
					{
						// ..een puntje maken met tijd, naam en evt. spoor
						echo '<li>'.$station['tijd'].'&nbsp;-&nbsp;'.$station['naam'];
						if(isset($station['spoor']))
						{
							echo '<span class="right">spoor '.$station['spoor'].'</span>';
						}
						echo '</li>';						
					}
					echo '</ul>';
					$i++;
					
					// Als het aantal reisdelen nog niet afgegaan is maar deze wel ten einde is, betekent dat een overstap
					if($i < $result['aantalReisDelen'])
					{
						echo '<ul><li class="overstap">&nbsp;</li></ul>';
					}
			}
			?>
		</span>
	</div>
</div><?php

if($matches==0){

	?><div class="col4 matches desktop-only" id="matches-failure">
		<div class="alert gray">
			<span id="message-success">
				<h2>Geen directe match gevonden</h2>
				
				<p>We hebben geen reiziger met een abonnement gevonden die dezelfde reis aflegt. Probeer een ander traject of tijdstip.</p>
			</span>
		</div>
	</div><?php
}
else
{
	?><div class="col4 matches" id="matches-success">
		<div class="android-header mobile-only">Reis aanvragen</div>
		<div class="alert gray clickable-list">
			<span id="message-success">
				<h2 class="desktop-only">Match is gevonden!</h2>
				
				<p class="desktop-only">Er is een reiziger met een abonnement gevonden die deze reis ook aflegt. Wat zijn de volgende stappen?</p>

				<ol>
					<li><a name="laten-weten" class="nolink desktop-only">Laten weten dat jij mee wil reizen.</a>
						<small class="help desktop-only">Vul hieronder je contactgegevens in om de reis aan te vragen. We gaan zorgvuldig met je gegevens om.</small>

						<form action="<?php echo BASE_URL; ?>reis/aanvragen" method="post" id="reis-aanvragen">
							<input type="hidden" name="van" value="<?php echo urldecode($from);?>">
							<input type="hidden" name="naar" value="<?php echo urldecode($to);?>">
							<input type="hidden" name="datum" value="<?php echo $result['date'];?>">
							<input type="hidden" name="datumparsed" value="<?php echo date("Y-m-d\TH:i:s\&plus;O", strtotime($result['date'])); ?>">
							<input type="hidden" name="vertrektijd" value="<?php echo $result['vertrekTijd']; ?>">
							<input type="hidden" name="aankomsttijd" value="<?php echo $result['aankomstTijd']; ?>">
							<input type="hidden" name="reistijd" value="<?php echo $result['reisTijd']; ?>">
							<input type="hidden" name="overstappen" value="<?php echo $result['overstappen']; ?>">

							<label>
								<span class="desktop-only">Je e-mailadres</span>
								<input type="text" name="email" placeholder="E-mailadres" style="width: 100%;"></label>
							
							<label>
								<span class="desktop-only">Je telefoonnummer</span>
								<input type="text" name="telefoon" placeholder="Telefoonnummer" style="width: 100%;"></label>
							
							<button onclick="window.location.href='<?php echo BASE_URL; ?>docs'" class="gray mobile-only">Hoe werkt dit <i class="ion-help"></i>
							</button><button type="submit">Reis aanvragen <i class="icon-arrow-right desktop-only"></i><i class="ion-chevron-right mobile-only"></i></button>
						</form>
	
						
					</li>
					<li class="desktop-only">Wachten op goedkeuring van de reiziger.<br>
						<small class="help">Op deze manier is de reiziger ervan op de hoogte dat jij mee wil reizen.</small>
					</li>
					<li class="desktop-only">De reiziger betalen via ons systeem en zijn contactgegevens ontvangen.<br>
						<small class="help">Het geld is een kleine beloning voor de abonnementhouder en is zo'n 10% van de prijs van het treinkaartje.</small>
					</li>
				</ol>
							
			</span>
		</div>
	</div>
	<?php
}

include 'footer.php';
?>