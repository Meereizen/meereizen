<?php
if($mobile)
{
	?>
	<div class="android-header mobile-only">Kies loginwijze</div>

	<div class="clickable-list">
		<a href="<?php echo BASE_URL; ?>abonnementhouder/facebook" class="btn facebook">Inloggen met Facebook</a>
		<a href="<?php echo BASE_URL; ?>abonnementhouder/twitter" class="btn twitter">Inloggen met Twitter</a>
		<a href="<?php echo BASE_URL; ?>abonnementhouder/google" class="btn google">Inloggen met Google</a>
	</div>

	<div class="android-header mobile-only">Permissies</div>

	<div class="clickable-list">
		<p>Wij zullen u..</p>
		<ul style="color:green;">
			<li>om uw basis informatie vragen.</li>
			<li>om uw e-mailadres vragen.</li>
		</ul>
		
		<p>Wij zullen geen..</p>
		<ul style="color:red;">
			<li>updates kunnen plaatsen via uw account.</li>
			<li>wijzigingen aan uw profiel kunnen doorvoeren.</li>
			<li>informatie over uw contacten ontvangen.</li>
		</ul>
	</div>
	<?php
}
else
{
$title = 'Inloggen';
include('header.php');
?>

<div class="col6">
<h1 class="desktop-only">Inloggen</h1>
	
	<p class="desktop-only">Heeft u een treinabonnement? Dan kunt u geld verdienen! <a href="<?php echo BASE_URL;?>abonnementhouder/info">Hier kunt u daar meer over lezen.</a> Wat u hierna doet is u identificeren met &eacute;&eacute;n van uw social media accounts en ga verder.</p>
	
	<p class="desktop-only">Wanneer u klikt op &eacute;&eacute;n van de knoppen aan de rechterkant, zult u doorgestuurd worden naar het betreffende sociale netwerk. Daar vragen we u voor toestemming om uw basisgegevens te ontvangen. Dit is alles wat we zullen ontvangen. We zullen dus niet in staat zijn om uw account op welke manier dan ook te veranderen of om updates te plaatsen.</p>
	
	<div class="alert yellow big">
		Belangrijk: u hoeft niet te registreren, inloggen is genoeg!
	</div>

</div><div class="col4 inloggen">
	
	<legend>Inloggen met social media</legend>
	<br>
	
	<span>
		Wij zullen u..
		<ul style="color:green;">
			<li>om uw basis informatie vragen.</li>
			<li>om uw e-mailadres vragen.</li>
		</ul>
		
		Wij zullen geen..
		<ul style="color:red;">
			<li>updates kunnen plaatsen via uw account.</li>
			<li>wijzigingen aan uw profiel kunnen doorvoeren.</li>
			<li>informatie over uw contacten ontvangen.</li>
		</ul>
	</span>

	<a href="<?php echo BASE_URL; ?>abonnementhouder/facebook" class="btn facebook">Inloggen met Facebook</a><br>
	<a href="<?php echo BASE_URL; ?>abonnementhouder/twitter" class="btn twitter">Inloggen met Twitter</a><br>
	<a href="<?php echo BASE_URL; ?>abonnementhouder/google" class="btn google">Inloggen met Google</a><br>
</div>

<?php
include('footer.php');
}
?>