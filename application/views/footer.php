<?php
if(!isset($footerAlreadyRequested) || !$footerAlreadyRequested)
{
?>
			<div class="footer">
				<a href="#">Contactgegevens</a> | <a href="#">Over ons</a> | <a href="<?php echo BASE_URL;?>docs">Documentatie</a> | <a href="#">Algemene voorwaarden</a><br>
				&copy; <?php echo date('Y'); ?>, Meereizen.nu
			</div>
		</div>
	</div>

    <script src="<?php echo BASE_URL; ?>static/js/jquery.js"></script>
    <script src="<?php echo BASE_URL; ?>static/js/jquery-ui.js"></script>
    <script>var BASE_URL = '<?php echo 'http://'.$_SERVER['HTTP_HOST'] ?>';</script>
    <script src="<?php echo BASE_URL; ?>static/js/main.js"></script>    

	<script src="<?php echo BASE_URL; ?>static/js/fastclick.js" type="text/javascript"></script>

<?php
if(isset($planner) || isset($autocomplete))
{
	?>
	<script src="<?php echo BASE_URL; ?>static/js/swipe.js" type="text/javascript"></script>

	<script src="<?php echo BASE_URL; ?>static/js/mobiscroll/mobiscroll.core.js" type="text/javascript"></script>
	<script src="<?php echo BASE_URL; ?>static/js/mobiscroll/mobiscroll.datetime.js" type="text/javascript"></script>
	<script src="<?php echo BASE_URL; ?>static/js/mobiscroll/mobiscroll.meereizen.js" type="text/javascript"></script>
	<script src="<?php echo BASE_URL; ?>static/js/mobiscroll/i18n/mobiscroll.i18n.nl.js" type="text/javascript"></script>
		
	<link href="<?php echo BASE_URL; ?>static/css/mobiscroll/mobiscroll.core.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo BASE_URL; ?>static/css/mobiscroll/mobiscroll.meereizen.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo BASE_URL; ?>static/css/mobiscroll/mobiscroll.animation.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function() {
			// Alle stations in een array
			var Stations = [];
			// Haal stationslijst op
		    $.get('<?php echo BASE_URL; ?>ajax/stations', function(d){
		    	// Vind de Station tag
		    	$(d).find('Station').each(function(){
		    		// Voeg elk station toe aan de array
			        var $station = $(this);
			        var Naam = $station.find('Lang').text();  
			        Stations.push(Naam);
			    });
		    }); 
		        
		    if(window.innerWidth < 700){
			    $(".autocomplete-vertrek").each(function(){
					$(this).autocomplete({
						source: Stations,
						minLength: 2,
						select: function(event, ui) {
							$(this).val(ui.item.value);
						},
						appendTo: "#autocomplete-vertrek"
					});
				});
			    $(".autocomplete-aankomst").each(function(){
					$(this).autocomplete({
						source: Stations,
						minLength: 2,
						select: function(event, ui) {
							$(this).val(ui.item.value);
						},
						appendTo: "#autocomplete-aankomst"
					});
				});
		   	}else{
			    $(".autocomplete").each(function(){
					$(this).autocomplete({
						source: Stations,
						minLength: 2,
						select: function(event, ui) {
							$(this).val(ui.item.value);
						}
					});
				});
			}

			$('#datepicker, #datepicker-mobile').mobiscroll().datetime({
				dateFormat: 'dd-mm-yy',
				startYear: '2014',
				theme: 'meereizen',
				mode: 'mixed',
				lang: 'nl',
				display: 'modal',
				animate: 'pop',
				timeFormat: 'HH:ii',
				timeWheels: 'HHii'
			});

			var swiperPos = 1;

			var elem = document.getElementById('swiper');
			window.swiper = Swipe(elem, {
				startSlide: 1,
				continuous: false,
				speed: 200,
				disableScroll: false,
				callback: function(pos) {
					if(pos !==1){
						window.location.hash = pos;
					}else{
						window.location.hash = '';
					}
					swiperPos = pos;

					$('#bar button').each(function(i) {
						if(i == pos){
							$(this).addClass('active').find('i').removeClass($(this).find('i').data('original'));
							$(this).find('i').addClass($(this).find('i').data('bold'));
						}else{
							$(this).removeClass('active').find('i').addClass($(this).find('i').data('original'));
							$(this).find('i').removeClass($(this).find('i').data('bold'));
						}
					});
				}
			});

			var showSubmit = 0;
			$('.planner input').keyup(function(){
				if($(this).val() && showSubmit == 0) {
					$('.swiper-header .tabs-holder').animate({width: '80%'}, 100);
					$('.swiper-header #submitter').animate({right: '0',top: '0'}, 100, function(){
						showSubmit = 1;
					});
				}
			});

			var hash = window.location.href.split('#')[1];

			if(!isNaN(hash) && hash.length !== 0){
				swiper.slide(hash,0);				
			}else{
				alert
				var defaultPos = 1;

				$('#bar button').each(function(i) {
					if(i == defaultPos){
						$(this).addClass('active').find('i').removeClass($(this).find('i').data('original'));
						$(this).find('i').addClass($(this).find('i').data('bold'));
					}else{
						$(this).removeClass('active').find('i').addClass($(this).find('i').data('original'));
						$(this).find('i').removeClass($(this).find('i').data('bold'));
					}
				});
			}

			$('.swipe-main, .swipe-wrap, .swipe-wrap > div').hide(0);
		});
	</script>
	<?php
}
?>

</body>
</html>
<?php
}
?>