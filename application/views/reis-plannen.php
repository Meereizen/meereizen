<?php
/*
*
*	Deze view verwacht een aantal variabelen van de controller:
*	$from, $to, $date, $time, $results
*		$results is een array met alle reismogelijkheden en de bijbehorende gegevens
*
*/

$autocomplete = TRUE;

$title = $matches.' resultaten';
if($matches==1) $title = '1 resultaat';
include 'header.php';
?>

<div id="reismogelijkheden-holder">
	<?php
	if($matches>0)
	{
		?>
		<h1 class="nopadding nomargin center plannen-title desktop-only">
			Kies een reis
		</h1>
		<legend class="subtitle center desktop-only">Meerdere reizen voldoen aan de zoekopdracht.</legend>

		<div class="android-header mobile-only">Mogelijkheden</div>
		<?php
		foreach($results as $result)
		{
			?>
			<div class="col6 offset2 alert reismogelijkheid reismogelijkheid-collapsed" id="<?php echo $result['id']; ?>" data-date="<?php echo $result['date']; ?>" data-dateparsed="<?php echo date("Y-m-d\TH:i:s\&plus;O", strtotime($result['date'])); ?>">
				<div class="one-fifth field-vertrektijd mobile-three-eighth" data-value="<?php echo $result['vertrekTijd']; ?>">
					<span class="table-label">Vertrektijd</span>
					<span class="verylarge">
						<?php echo $result['vertrekTijd']; ?>
					</span>
				</div><div class="one-fifth field-aankomsttijd mobile-three-eighth" data-value="<?php echo $result['aankomstTijd']; ?>">
					<span class="table-label">Aankomsttijd</span>
					<span class="verylarge">
						<?php echo $result['aankomstTijd']; ?>
					</span>
				</div><div class="one-fifth field-reistijd desktop-only" data-value="<?php echo $result['reisTijd']; ?>">
					<span class="table-label">Reistijd</span>
					<span class="verylarge">
						<?php echo $result['reisTijd']; ?>
					</span>
				</div><div class="one-fifth field-overstappen desktop-only" data-value="<?php echo $result['overstappen']; ?>">
					<span class="table-label">Overstappen</span>
					<span class="verylarge ">
						<?php echo $result['overstappen']; ?>
					</span>
				</div><div class="one-fifth matches-count desktop-only">
					<span class="table-label">Matches</span>
					<span class="verylarge">
						<?php echo $result['matches']; ?>
					</span>
				</div><div class="mobile-only mobile-one-fourth right">
							
					<a class="btn small right kiesmogelijkheid" href="<?php echo BASE_URL; ?>reis/details/<?php echo $from; ?>/<?php echo $to; ?>/<?php echo date("Y-m-d", strtotime($result['date'])); ?>/<?php echo date("H:i", strtotime($result['vertrekTijd'])); ?>/<?php echo date("H:i", strtotime($result['aankomstTijd'])); ?>">
						<i class="icon-arrow-right"></i>
					</a>
				</div>
				
				<div class="mobile-only small center reismogelijkheid-detail"><?php echo $result['overstappen']; ?> overstap(pen) en <?php echo $result['matches']; ?> match(es)</div>

				<a href="<?php echo BASE_URL; ?>reis/details/<?php echo $from; ?>/<?php echo $to; ?>/<?php echo date("Y-m-d", strtotime($result['date'])); ?>/<?php echo date("H:i", strtotime($result['vertrekTijd'])); ?>/<?php echo date("H:i", strtotime($result['aankomstTijd'])); ?>" class="btn small right kiesmogelijkheid desktop-only" data-id="<?php echo $result['id']; ?>">
					<i class="icon-arrow-right"></i> Kies deze reis
				</a>

				<span class="desktop-only">
					<a class="btn white small meerinfo"><i class="icon-arrow-down"></i> Uitklappen</a>
				</span>
				
				<span class="info">
					<?php
					$i = 0;
					// Voor elk reisdeel..
					foreach($result['reisDelen'] as $reisDeel)
					{
						// Een lijst maken (= lijn met puntjes na CSS)
						echo '<ul>';
		
						// Voor elk station..
						foreach($reisDeel as $station)
						{
							// ..een puntje maken met tijd, naam en evt. spoor
							echo '<li>'.$station['tijd'].'&nbsp;-&nbsp;'.$station['naam'];
							if(isset($station['spoor']))
							{
								echo '<span class="right">spoor '.$station['spoor'].'</span>';
							}
							echo '</li>';						
						}
						echo '</ul>';
						$i++;
						
						// Als het aantal reisdelen nog niet afgegaan is maar deze wel ten einde is, betekent dat een overstap
						if($i < $result['aantalReisDelen'])
						{
							echo '<ul><li class="overstap">&nbsp;</li></ul>';
						}
					}
					?>
				</span>
			</div>
			<?php
		}
	}
	else
	{
		?>
		<h1 class="nopadding nomargin center">
			<i class="ion-ios7-close-outline huge-icon"></i>
			Niks gevonden
		</h1>

		<div class="center">
			<div class="alert nopadding col6">
				<p>Er zijn geen abonnementhouders die dit traject afleggen. Probeer een ander vertrekstation.</p>
			</div>
		</div>
		<?php
	}
	?>
</div>

<?php
include 'footer.php';
?>