<?php
$title = 'Docs';
include 'header.php';
?>

<div class="sidebar">

	<h2>Hoe werkt Meereizen</h2>
	<ul class="vertical navigation">
		<li><a href="<?php echo BASE_URL;?>docs/page/algemeen/introductie">Introductie</a></li>
		<li><a href="<?php echo BASE_URL;?>docs/page/algemeen/doelgroepen">Doelgroepen</a></li>
	</ul>

	<h2>Beginnen met Meereizen</h2>
	<ul class="vertical navigation">
		<li><a href="<?php echo BASE_URL;?>docs/page/beginnen/reisplanner">Reisplanner gebruiken</a></li>
		<li><a href="<?php echo BASE_URL;?>docs/page/beginnen/match">Match vinden</a></li>
		<li><a href="<?php echo BASE_URL;?>docs/page/beginnen/reizen">Reizen met korting</a></li>
	</ul>

	<h2>Voor abonnementhouders</h2>
	<ul class="vertical navigation">
		<li><a href="<?php echo BASE_URL;?>docs/page/abonnementhouder/aanmelden">Aanmelden</a></li>
		<li><a href="<?php echo BASE_URL;?>docs/page/abonnementhouder/trajecten">Trajecten opgeven</a></li>
		<li><a href="<?php echo BASE_URL;?>docs/page/abonnementhouder/geld">Geld ontvangen</a></li>
	</ul>

</div><div class="content">
	<h1><?php echo $data['title']; ?></h1>

	<?php echo $data['body']; ?>
</div>

<?php
include 'footer.php';
?>