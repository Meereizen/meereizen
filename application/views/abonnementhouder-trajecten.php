<h1 class="desktop-only">Mijn trajecten</h1>

<p class="desktop-only">Hier staan de trajecten die u aflegt met uw abonnement. Op basis van deze trajecten zullen reizigers u kunnen vinden en u betalen om mee te reizen.</p>

<legend class="desktop-only">Mijn trajecten</legend>
<div class="mobile-only android-header">Mijn trajecten</div>

<?php
if(empty($data['trajecten']))
{
	?>
	<div class="alert yellow">
		Momenteel heeft u nog geen trajecten opgegeven.
	</div>
	<?php
}
else
{
	?>
	<div class="clickable-list">
		<div class="alert blue mobile-only">Klik op een traject om deze te verwijderen.</div>
	<?php
	foreach($data['trajecten'] as $traject)
	{
		?>
			<a class="traject-overview mobile-only link-to-confirm" data-message="Weet je zeker dat je dit traject wil verwijderen?" data-form="traject-<?php echo $traject['id']; ?>" href="http://www.google.com">
				<span class="station">
					<?php echo $traject['van']; ?>
				</span><span class="divider"><i class="icon-angle-right"></i></span><span class="station">
					<?php echo $traject['naar']; ?>
				</span>
				<span class="right"><?php
					$dagen = Array();
		
					// Checken of de dag TRUE of FALSE is en in array zetten als hij TRUE is.
					if(!$traject['maandag']==0) $dagen[] = 'Maandag';
					if(!$traject['dinsdag']==0) $dagen[] = 'Dinsdag';
					if(!$traject['woensdag']==0) $dagen[] = 'Woensdag';
					if(!$traject['donderdag']==0) $dagen[] = 'Donderdag';
					if(!$traject['vrijdag']==0) $dagen[] = 'Vrijdag';
					if(!$traject['zaterdag']==0) $dagen[] = 'Zaterdag';
					if(!$traject['zondag']==0) $dagen[] = 'Zondag';
						
					// Array imploden met comma-spatie als glue en echoen.
					echo implode(', ', $dagen);
					?></span>
				<span class="half date"><i class="desktop-only"><?php echo $weekday; ?></i><span><?php echo date('H:i', strtotime($traject['vertrektijd'])); ?></span></span>
			</a>
			<div class="alert gray reismogelijkheid traject desktop-only">
			    <div class="two-fifth">
				    <span class="table-label">Vertrekstation</span>
					<span class="large">
						<?php echo $traject['van'];?>
					</span>
				</div><div class="two-fifth">
					 <span class="table-label">Aankomststation</span>
					<span class="large">
						<?php echo $traject['naar'];?>
					</span>
				</div><div class="one-fifth desktop-only">
				     <span class="table-label">Vertrektijd</span>
					<span class="verylarge">
						<?php echo date("H:i", strtotime($traject['vertrektijd']));?>
					</span>
				</div>
				
				<form method="post" id="traject-<?php echo $traject['id']; ?>">				
					<input type="hidden" name="van" value="<?php echo $traject['van'];?>">
					<input type="hidden" name="naar" value="<?php echo $traject['naar'];?>">
					<input type="hidden" name="tijd" value="<?php echo $traject['vertrektijd'];?>">
					
					<input type="hidden" name="delete" value="true">				
					
				<div class="right traject-dagen">
					<span class="mobile-only">Om <b><?php echo date("H:i", strtotime($traject['vertrektijd']));?></b> op </span>
					<?php
					$dagen = Array();
	
					// Checken of de dag TRUE of FALSE is en in array zetten als hij TRUE is.
					if(!$traject['maandag']==0) $dagen[] = 'Maandag';
					if(!$traject['dinsdag']==0) $dagen[] = 'Dinsdag';
					if(!$traject['woensdag']==0) $dagen[] = 'Woensdag';
					if(!$traject['donderdag']==0) $dagen[] = 'Donderdag';
					if(!$traject['vrijdag']==0) $dagen[] = 'Vrijdag';
					if(!$traject['zaterdag']==0) $dagen[] = 'Zaterdag';
					if(!$traject['zondag']==0) $dagen[] = 'Zondag';
					
					// Array imploden met comma-spatie als glue en echoen.
					echo implode(', ', $dagen);
					?>
				</div>
				
					<button type="submit" class="btn small red">
						<i class="icon-trash"></i> Verwijder
					</button>
	
					
				</form>
			</div>
		<?php
	}
	?>
	</div>
	<?php
}
?>
<form class="full" method="post" action="">

	<legend class="desktop-only">Een nieuw traject toevoegen</legend>
	<div class="mobile-only android-header">Nieuw traject toevoegen</div>
	
	<?php
	if(isset($data['error']))
	{
		echo '<div class="alert red">'.$data['error'].'</div>';
	}
	if(isset($data['success']))
	{
		echo '<div class="alert green">'.$data['success'].'</div>';
	}
	?>

	<p class="two-fifth field-with-icon-container">
		<label for="van" class="desktop-only">
			Vertrekstation
		</label>
		<i class="ion-ios7-upload-outline mobile-only"></i>
		<input type="text" id="van" name="van" class="autocomplete clean clean-with-icon" placeholder="Vertrekstation">
	</p><p class="two-fifth field-with-icon-container">
		<label for="naar" class="desktop-only">
			Aankomststation
		</label>
		<i class="ion-ios7-download-outline mobile-only"></i>
		<input type="text" id="naar" name="naar" class="autocomplete clean clean-with-icon" placeholder="Aankomststation">
	</p><p class="one-fifth field-with-icon-container">
	    <label for="tijd" class="desktop-only">
		    Vertrektijd
		</label>
		<i class="ion-ios7-clock-outline mobile-only"></i>
		<input type="text" id="tijd" name="tijd" class="clean clean-with-icon" placeholder="Vertrektijd">		
	</p>
	<div class="clickable-list">
		<p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[1]" value="1" checked>
					<span></span>
					Maandag
				</label>
			</span>
		</p><p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[2]" value="1">
					<span></span>
					Dinsdag
				</label>
			</span>
		</p><p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[3]" value="1">
					<span></span>
					Woensdag
				</label>
			</span>
		</p><p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[4]" value="1">
					<span></span>
					Donderdag
				</label>
			</span>
		</p><p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[5]" value="1">
					<span></span>
					Vrijdag
				</label>
			</span>
		</p><p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[6]" value="1">
					<span></span>
					Zaterdag
				</label>
			</span>
		</p><p class="one-seventh">
			<span class="custom-checkbox">
				<label>
					<input type="checkbox" name="dag[7]" value="1">
					<span></span>
					Zondag
				</label>
			</span>
		</p>
	</div>

	<input type="submit" value="Voeg traject toe">
</form>