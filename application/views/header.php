<?php
$loggedIn = FALSE;
if(isset($_SESSION['mr_logged']) && isset($_SESSION['mr_user_agent'])) $loggedIn = TRUE;
if(isset($_COOKIE['remember']) && $loggedIn == FALSE) {
	header('Location:'.BASE_URL.'abonnementhouder/rememberme');
}

$docs = '';
if(isset($title) && $title=='Docs')
{
	$docs = ' <i style="font-weight:lighter;font-style:normal;">docs</i>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    
    <title>Meereizen.nu</title>
    <meta name="description" content="">
    <meta name="author" content="Meereizen">
    
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
       
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:600,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/mobile.css" type="text/css" media="all and (max-width: 699px)" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/main.css" type="text/css" media="all and (min-width: 700px)" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/responsive.css" type="text/css" media="all and (max-width: 1049px) and (min-width: 700px)" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/font-awesome.min.css" type="text/css" media="all" />
    
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL; ?>static/css/ionicons.css">
    
    <!-- Webapp icons -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASE_URL; ?>static/images/apple/icons/57x57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASE_URL; ?>static/images/apple/icons/72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASE_URL; ?>static/images/apple/icons/144x144.png">
</head>
<body class="<?php if(isset($planner)){ echo 'body-planner'; } ?><?php if(!empty($docs)){ echo 'body-docs'; } ?>">

	<div class="promotional-popup desktop-only">
		<div class="close"><i class="ion-ios7-close-empty"></i></div>
		<div class="inner">

			<div class="promotional-abonnementhouders">
				<img src="<?php echo BASE_URL;?>static/images/Single_Tap.png">
				<h2>Verdien geld met je treinabonnement</h2>
				<p>Schrijf je in en verdien geld door reizigers mee te laten reizen.op je abonnement.</p>
			</div>
			<div class="promotional-reisplanner">
				<img src="<?php echo BASE_URL;?>static/images/Single_Tap.png">
				<h2>Vind abonnementhouders</h2>
				<p>Gebruik onze reisplanner, vind abonnementhouders en reis met korting.</p>
			</div>
		</div>
	</div>

	<div class="mobile-bottom-loader mobile-only">
		Laden..
	</div>

	<div class="mobile-bottom-error mobile-only">
		Verbinding verbroken. <a onclick="location.reload();">Herladen</a>.
	</div>

	<div class="header<?php if(!isset($planner)){ echo ' header-small'; }else{ echo ' desktop-only';} ?>">
		<div class="header-background-image">
			<div class="wrap">
				<nav>
					<a class="form-submitter left btn mobile-only"<?php if(isset($terugButton)) { ?> href="<?php echo $terugButton; ?>"<?php }else{ ?> data-onclick="window.history.back()"<?php } ?>><i class="ion-chevron-left"></i> Terug</a>

					<span class="title mobile-only"><?php if(isset($title)){ echo $title; }else{ echo 'Meereizen'; } ?></span>
					<a href="<?php echo BASE_URL; ?>" class="logo"><img src="<?php echo BASE_URL; ?>static/images/logo-light.png" alt="Meereizen.nl" align="top"> <span>Meereizen<?php echo $docs; ?></span></a>
						
					<?php
					// Checken of er ingelogd is
					if($loggedIn)
					{
						$name = 'Dashboard';
						if(isset($_SESSION['mr_name']))
						{
							$name = $_SESSION['mr_name'];
						}
						?>
						<a href="<?php echo BASE_URL; ?>abonnementhouder/" class="right btn"><i class="icon-user"></i> <?php echo $name; ?></a>
						<?php
					}
					else
					{
						?>
						<a href="<?php echo BASE_URL; ?>abonnementhouder" class="right btn"><i class="icon-unlock"></i> Inloggen</a>
						<?php
					}
					?>
					<a href="<?php echo BASE_URL; ?>abonnementhouder/info" class="right btn transparent">Voor abonnementhouders</a>
					<?php
					if(isset($planner))
					{
						?>
						<span class="desktop-only slogan">
							Krijg 25% korting op je treinreis!
						</span>
						<?php
					}

					if(isset($formSubmitter))
					{
						?>
						<a data-form="<?php echo $formSubmitter['form']; ?>" class="form-submitter right btn mobile-only">
							<?php
							echo $formSubmitter['title'];
							?> <i class="ion-chevron-right"></i>
						</a>
						<?php
					}
					?>
				</nav>
			</div>
		</div>
	</div>
<?php
if(isset($planner))
{
	?>
	<div class="swiper-header mobile-only">
		<div id="bar">
			<div class="tabs-holder">
				<button onclick="swiper.slide(0,250)"><i class="ion-ios7-lightbulb-outline" data-original="ion-ios7-lightbulb-outline" data-bold="ion-ios7-lightbulb"></i>
				</button><button onclick="swiper.slide(1,250)"><i class="ion-ios7-search" data-original="ion-ios7-search" data-bold="ion-ios7-search-strong"></i>
				</button><button onclick="swiper.slide(2,250)"><?php
				if($loggedIn){
					?><i class="ion-ios7-person-outline" data-original="ion-ios7-person-outline" data-bold="ion-ios7-person"></i><?php
				}else{
					?><i class="ion-ios7-locked-outline" data-original="ion-ios7-locked-outline" data-bold="ion-ios7-locked"></i><?php
				}
				?></button></div><button data-form=".planner #planner-form" class="form-submitter" id="submitter"><i class="ion-ios7-checkmark-empty" style="font-size:1.6em;"></i></button>
		</div>
	</div>
	<?php
}
?>
	<div id="page">
		<?php
		if(isset($planner)){
			?>
			<div class="planner desktop-only">
				<form class="horizontal" id="planner-form" action="<?php echo BASE_URL; ?>reis/getPlanner" method="post">
					<p class="five-sixteenth" id="field-van">
						<input id="van" class="autocomplete" type="text" placeholder="Vertrekstation" name="van" autocapitalize="on" required>
					</p><p class="five-sixteenth" id="field-naar">
						<input id="naar" class="autocomplete not-required" type="text" placeholder="Aankomststation" name="naar" autocapitalize="on" value="">
					</p><p class="fourth" id="field-datum">
						<input type="text" name="tijd" class="not-required" id="datepicker" placeholder="Vertrektijd" value="">
					</p><p class="eighth">
						<input type="submit" value="Zoek">
					</p>
				</form>
			</div>

			<div id="swiper" class="swipe-main mobile-only">
			  <div class='swipe-wrap'>
			    <div>
					<div class="android-header mobile-only">Vandaag vertrekkend</div>
					<div class="ajax-container clickable-list" data-type="uitgelicht-vandaag">
						<br><img src="<?php echo BASE_URL; ?>static/images/spinner.gif" width="16" height="16" class="center ajax-loader-animation">
					</div>
			    </div>
			    <div>
			    	<div class="planner">
						<form class="horizontal" id="planner-form" action="http://oli4jansen.nl:81/reis/getPlanner" method="post">
							<span class="android-header mobile-only">Vereiste velden</span>
							<p class="five-sixteenth" id="field-van">
								<i class="ion-ios7-upload-outline mobile-only"></i>
								<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input id="van" class="autocomplete-vertrek placeholder ui-autocomplete-input" type="text" placeholder="Vertrekstation" name="van" autocapitalize="on" required="" autocomplete="off" data-index="1">
								<div id="autocomplete-vertrek"></div>
							</p><span class="android-header mobile-only">Optionele velden</span><p class="five-sixteenth" id="field-naar">
								<i class="ion-ios7-download-outline mobile-only"></i>
								<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input id="naar" class="autocomplete-aankomst placeholder ui-autocomplete-input" type="text" placeholder="Aankomststation" name="naar" autocapitalize="on" autocomplete="off" data-index="2">
								<div id="autocomplete-aankomst"></div>
							</p><p class="fourth" id="field-datum">
								<i class="ion-ios7-clock-outline mobile-only"></i>
								<input type="text" name="tijd" id="datepicker-mobile" placeholder="Vertrektijd" autocomplete="off" class="placeholder" readonly="" data-index="3">
							</p>
						</form>
					</div>
			    </div>
				<?php
				if($loggedIn){
					?>
					<div>
						<ul class="tile-navigation">
							<li><a href="<?php echo BASE_URL; ?>abonnementhouder/"><i class="ion-ios7-download-outline mobile-only"></i><span>Openstaande reizen <div class="ajax-container" data-type="openstaande-reizen-count"></div></span></a>
							</li><li><a href="<?php echo BASE_URL; ?>abonnementhouder/geschiedenis"><i class="ion-ios7-clock-outline mobile-only"></i><span>Mijn geschiedenis</span></a>
							</li><li><a href="<?php echo BASE_URL; ?>abonnementhouder/trajecten"><i class="ion-ios7-paperplane-outline mobile-only"></i><span>Mijn trajecten</span></a>
							</li><li><a href="<?php echo BASE_URL; ?>abonnementhouder/instellingen"><i class="ion-ios7-gear-outline mobile-only"></i><span>Instellingen</span></a>
							</li><li><a href="<?php echo BASE_URL; ?>abonnementhouder/loguit"><i class="ion-ios7-locked-outline mobile-only"></i><span>Uitloggen</span></a></li>
						</ul>
					</div>
					<?php
				}else{
					?>
					<div class="ajax-container" data-type="mobile-login"><br><br><img src="<?php echo BASE_URL; ?>static/images/spinner.gif" width="16" height="16" class="ajax-loader-animation center"></div>
					<?php
				}
				?>
			  </div>
			</div>

			<?php
		}
		?>
		
		<div class="wrap" id="content">