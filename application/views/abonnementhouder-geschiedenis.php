<h1 class="desktop-only">Geschiedenis</h1>

<p>Hieronder staan al uw afgeronde Meereizen. <i>Een Meereis is 'afgerond' als uw de verificatiecode van de reiziger heeft ingevoerd.</i></p>
	
<?php
if(empty($data['meereizen']))
{
	?>
	<div class="alert yellow">
		Momenteel heeft u nog geen Meereizen afgerond.
	</div>
	<?php
}
foreach($data['meereizen'] as $meereis)
{
	?>
	<div class="alert gray">
	    <div class="one-half">
		    Vertrekstation<br>
			<span class="large">
				<?php echo $meereis['van'];?>
			</span>
		</div><div class="one-half">
			Aankomststation<br>
			<span class="large">
				<?php echo $meereis['naar'];?>
			</span>
		</div>
		<br><br>
		<div class="one-half">
		    Vertrekdatum en -tijd<br>
			<span class="verylarge">
				<?php echo date("d-m-Y H:i", strtotime($meereis['vertrektijd']));?>
			</span>
		</div><div class="one-half">
		    Wat u heeft verdient<br>
			<span class="verylarge">
				&euro; <?php echo $meereis['vol_tarief']*MEEREIZEN_VOOR_ABONNEMENTHOUDER;?>
			</span>
		</div>
	</div>
	<?php
}
?>
