<?php
$matches = 0;
foreach($results as $result) $matches = $matches + count($result);

$title = $matches.' matches gevonden';
$title = 'Vertrektijd';
include 'header.php';

if($matches>0)
{
	?>
	<h1 class="nopadding nomargin center plannen-title desktop-only">
		<span class="mobile-three-seventh table-cell" id="from" data-station="<?php echo $from; ?>">
			<?php echo urldecode($from);?>
		</span><span class="mobile-one-seventh mobile-icon-middle">
			&nbsp;<i class="icon-angle-right"></i>&nbsp;
		</span><span class="mobile-three-seventh table-cell" id="to" data-station="<?php echo $to; ?>">
			<?php echo urldecode($to);?>
		</span>
	</h1>
	<legend class="subtitle center  desktop-only"><?php 
		if($matches==1)
		{
			echo 'Er is 1 match gevonden.';
		}
		else
		{
			echo '<span class="desktop-only">Er zijn '.$matches.' matches gevonden. </span>Kies een vertrektijd om verder te gaan.';
		}
	?></legend>
	<?php
	foreach($results as $dag => $result)
	{
		$countResult = count($result);
		?><div class="one-seventh"><div class="android-header">
			<?php echo $dag; ?>
		</div><div class="clickable-list" id="<?php echo $dag; ?>" data-count="<?php echo count($result); ?>">
			<?php
			if(count($result)!==0)
			{
				$days = Array(
					'maandag' => 'monday',
					'dinsdag' => 'tuesday',
					'woensdag' => 'wednesday',
					'donderdag' => 'thursday',
					'vrijdag' => 'friday',
					'zaterdag' => 'saturday',
					'zondag' => 'sunday'
				);
				foreach($result as $match)
				{
					?><a href="<?php echo BASE_URL; ?>reis/details/<?php echo $match['van']; ?>/<?php echo $match['naar']; ?>/<?php echo date('Y-m-d', strtotime('next '.$days[$dag])); ?>/<?php echo $match['vertrektijd']; ?>" class="list-item">
						<?php echo date("H:i", strtotime($match['vertrektijd'])); ?> uur &nbsp;&nbsp;<i class="icon-angle-right desktop-only"></i><span class="mobile-only btn"><i class="ion-ios7-arrow-right"></i></span>
					</a><?php
				}
			}
			?>
		</div></div><?php
	}
}
else
{
?>
	<h1 class="nopadding nomargin center">
		<i class="ion-ios7-close-outline huge-icon"></i>
		Niks gevonden
	</h1>

	<div class="center">
		<div class="alert nopadding col6">
			<p>Er zijn geen abonnementhouders die dit traject (<?php echo urldecode($from);?> naar <?php echo urldecode($to);?>) afleggen.</p>
		</div>
	</div>
<?php
}
include 'footer.php';
?>