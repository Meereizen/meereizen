<?php
$menu = TRUE;

$title = 'Openstaande reizen';
if(isset($page) && $page=='trajecten')
{
	$title = 'Mijn trajecten';
}
elseif(isset($page) && $page=='geschiedenis')
{
	$title = 'Geschiedenis';
}
elseif(isset($page) && $page=='instellingen')
{
	$title = 'Instellingen';
	$formSubmitter['title'] = 'Opslaan';
	$formSubmitter['form'] = '#form-instellingen';
}

$terugButton = BASE_URL.'#2';

include('header.php');
?>

<div class="col3 alert gray sidemenu desktop-only">
	<ul class="vertical navigation">
		<li><a href="<?php echo BASE_URL; ?>abonnementhouder/"><i class="ion-ios7-download-outline mobile-only">&nbsp;</i><i class="icon-inbox desktop-only">&nbsp;</i> Openstaande reizen</a></li>
		<li><a href="<?php echo BASE_URL; ?>abonnementhouder/geschiedenis"><i class="ion-ios7-clock-outline mobile-only">&nbsp;</i><i class="icon-location-arrow desktop-only">&nbsp;</i> Meereis-geschiedenis</a></li>
		<li><a href="<?php echo BASE_URL; ?>abonnementhouder/trajecten"><i class="ion-ios7-paperplane-outline mobile-only">&nbsp;</i><i class="icon-ticket desktop-only">&nbsp;</i> Mijn trajecten</a></li>
		<li><a href="<?php echo BASE_URL; ?>abonnementhouder/instellingen"><i class="ion-ios7-gear-outline mobile-only">&nbsp;</i><i class="icon-gear desktop-only">&nbsp;</i> Instellingen</a></li>
		<li><a href="<?php echo BASE_URL; ?>abonnementhouder/loguit"><i class="ion-ios7-locked-outline mobile-only">&nbsp;</i><i class="icon-off desktop-only">&nbsp;</i> Uitloggen</a></li>
	</ul>
</div><div class="col7">
	<?php
	if(!isset($page) || empty($page))
	{
	?>
		<h1 class="desktop-only">Openstaande reizen</h1>
		
		<p class="desktop-only">U staat bij ons geregistreerd als abonnementhouder. Dit betekend dat we contact met u op kunnen nemen als iemand mee wil reizen met u, op &eacute;&eacute;n van de door u opgegeven trajecten.</p>
		
		
		<?php
		
		if(empty($data['meereizen']))
		{
			?>
			<div class="alert green big">Als iemand met u mee wil reizen, verschijnt dat hieronder. Aan u de taak om aan te geven of u dit goed vindt.</div>
			<?php
		}
		
		echo '<div class="clickable-list">';
		foreach($data['meereizen'] as $meereis)
		{

			// Als de Meereis nog niet gekoppeld is aan 1 v/d matches.
			if($meereis['gebruiker']==0 || empty($meereis['gebruiker']))
			{
				$status = '
				<form action="'.BASE_URL.'abonnementhouder/meereis" method="post">
					<input type="hidden" name="id" value="'.$meereis['id'].'">
					<button type="submit" name="state" value="1" class="green right">Accepteren</button>
					<button type="submit" name="state" value="0" class="red">Weigeren</button>
				</form>';
			}
			// Als de Meereis gekoppeld is aan huidige gebruiker, betaald is en de verificatie gateway geopend is.
			elseif($meereis['gebruiker']==$user_id && $meereis['betaald']==1)
			{
				$status = '
				<form class="alert green code-form" method="post" action="'.BASE_URL.'abonnementhouder/verificatie">
					<input type="hidden" name="meereis" value="'.$meereis['id'].'">
					<p>Wanneer u de verificatie code invoert, ontvangt u direct het geld op uw Paypal rekening.</p>
					<input type="text" name="code" class="code-field two-third" placeholder="De verificatiecode"><input type="submit" value="Geld opvragen" class="one-third">
				</form>';
			}
			// Als de Meereis gekoppeld is aan huidige gebruiker, maar er nog niet betaald is.
			elseif($meereis['gebruiker']==$user_id && $meereis['betaald']==0)
			{
				$status = '
				<div class="alert green">
					U heeft deze reiziger geaccepteerd. Zodra de reiziger u heeft betaald, ontvangt u diens contactgegevens.
				</div>';
			}
			// Als de Meereis niet gekoppeld is aan de huidige gebruiker.
			else
			{
				$status = '
				<div class="alert red">
					Iemand anders was u voor. De reiziger is inmiddels al gekoppeld aan een andere abonnementhouder.
				</div>';
			}
			?>

			<a class="traject-overview mobile-only">
				<span>
					<span class="station"><?php echo $meereis['van'];?></span><span class="divider"><i class="icon-angle-right"></i></span><span class="station"><?php echo $meereis['naar'];?></span>
				</span>
				<span class="mobile-only right">u verdient &euro; <?php echo $meereis['vol_tarief']*MEEREIZEN_VOOR_ABONNEMENTHOUDER;?></span>
				<span class="half date"><span><?php echo date("d-m-Y H:i", strtotime($meereis['vertrektijd']));?></span></span>

				<br><br>
				<?php echo $status; ?>
			</a>

			<div class="alert gray desktop-only">
			    <div class="one-half">
				    Vertrekstation<br>
					<span class="large">
						<?php echo $meereis['van'];?>
					</span>
				</div><div class="one-half">
					Aankomststation<br>
					<span class="large">
						<?php echo $meereis['naar'];?>
					</span>
				</div>
				<br><br>
				<div class="one-half">
				    Vertrekdatum en -tijd<br>
					<span class="verylarge">
						<?php echo date("d-m-Y H:i", strtotime($meereis['vertrektijd']));?>
					</span>
				</div><div class="one-half">
				    Wat u verdient<br>
					<span class="verylarge">
						&euro; <?php echo $meereis['vol_tarief']*MEEREIZEN_VOOR_ABONNEMENTHOUDER;?>
					</span>
				</div>
				<br><br>
				<?php echo $status; ?>
			</div>
			<?php
		}
		echo '</div>';
	}
	elseif(isset($page) && $page=='trajecten')
	{
		require('abonnementhouder-trajecten.php');
	}
	elseif(isset($page) && $page=='geschiedenis')
	{
		require('abonnementhouder-geschiedenis.php');
	}
	elseif(isset($page) && $page=='instellingen')
	{
		require('abonnementhouder-instellingen.php');
	}
	?>
</div>


<?php include('footer.php'); ?>