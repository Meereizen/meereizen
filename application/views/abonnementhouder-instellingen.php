<h1 class="desktop-only">Instellingen</h1>
		
<form id="form-instellingen" class="full" method="post" action="<?php echo BASE_URL;?>abonnementhouder/instellingen">

	<?php
	if(isset($data['error']))
	{
		echo '<div class="alert red">'.$data['error'].'</div>';
	}
	if(isset($data['success']))
	{
		echo '<div class="alert green">'.$data['success'].'</div>';
	}
	?>

<legend class="desktop-only">Contactgegevens</legend>
<span class="android-header mobile-only">Contactgegevens</span>

    <p class="field-with-icon-container">
	    <label for="email" class="desktop-only">
		    Uw e-mailadres
		</label>
		<i class="ion-ios7-email-outline mobile-only"></i>
		<input type="text" id="email" class="clean clean-with-icon" name="email" placeholder="E-mailadres" value="<?php echo $data['gebruiker']['email']; ?>">
	</p>
                	
    <p class="field-with-icon-container">
    	<label for="telefoon" class="desktop-only">
        	Uw mobiele telefoonnummer
        </label>
        <i class="ion-ios7-telephone-outline mobile-only"></i>
        <input type="text" id="telefoon" class="clean clean-with-icon" name="telefoon" placeholder="Telefoonnummer" value="<?php echo $data['gebruiker']['telefoon']; ?>">
    </p>

<legend class="desktop-only">Rekening informatie</legend>
<span class="android-header mobile-only">Paypal rekening</span>

    <p class="field-with-icon-container">
	    <label for="paypal" class="desktop-only">
	    	Uw Paypal rekeningadres (e-mail)
	    </label>
	    <i class="ion-ios7-pricetag-outline mobile-only"></i>
	    <input type="text" id="paypal" class="clean clean-with-icon" name="paypal" placeholder="Paypal rekening (email)" value="<?php echo $data['gebruiker']['paypal']; ?>">
    </p>
	             
	<input type="hidden" name="update" value="TRUE">   
    <input type="submit" value="Opslaan" class="desktop-only">
</form>