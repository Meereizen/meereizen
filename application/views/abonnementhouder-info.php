<?php
$title = 'Voor abonnementhouders';
include('header.php');
?>

<h1>Voor abonnementhouders</h1>

<p>Ook u als abonnementhouder kunt goedkoper met de trein. Wanneer u met Meereizen reist kunt u per keer dat u meereizen gebruikt tot  30% van de kosten van de treinreis terugverdienen.</p>

<p>Drie makkelijke stappen om geld terug te verdienen:</p>

<h3>1. Login met Facebook, Twitter of Google.</h3>
<h3>2. Voer uw eventuele vaste traject in. </h3>
<h3>3. Wacht op een medereiziger en ontvang geld.</h3>

<?php
include('footer.php');
?>