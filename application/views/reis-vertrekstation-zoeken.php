<?php
$matches = 0;
foreach($results as $result) $matches = $matches + count($result);

$title = 'Eindbestemming';
if($matches<1) $title = 'Niks gevonden';
include 'header.php';

if($matches>0)
{
	?>
	<h1 class="nopadding nomargin center plannen-title desktop-only">
		Kies een eindbestemming
	</h1>
	<legend class="subtitle center desktop-only">Ik wil reizen van <?php echo urldecode($from); ?> naar ...</legend>

	<p class="center mobile-only">Kies een eindbestemming om meer informatie te zien over de reismogelijkheden die er zijn op dat traject.</p>

	<span class="android-header mobile-only">Mogelijke eindbestemmingen</span>
	<div class="center clickable-list eindbestemmingen">
		<?php
		foreach($results as $result)
		{
			?><a href="<?php echo BASE_URL; ?>reis/zoeken/<?php echo $from; ?>/<?php echo urlencode($result['naar']); ?>">
				<?php echo $result['naar']; ?> <span class="btn"><i class="ion-ios7-arrow-right"></i></span>
			</a><?php
		}
		?>
	</div>
	<?php
}
else
{
?>
	<h1 class="nopadding nomargin center">
		<i class="ion-ios7-close-outline huge-icon"></i>
		Niks gevonden
	</h1>

	<div class="center">
		<div class="alert nopadding col6">
			<p>Er zijn geen abonnementhouders die dit traject afleggen. Probeer een ander vertrekstation.</p>
		</div>
	</div>
<?php
}
include 'footer.php';
?>