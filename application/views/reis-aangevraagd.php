<?php
$title = 'Reis aangevraagd';
include 'header.php';
?>

<div class="col6">
<h1 class="desktop-only">De reis is aangevraagd</h1>
	<br class="desktop-only">

	<div class="alert green big">De reis is succesvol aangevraagd! <b>Houd je inbox in de gaten!</b> Je ontvangt zo snel mogelijk een e-mail met de reactie van de abonnementhouder.</div>

	<p class="desktop-only">We hebben de abonnementhouder laten weten dat je mee wil reizen. Hij of zij zal zo snel mogelijk laten weten of hij de reis ook echt aflegt en of jij mee mag reizen. Zie het stappenplan hiernaast wat de volgende stappen zullen zijn.</p>
	
	<div class="button-holder desktop-only">
		<a href="http://www.gmail.com/" class="btn google"><i class="icon-external-link">&nbsp;</i> Gmail.com</a>
		<a href="http://www.outlook.com/" class="btn outlook"><i class="icon-external-link">&nbsp;</i> Outlook.com</a>
		<a href="http://mail.yahoo.com/" class="btn yahoo"><i class="icon-external-link">&nbsp;</i> Yahoo! Mail</a>
	</div>
	
</div><div class="android-header mobile-only">Stappenplan</div><div class="col4 alert inloggen desktop-only">
	<h2 class="desktop-only">Bericht verstuurd</h2>
			
	<p class="desktop-only">We zijn nu contact aan het zoeken met de abonnementhouder. Wat zijn de volgende stappen?</p>
			
	<ol>
		<li><strike>Laten weten dat jij mee wil reizen.</strike><br>
			<small class="help"><strike>Dit hebben wij net voor je gedaan.</strike></small>
		</li>
		<li>Wachten op goedkeuring van de reiziger.<br>
			<small class="help">Op deze manier is de reiziger ervan op de hoogte dat jij mee wil reizen.</small>
		</li>
		<li>De reiziger betalen via ons systeem en zijn contactgegevens ontvangen.<br>
			<small class="help">Het geld is een kleine beloning voor de abonnementhouder en is zo'n 10% van de prijs van het treinkaartje.</small>
		</li>
	</ol>
	
</div><div class="clickable-list mobile-only">
	<ol>
		<li><strike>Laten weten dat jij mee wil reizen.</strike><br>
			<small class="help"><strike>Dit hebben wij net voor je gedaan.</strike></small>
		</li>
		<li>Wachten op goedkeuring van de reiziger.<br>
			<small class="help">Op deze manier is de reiziger ervan op de hoogte dat jij mee wil reizen.</small>
		</li>
		<li>De reiziger betalen via ons systeem en zijn contactgegevens ontvangen.<br>
			<small class="help">Het geld is een kleine beloning voor de abonnementhouder en is zo'n 10% van de prijs van het treinkaartje.</small>
		</li>
	</ol>
</div>

<?php
include 'footer.php';
?>