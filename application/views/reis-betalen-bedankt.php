<?php
$title = 'Betalen';
include 'header.php';
?>

<div class="col6">
<h1>Bedankt voor het betalen</h1>

	<p>We verwachten binnen enkele seconden een bevestiging van Paypal te ontvangen. Daarna versturen we je de gegevens van de abonnementhouder. Gebruik deze gegevens om een ontmoetingsplaats af te spreken.</p>

	
</div><div class="col4">
	<div class=" alert inloggen">
		<h2>Reis betaald</h2>
				
		<p>Alle stappen zijn afgerond!</p>
				
		<ol>
			<li><strike>Laten weten dat jij mee wil reizen.</strike><br>
				<small class="help"><strike>Dit hebben wij net voor je gedaan.</strike></small>
			</li>
			<li><strike>Wachten op goedkeuring van de reiziger.</strike><br>
				<small class="help"><strike>Op deze manier is de reiziger ervan op de hoogte dat jij mee wil reizen.</strike></small>
			</li>
			<li><strike>De reiziger betalen via ons systeem en zijn contactgegevens ontvangen.</strike><br>
				<small class="help"><strike>Het geld is een kleine beloning voor de abonnementhouder en is zo'n 10% van de prijs van het treinkaartje.</strike></small>
			</li>
		</ol>
	</div>
</div>

<?php
include 'footer.php';
?>